package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatPerfilRepository;

import com.hellokoding.auth.modelo.entities.CatPerfil;

import java.util.List;
import java.util.Optional;

@Component
public class CatPerfilService {

@Autowired 
CatPerfilRepository repository;

    public List<CatPerfil> getCatPerfil() {
        return (List<CatPerfil>) repository.findAll();
    }

    public void add(CatPerfil dto) {
        System.out.println("+++++++++++++++++++++++++++++++++++++ AQUI");
        repository.save(dto);
    }
  
}