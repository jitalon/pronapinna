package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblUsuarioCreate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;

import java.util.List;



public interface TblUsuarioCreateRepository extends JpaRepository<TblUsuarioCreate, Integer> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE TblUsuarioCreate t set t.password =?1 where t.id = ?2")
    void actualizaPassword(String password, int id);

}