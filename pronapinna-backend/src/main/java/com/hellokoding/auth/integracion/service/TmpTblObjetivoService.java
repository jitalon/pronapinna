package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TmpTblObjetivoRepository;

import com.hellokoding.auth.modelo.entities.TmpTblObjetivo;

import java.util.List;
import java.util.Optional;

@Component
public class TmpTblObjetivoService {

@Autowired 
TmpTblObjetivoRepository repository;

    public void add(TmpTblObjetivo dto) {
        repository.save(dto);
    }  
}