package com.hellokoding.auth.integracion.service.repositorio;

import java.util.List;
import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.objects.Filtros;

interface TblPlanNacionalAnualRepositoryCustom {

	List<TblPlanNacionalAnual> tblPlanNacionalAnualFilters(Filtros filtro);
}