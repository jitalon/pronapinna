package com.hellokoding.auth.integracion.service;

import com.hellokoding.auth.objects.Respuesta;
import com.hellokoding.auth.objects.RolDependencia;

import java.util.List;

import com.hellokoding.auth.objects.GraficaDto;

public interface CatalogosService {
	public Respuesta consultarCatalogos();
	public GraficaDto graficas();
	public List<RolDependencia> dependenciaRol();
}


