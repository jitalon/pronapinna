package com.hellokoding.auth.integracion.service.repositorio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.TypedQuery;

import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.objects.Filtros;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OprBitacoraRepositoryImpl implements OprBitacoraRepositoryCustom {

	private static final Logger LOG = LoggerFactory.getLogger(OprBitacoraRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	public List<OprBitacora> oprBitacoraFilters2(Filtros filtro, boolean bndFiltro) {

		List<OprBitacora> lstOprBitacora = null;

		try {
			TypedQuery<OprBitacora> typedQuery = em.createQuery(this.generarQuery3(filtro, bndFiltro),
					OprBitacora.class);
			this.establecerparametros(filtro, typedQuery);
			lstOprBitacora = typedQuery.getResultList();

		} catch (Exception exc) {

		} finally {
			if (em != null) {
				em.close();
			}
		}
		return lstOprBitacora;
	}

	private String generarQuery3(Filtros filtro, boolean bndFiltro) {
		// StringBuilder query = new StringBuilder("from OprBitacora e ");
		StringBuilder query = new StringBuilder("SELECT bt FROM OprBitacora bt INNER JOIN bt.tblUsuario tbus "
				+ ((bndFiltro) ? "INNER JOIN tbus.catPerfil cape " : "")
				+ "INNER JOIN tbus.catDependencia catde INNER JOIN bt.oprActividad act INNER JOIN act.tblPlanNacionalAnual tpna LEFT JOIN tpna.catDependenciaCoodinadora cd2");
		boolean primerParametro = true;// INNER JOIN bt.tblUsuario tbus INNER JOIN tu.catPerfil cape
//		System.out.println("********************************************");
//		System.out.println(filtro.getDependencia());
		if (filtro.getIdEstatus() > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" bt.idEstatus = :idEstatus");
		}
		if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" cd2.id = :dependencia");
		} // " and not act.id = 3 and (select id from tbl_usuario tu where tu.id =
			// bt.id_usuario ) = 2"
		query.append((bndFiltro) ? " and not act.id = 3 and cape.id = 1" : "");
//		query.append((bndFiltro) ? " and not act.id = 3" : "");//se comento la condicion de arriba por que no traia los datos de captura
//		query.append(" group by bt.oprActividad.fecha, bt.oprActividad.id");
//		System.out.println(query);
//		System.out.println("------------------------------------------------");
		return query.toString();
	}

	public List<OprBitacora> oprBitacoraFilters(Filtros filtro, boolean bndFiltro) {
		List<OprBitacora> lstOprBitacora = null;
		try {
			TypedQuery<OprBitacora> typedQuery = null;
//			if (!bndFiltro) {
//				typedQuery = em.createQuery(this.generarQuery(), OprBitacora.class);
//			} else {
			typedQuery = em.createQuery(this.generarQuery(filtro, bndFiltro), OprBitacora.class);
//			}
			this.establecerparametros(filtro, typedQuery);
			lstOprBitacora = typedQuery.getResultList();

		} catch (Exception exc) {

		} finally {
			if (em != null) {
				em.close();
			}
		}
		return lstOprBitacora;
	}

	private String generarQuery(Filtros filtro, boolean bndFiltro) {
		// StringBuilder query = new StringBuilder("from OprBitacora e ");
		StringBuilder query = new StringBuilder(
				"SELECT bt FROM OprBitacora bt INNER JOIN bt.oprActividad act INNER JOIN act.tblPlanNacionalAnual tpna LEFT JOIN tpna.catDependenciaCoodinadora cd2"
						+ ((bndFiltro) ? " WHERE bt.idEstatus = 1" : ""));
		boolean primerParametro = false;

		if (filtro.getIdEstatus() > 0) {
			primerParametro = this.agregarConectorinicial(((bndFiltro) ? false : true), query);
			query.append(" bt.accion LIKE '%' || :idEstatus || '%' ");
		}
		if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" cd2.id = :dependencia");
		}
		return query.toString();
	}

//	private String generarQuery() {
//		StringBuilder query = new StringBuilder(
//				"SELECT bt FROM OprBitacora bt INNER JOIN bt.oprActividad act INNER JOIN act.tblPlanNacionalAnual tpna LEFT JOIN tpna.catDependenciaCoodinadora cd2");
//		return query.toString();
//	}

	private boolean agregarConectorinicial(boolean esPrimerParametro, StringBuilder query) {
		boolean primerParametro = esPrimerParametro;
		if (primerParametro) {
			query.append(" where");
			primerParametro = false;
		} else {
			query.append(" and");
		}
		return primerParametro;
	}

	private void establecerparametros(Filtros filtro, TypedQuery<OprBitacora> typedQuery) {
		if (filtro != null) {
			if (Integer.valueOf(filtro.getIdEstatusPNA()) != null && Integer.valueOf(filtro.getIdEstatusPNA()) > 0) {
				typedQuery.setParameter("idEstatusPNA", filtro.getIdEstatusPNA());
			}
			if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
				typedQuery.setParameter("dependencia", filtro.getDependencia());
			}
			if (Integer.valueOf(filtro.getIdEstatus()) != null && Integer.valueOf(filtro.getIdEstatus()) > 0) {
				typedQuery.setParameter("idEstatus", filtro.getIdEstatus());
			}
			if (Integer.valueOf(filtro.getPeriodoProgramado()) != null
					&& Integer.valueOf(filtro.getPeriodoProgramado()) > 0) {
				typedQuery.setParameter("periodoProgramado", filtro.getPeriodoProgramado());
			}
			if (Integer.valueOf(filtro.getPeriodoRealizado()) != null
					&& Integer.valueOf(filtro.getPeriodoRealizado()) > 0) {
				typedQuery.setParameter("periodoRealizado", filtro.getPeriodoRealizado());
			}
			if (Integer.valueOf(filtro.getRespuesta()) != null && Integer.valueOf(filtro.getRespuesta()) > 0) {
				typedQuery.setParameter("respuesta", filtro.getRespuesta());
			}

		}

	}

	/* ----------------------------------------------------- */

	public List<OprBitacora> btRevicionFilters(Filtros filtro) {

		List<OprBitacora> lstOprBitacora = null;

		try {
			TypedQuery<OprBitacora> typedQuery = em.createQuery(this.generarQuery2(filtro), OprBitacora.class);
			this.establecerparametros(filtro, typedQuery);
			lstOprBitacora = typedQuery.getResultList();

		} catch (Exception exc) {

		} finally {
			if (em != null) {
				em.close();
			}
		}
		return lstOprBitacora;
	}

	private String generarQuery2(Filtros filtro) {

		StringBuilder query = new StringBuilder(
				"SELECT bt FROM OprBitacora bt INNER JOIN bt.oprActividad act INNER JOIN act.tblPlanNacionalAnual tpna LEFT JOIN tpna.catDependenciaCoodinador cd "
						+ " LEFT JOIN tpna.catDependenciaCoodinadora cd2 LEFT JOIN tpna.catTipoRespuesta ctr LEFT JOIN act.catPeriodoRealizacion pR WHERE bt.idEstatus = 1");
		boolean primerParametro = false;

		if (Integer.valueOf(filtro.getIdEstatusPNA()) != null && Integer.valueOf(filtro.getIdEstatusPNA()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.idEstatus = :idEstatusPNA");
		}
		if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" cd2.id = :dependencia");
		}
		if (Integer.valueOf(filtro.getPeriodoProgramado()) != null
				&& Integer.valueOf(filtro.getPeriodoProgramado()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.oprActividad.catPeriodoProgramado.id = :periodoProgramado");
		}
		if (Integer.valueOf(filtro.getPeriodoRealizado()) != null
				&& Integer.valueOf(filtro.getPeriodoRealizado()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" pR.id = :periodoRealizado");
		}
		if (Integer.valueOf(filtro.getRespuesta()) != null && Integer.valueOf(filtro.getRespuesta()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.catTipoRespuesta.id = :respuesta");
		}
		if (Integer.valueOf(filtro.getIdEstatus()) != null && Integer.valueOf(filtro.getIdEstatus()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" bt.accion LIKE '%' || :idEstatus || '%' ");
		}

		return query.toString();
	}

}