package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatTipoRespuestaRepository;

import com.hellokoding.auth.modelo.entities.CatTipoRespuesta;

import java.util.List;
import java.util.Optional;

@Component
public class CatTipoRespuestaService {

@Autowired 
CatTipoRespuestaRepository repository;

    public List<CatTipoRespuesta> getCatTipoRespuesta() {
        return (List<CatTipoRespuesta>) repository.findAll();
    }

    public List<CatTipoRespuesta> getCatTipoRespuestaActivo() {
        return (List<CatTipoRespuesta>) repository.findAllActivo();
    }

    public void add(CatTipoRespuesta dto) {
        repository.save(dto);
    }
  
}