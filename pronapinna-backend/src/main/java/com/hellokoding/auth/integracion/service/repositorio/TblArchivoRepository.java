package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblArchivo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblArchivoRepository extends JpaRepository<TblArchivo, Integer>, TblArchivoRepositoryCustom {


}

