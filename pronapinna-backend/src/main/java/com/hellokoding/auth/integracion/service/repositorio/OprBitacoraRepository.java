package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.jpa.repository.query.Procedure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.objects.Filtros;

public interface OprBitacoraRepository extends JpaRepository<OprBitacora, Integer> , OprBitacoraRepositoryCustom {


}


    