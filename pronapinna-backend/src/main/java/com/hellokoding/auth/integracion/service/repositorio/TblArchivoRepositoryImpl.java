package com.hellokoding.auth.integracion.service.repositorio;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hellokoding.auth.modelo.entities.TblArchivo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public class TblArchivoRepositoryImpl implements TblArchivoRepositoryCustom {

	private static final Logger LOG = LoggerFactory.getLogger(OprBitacoraRepositoryImpl.class);

	@PersistenceContext 
	private EntityManager em;

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional
	public int insertArchivo(TblArchivo entity) {

		try {

			String query = " WITH s AS (" + 
					"     (select ? as idArchivo, ? as descripcion, ? as archivo1, ? as archivo2 , ? as archivo3, ? as disponible, ? as id_actividad)" + 
					")," + 
					" upd AS (" + 
					"     UPDATE pronapinna.tbl_archivo " + 
					"     SET    descripcion = s.descripcion ," +
					"            archivo1 = s.archivo1 ," +
					"            archivo2 = s.archivo2 ," +
					"            archivo3 = s.archivo3 ," +
					"            disponible = s.disponible " +
					"     FROM   s " + 
					"     WHERE  id = s.idArchivo " + 
					"     RETURNING id " + 
					" ) " + 
					" INSERT INTO pronapinna.tbl_archivo (descripcion, archivo1, archivo2, archivo3, disponible, id_actividad) " + 
					" SELECT  s.descripcion,s.archivo1,s.archivo2 ,s.archivo3,s.disponible,s.id_actividad " + 
					"   FROM  s " + 
					"  WHERE  s.idArchivo NOT IN (select id FROM upd);";

			em.createNativeQuery(query)
			.setParameter(1, entity.getId())
			.setParameter(2, entity.getDescripcion())
			.setParameter(3, entity.getArchivo1())
			.setParameter(4, entity.getArchivo2())
			.setParameter(5, entity.getArchivo3())
			.setParameter(6, entity.getDisponible())
			.setParameter(7, entity.getOprActividad().getId())
			.executeUpdate();

		}catch (Exception exc) {

		}finally {
			if (em != null) {
				em.close();
			}
		}
		return 0;
	}

}