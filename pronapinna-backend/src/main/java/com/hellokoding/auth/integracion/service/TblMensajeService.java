package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblMensajeRepository;

import com.hellokoding.auth.modelo.entities.TblMensaje;

import java.util.List;
import java.util.Optional;

@Component
public class TblMensajeService {

@Autowired 
TblMensajeRepository repository;

    public List<TblMensaje> getTblMensaje() {
        return (List<TblMensaje>) repository.findAll();
    }
  
}