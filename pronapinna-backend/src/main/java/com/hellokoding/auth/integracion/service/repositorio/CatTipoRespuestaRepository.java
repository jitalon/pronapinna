package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatTipoRespuesta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface CatTipoRespuestaRepository extends CrudRepository<CatTipoRespuesta, Integer> {

    @Query("SELECT d FROM CatTipoRespuesta d WHERE d.idEstatus = 1")
    List<CatTipoRespuesta> findAllActivo();

}