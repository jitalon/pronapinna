package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.hellokoding.auth.modelo.entities.CatPoblacion;
import java.util.List;

public interface CatPoblacionRepository extends CrudRepository<CatPoblacion, Integer> {

}