package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatPermiso;

public interface CatPermisoRepository extends RepositoryGeneric<CatPermiso, Integer> {

}