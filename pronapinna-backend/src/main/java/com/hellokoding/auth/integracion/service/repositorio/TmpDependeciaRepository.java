package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TmpDependencia;

public interface TmpDependeciaRepository extends RepositoryGeneric<TmpDependencia, Integer> {

}