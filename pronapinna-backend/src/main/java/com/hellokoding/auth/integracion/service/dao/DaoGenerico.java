package com.hellokoding.auth.integracion.service.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collection;
import java.io.IOException;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;

//import javax.naming.InitialContext;
//import javax.sql.DataSource;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import java.util.Date;
//import java.util.Iterator;
//import java.text.SimpleDateFormat;

import java.util.List;
//import java.util.ListIterator;
//import java.util.ArrayList;

import com.hellokoding.auth.objects.TmpLoadFile;
import com.hellokoding.auth.utils.ConexionJDNI;
import com.hellokoding.auth.objects.GraficaDto;
import com.hellokoding.auth.objects.RolDependencia;

public class DaoGenerico {

//	private String url = null;
//	private String username = null;
//	private String password = null;
	ConexionJDNI conexionJDNI = new ConexionJDNI();
	private Logger logger = Logger.getAnonymousLogger();

	// private static final Logger LOG = LoggerFactory.getLogger(DaoGenerico.class);

	public DaoGenerico() {
//		Properties sipinnaSetup = new Properties();
//		try {
//			sipinnaSetup.load(DaoGenerico.class.getResourceAsStream("/application.properties"));
//
//			url = sipinnaSetup.getProperty("spring.datasource.url");
//			username = sipinnaSetup.getProperty("spring.datasource.username");
//			password = sipinnaSetup.getProperty("spring.datasource.password");

//		} catch (IOException e) {
//			logger.log(Level.SEVERE, "Error", e);
//		}
	}

	public int actualizaEstatusTblPlanNacionalAnual() {

		Connection conn = null;
		Statement st = null;
		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate("update pronapinna.tbl_plan_nacional_anual set id_estatus = 2 "
					+ " where id_periodo in (select p.id from pronapinna.tbl_periodo p inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + "   and pn.id_estatus = 1)");
			st.executeUpdate("update pronapinna.tbl_periodo set id_estatus = 2 "
					+ " where id_plan_nacional in ( select pn.id from pronapinna.tbl_plan_nacional pn where pn.id_estatus = 1)");
			st.executeUpdate("update pronapinna.tbl_plan_nacional set id_estatus = 2 where id_estatus = 1");
			st.executeUpdate("update pronapinna.tbl_linea_accion set id_estatus = 2");
			st.executeUpdate("update pronapinna.tbl_estrategia set id_estatus = 2");
			st.executeUpdate("update pronapinna.tbl_objetivo set id_estatus = 2");
			st.executeUpdate("commit;");

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public int crearPlanNacional(String clavePlanNacional) {

		Connection conn = null;
		Statement st = null;
		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate("insert into pronapinna.tbl_objetivo (clave,descripcion,id_plan_nacional,id_estatus)"
					+ " (select clave,descripcion, (select id from pronapinna.tbl_plan_nacional where clave like '%"
					+ clavePlanNacional + "%' and id_estatus = 1) ,1 " + "    from pronapinna.tmp_tbl_objetivo)");
			st.executeUpdate("commit;");
			st.executeUpdate("insert into pronapinna.tbl_estrategia (clave,descripcion,id_objetivo,id_estatus) "
					+ "     select est.clave_estrategia,est.descripcion,obj.id, 1 "
					+ "       from pronapinna.tbl_objetivo obj inner join pronapinna.tmp_tbl_estrategia est "
					+ "         on obj.clave = est.clave_objetivo and obj.id_estatus = 1");
			st.executeUpdate("commit;");
			st.executeUpdate("insert into pronapinna.tbl_linea_accion (clave,descripcion,id_estategia,id_estatus) "
					+ "     select la.clave_linea_accion,la.descripcion,est.id,1  "
					+ "       from pronapinna.tbl_estrategia est inner join pronapinna.tmp_tbl_linea_accion la "
					+ "         on est.clave = la.clave_estrategia and est.id_estatus = 1");
			st.executeUpdate("commit;");
			st.executeUpdate("insert into pronapinna.cat_dependencia (clave,descripcion) "
					+ "     select distinct(tmp.clave_dependencia), tmp.descripcion "
					+ "       from pronapinna.tmp_dependencias tmp where tmp.clave_dependencia not in (select clave from pronapinna.cat_dependencia)");
			st.executeUpdate("commit;");

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public int truncateTmp() {

		Connection conn = null;
		Statement st = null;

		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate(
					"truncate table pronapinna.tmp_tbl_objetivo; " + "truncate table pronapinna.tmp_tbl_estrategia; "
							+ "truncate table pronapinna.tmp_tbl_linea_accion; ");

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public int crearOprPlanNacional(String fechaInicio, String fechaFin) {

		Connection conn = null;
		Statement st = null;
		;
		ResultSet rs = null;

		try {
			// LOG.debug("++++++++++++++++++++++++++++++ ::::::::::::::::::::::::: " );
//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate("update pronapinna.tbl_periodo set id_estatus = 2 ");
			st.executeUpdate("commit;");
			st.executeUpdate("update pronapinna.tbl_plan_nacional_anual set id_estatus = 2 ");
			st.executeUpdate("commit;");

			rs = st.executeQuery(
					"insert into pronapinna.tbl_periodo (id_plan_nacional, periodo_ini, periodo_fin, id_estatus) VALUES "
							+ " ((select id from pronapinna.tbl_plan_nacional where id_estatus = 1) ,'" + fechaInicio
							+ "','" + fechaFin + "', 1) RETURNING id ;");
			rs.next();
			int idPeriodoCreado = rs.getInt(1);
			st.executeUpdate("commit;");

			String inserTable = " insert into pronapinna.tbl_plan_nacional_anual (id_linea_accion,fecha_inicio,fecha_fin,id_estatus,id_periodo)"
					+ "(select la.id, '" + fechaInicio + "','" + fechaFin + "', 1 , " + idPeriodoCreado
					+ "   from pronapinna.tbl_linea_accion la inner join pronapinna.tmp_dependencias dtmp "
					+ "     on la.clave = dtmp.clave_linea_accion " + "    and dtmp.rol = 'Coordinadora' "
					+ "    and la.id_estatus = 1 inner join pronapinna.cat_dependencia dep "
					+ "     on dtmp.clave_dependencia = dep.clave inner join pronapinna.tbl_estrategia est "
					+ "     on la.id_estategia = est.id "
					+ "    and est.id_estatus = 1 inner join pronapinna.tbl_objetivo obj "
					+ "     on obj.id = est.id_objetivo "
					+ "    and obj.id_estatus = 1 inner join pronapinna.tbl_plan_nacional pn "
					+ "     on obj.id_plan_nacional = pn.id " + "    and pn.id_estatus = 1 )";

			st.executeUpdate(inserTable);
			st.executeUpdate("commit;");

			st.executeUpdate(
					"insert into pronapinna.opr_plan_anual_depen_coordinadora (id_plan_nacional_anual,id_dependencia) "
							+ "select pna.id,dep.id "
							+ "  from pronapinna.tbl_linea_accion la inner join pronapinna.tbl_plan_nacional_anual pna "
							+ "    on la.id = pna.id_linea_accion "
							+ "   and la.id_estatus = 1 inner join pronapinna.tmp_dependencias dtmp  "
							+ "    on la.clave = dtmp.clave_linea_accion "
							+ "   and dtmp.rol = 'Coordinadora' inner join pronapinna.cat_dependencia dep  "
							+ "    on dtmp.clave_dependencia = dep.clave " + "   and pna.id_estatus = 1 ");
			st.executeUpdate("commit;");

			st.executeUpdate(
					"insert into pronapinna.opr_plan_anual_depen_coordinada (id_plan_nacional_anual,id_dependencia) "
							+ "select pna.id,dep.id "
							+ "  from pronapinna.tbl_linea_accion la inner join pronapinna.tbl_plan_nacional_anual pna "
							+ "    on la.id = pna.id_linea_accion "
							+ "   and la.id_estatus = 1 inner join pronapinna.tmp_dependencias dtmp "
							+ "    on la.clave = dtmp.clave_linea_accion "
							+ "   and dtmp.rol != 'Coordinadora' inner join pronapinna.cat_dependencia dep "
							+ "    on dtmp.clave_dependencia = dep.clave " + "   and pna.id_estatus = 1 ");
			st.executeUpdate("commit;");

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public int actualizaPass(String pass, String mail) {

		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = conexionJDNI.ConexionJDNI();
			String sqlStr = "update pronapinna.tbl_usuario set password = ? where email = ? ";

			st = conn.prepareStatement(sqlStr);
			st.setString(1, pass );
			st.setString(2, mail );
			st.executeUpdate();
			//st.execute("commit");
			
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public List<TmpLoadFile> TmpLoadFile() {

		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<TmpLoadFile> lstTmpLoadFile = new ArrayList<TmpLoadFile>();

		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			String sql = "select obj.clave as claveObj, obj.descripcion as descObj, est.clave_estrategia as claveEst, est.descripcion as descEst, "
					+ "       acc.clave_linea_accion as claveAccion, acc.descripcion as descAccion, dep.clave_dependencia as dependencia , dep.rol "
					+ "  from pronapinna.tmp_tbl_objetivo obj inner join pronapinna.tmp_tbl_estrategia est "
					+ "    on obj.clave = est.clave_objetivo inner join pronapinna.tmp_tbl_linea_accion acc "
					+ "    on est.clave_estrategia = acc.clave_estrategia inner join pronapinna.tmp_dependencias dep "
					+ "    on dep.clave_linea_accion = acc.clave_linea_accion " + " order by obj.clave " + " LIMIT 5 ";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				TmpLoadFile tmpLoadFile = new TmpLoadFile();
				tmpLoadFile.setClaveObj(rs.getString("claveObj"));
				tmpLoadFile.setDescObj(rs.getString("descObj"));
				tmpLoadFile.setClaveEst(rs.getString("claveEst"));
				tmpLoadFile.setDescEst(rs.getString("descEst"));
				tmpLoadFile.setClaveAccion(rs.getString("claveAccion"));
				tmpLoadFile.setDescAccion(rs.getString("descAccion"));
				tmpLoadFile.setDependencia(rs.getString("dependencia"));
				tmpLoadFile.setRol(rs.getString("rol"));

				lstTmpLoadFile.add(tmpLoadFile);
			}

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return lstTmpLoadFile;
	}

	public int actualizaPNA(int idPeriodo) {

		Connection conn = null;
		Statement st = null;
		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate("update pronapinna.tbl_plan_nacional_anual set id_estatus = 1 where id_periodo = "
					+ idPeriodo + " and id_estatus = 2");
			st.executeUpdate("commit;");

			st.executeUpdate(
					"update pronapinna.tbl_periodo set id_estatus = 1 where id = " + idPeriodo + " and id_estatus = 2");
			st.executeUpdate("commit;");

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public int actualizaBT(int idActividad, int idAccion) {

		Connection conn = null;
		Statement st = null;
		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			st.executeUpdate("update pronapinna.opr_bitacora set id_estatus = 2 where id_actividad = " + idActividad
					+ " and accion != '" + idAccion + "'");
			st.executeUpdate("commit;");

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return 1;
	}

	public GraficaDto Graficas() {

		Connection conn = null;
		Statement st = null;
		GraficaDto grafica = new GraficaDto();
		grafica.setFecha(new Date());

		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			String sql = "select row_to_json(t) " + " from ( "
					+ " select ROUND(((cast (tbl3.conprograma as decimal) / tbl3.Total) * 100),2) as ConProgramacion, "
					+ "        ROUND(((cast (tbl3.sinprograma as decimal) / tbl3.Total) * 100),2) as SinProgramacion, "
					+ "        tbl3.Total, tbl3.sinprograma as sinprogramaNum , tbl3.conprograma as conprogramaNum "
					+ " from ( "
					+ " select sum(1) as Total,sum(coalesce(campo,0)) as ConPrograma , (sum(1) - sum(coalesce(campo,0))) as SinPrograma "
					+ "   from pronapinna.tbl_plan_nacional_anual as tbl1 inner join pronapinna.tbl_periodo p "
					+ "     on p.id = tbl1.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "     on pn.id = p.id_plan_nacional " + "    and p.id_estatus = 1 " + "    and pn.id_estatus = 1 "
					+ "        LEFT join "
					+ "        (select distinct(id_plan_nacional_anual),1 as campo, id_plan_nacional_anual as idRef "
					+ "           from pronapinna.opr_actividad "
					+ "          where id_plan_nacional_anual in (select pna.id "
					+ "                                              from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "                                                on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "                                                on pn.id = p.id_plan_nacional "
					+ "                                               and p.id_estatus = 1 "
					+ "                                             where pn.id_estatus = 1) ) tbl2 "
					+ "     on tbl1.id = tbl2.idRef " + " ) tbl3 " + " ) t";

			try (ResultSet rs1 = st.executeQuery(sql)) {
				while (rs1.next()) {
					grafica.setGrafica1(rs1.getString(1));
				}
			} catch (SQLException e) {

			} finally {

			}

			String sql2 = "select row_to_json(t) " + "  from ( " + "select cast (sum(tbl3.Total) as decimal) as Total, "
					+ "       case when sum(tbl3.Total) > 0 then ROUND(((cast (sum(tbl3.SinRespuesta) as decimal) / sum(tbl3.Total)) * 100),2) else 0 end as SinRespuesta, "
					+ "       case when sum(tbl3.Total) > 0 then ROUND(((cast (sum(tbl3.EnProceso) as decimal) / sum(tbl3.Total)) * 100),2) else 0 end as EnProceso, "
					+ "       case when sum(tbl3.Total) > 0 then ROUND(((cast (sum(tbl3.Pendiente) as decimal) / sum(tbl3.Total)) * 100),2) else 0 end as Pendiente, "
					+ "       case when sum(tbl3.Total) > 0 then ROUND(((cast (sum(tbl3.Cumplida) as decimal) / sum(tbl3.Total)) * 100),2) else 0 end as Cumplida, "
					+ "       sum(tbl3.SinRespuesta) as sinrespuestanum, sum(tbl3.EnProceso) as enprocesonum, "
					+ "       sum(tbl3.Pendiente) as pendientenum, sum(tbl3.Cumplida) as cumplidanum " + "  from ( "
					+ "select coalesce(sum(1),0) as Total, 0 as SinRespuesta, 0  as EnProceso, 0 as Pendiente, 0 as Cumplida "
					+ "  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "    on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + " where pn.id_estatus = 1 "
					+ "  union " + "select 0, coalesce(sum(1),0), 0  as EnProceso, 0 as Pendiente, 0 as Cumplida "
					+ "  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "    on p.id = pna.id_periodo "
					+ "   and pna.id_tipo_respuesta is null inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + " where pn.id_estatus = 1 "
					+ "  union " + "select 0 , 0, coalesce(sum(1),0) as EnProceso ,0,0  " + // Se realizaran acciones el
																							// siguiente año a
					"  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.cat_tipo_repuesta ctr "
					+ "    on pna.id_tipo_respuesta = ctr.id " + "   and ctr.id_estatus = 1 "
					+ "   and ctr.clave = 'a' inner join pronapinna.tbl_periodo p "
					+ "    on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + " where pn.id_estatus = 1 "
					+ "  union " + "select 0 , 0, 0, coalesce(sum(1),0) as Pendiente , 0  " + // no se realizran
																								// acciones b
					"  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.cat_tipo_repuesta ctr "
					+ "    on pna.id_tipo_respuesta = ctr.id " + "   and ctr.id_estatus = 1 "
					+ "   and ctr.clave = 'b' inner join pronapinna.tbl_periodo p "
					+ "    on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + " where pn.id_estatus = 1 "
					+ "  union " + "select 0 , 0, 0, 0, coalesce(sum(1),0) as Cumplida " + // Accion Concluida c
					"  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.cat_tipo_repuesta ctr "
					+ "    on pna.id_tipo_respuesta = ctr.id " + "   and ctr.id_estatus = 1 "
					+ "   and ctr.clave = 'c' inner join pronapinna.tbl_periodo p "
					+ "    on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = p.id_plan_nacional " + "   and p.id_estatus = 1 " + " where pn.id_estatus = 1 "
					+ "  ) tbl3 " + ") t";
			try (ResultSet rs2 = st.executeQuery(sql2)) {
				while (rs2.next()) {
					grafica.setGrafica2(rs2.getString(1));
				}
			} catch (SQLException e) {

			} finally {

			}

			String sql3 = "select row_to_json(t) " + "  from ( "
					+ "        select sum(conResultados) as conResultados , sum(totalInstancias) as totalInstancias, "
					+ "               sum(instanciasRespuesta) as instanciasConInfo, sum(totalInstancias) - sum(instanciasRespuesta) as instanciasSinInfo "
					+ "          from ( "
					+ "				select count(*) as conResultados,0 as totalInstancias ,0 as instanciasRespuesta "
					+ "				  from pronapinna.opr_actividad "
					+ "				 where id_plan_nacional_anual in (select pna.id "
					+ "				                                    from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "				                                      on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "				                                      on pn.id = p.id_plan_nacional "
					+ "				                                     and p.id_estatus = 1 "
					+ "				                                   where pn.id_estatus = 1 "
					+ "                                                    and pna.id_tipo_respuesta is not null) " +
					// " and respuesta is not null "+ //se cambia resultado por respuesta
					"				union " + "				select 0,count(distinct(dep.id_dependencia)),0 "
					+ "				  from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "				    on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "				    on pn.id = p.id_plan_nacional " + "				   and p.id_estatus = 1 "
					+ "				   and pn.id_estatus = 1 inner join pronapinna.opr_plan_anual_depen_coordinadora dep "
					+ "				    on dep.id_plan_nacional_anual = pna.id " + "				union "
					+ "				select 0,0, count(distinct(dep.id_dependencia)) "
					+ "				  from pronapinna.opr_actividad act inner join pronapinna.opr_plan_anual_depen_coordinadora dep "
					+ "				    on dep.id_plan_nacional_anual = act.id_plan_nacional_anual "
					+ "				 where act.id_plan_nacional_anual in (select pna.id "
					+ "					                                    from pronapinna.tbl_plan_nacional_anual pna inner join pronapinna.tbl_periodo p "
					+ "					                                      on p.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "					                                      on pn.id = p.id_plan_nacional "
					+ "					                                     and p.id_estatus = 1 "
					+ "					                                   where pn.id_estatus = 1 "
					+ "                                                        and pna.id_tipo_respuesta is not null) "
					+
					// " and respuesta is not null "+ //se cambia resultado por respuesta
					"          ) tbl1 " + "  ) t ";

			try (ResultSet rs3 = st.executeQuery(sql3)) {
				while (rs3.next()) {
					grafica.setReporte2(rs3.getString(1));
				}
			} catch (SQLException e) {

			} finally {

			}

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return grafica;
	}

	public List<RolDependencia> dependenciaRol() {

		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<RolDependencia> respuesta = new ArrayList<RolDependencia>();

		// LOG.debug("******************************** dependenciaRol DAO");

		try {

//			conn = DriverManager.getConnection(url, username, password);
			conn = conexionJDNI.ConexionJDNI();
			st = conn.createStatement();

			String sql = "select * " + "from ( " + "select dep.descripcion,  "
					+ "(select (case when coalesce(count(*),0) > 0 then 'SI' else '-' end) as coordinadora "
					+ "  from pronapinna.tbl_linea_accion la inner join pronapinna.tbl_plan_nacional_anual pna "
					+ "    on la.id = pna.id_linea_accion left join pronapinna.opr_plan_anual_depen_coordinadora coordinadora "
					+ "    on pna.id = coordinadora.id_plan_nacional_anual inner join pronapinna.tbl_periodo periodo "
					+ "    on periodo.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = periodo.id_plan_nacional " + "   and pn.id_estatus = 1 "
					+ "   where coordinadora.id_dependencia = dep.id ), "
					+ "(select (case when coalesce(count(*),0) > 0 then 'SI' else '-' end) as coordinada "
					+ "  from pronapinna.tbl_linea_accion la inner join pronapinna.tbl_plan_nacional_anual pna "
					+ "    on la.id = pna.id_linea_accion left join pronapinna.opr_plan_anual_depen_coordinada coordinada "
					+ "    on pna.id = coordinada.id_plan_nacional_anual inner join pronapinna.tbl_periodo periodo "
					+ "    on periodo.id = pna.id_periodo inner join pronapinna.tbl_plan_nacional pn "
					+ "    on pn.id = periodo.id_plan_nacional " + "   and pn.id_estatus = 1 "
					+ "   where coordinada.id_dependencia = dep.id ) " + "  from pronapinna.cat_dependencia dep "
					+ "  ) as tbl_temp " + "where (tbl_temp.coordinada != '-' or tbl_temp.coordinadora != '-') "
					+ "order by tbl_temp.descripcion ";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				RolDependencia rolDependencia = new RolDependencia();

				rolDependencia.setDependencia(rs.getString(1));
				rolDependencia.setCoordinadora(rs.getString(2));
				rolDependencia.setCoordinada(rs.getString(3));

				respuesta.add(rolDependencia);
				System.out.println("************************************************ :::::::::::::::::::::::: "
						+ respuesta.size());
			}
			// LOG.debug("******************************** dependenciaRol DAO " +
			// respuesta.size());
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return respuesta;
	}

}