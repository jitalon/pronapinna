package com.hellokoding.auth.integracion.service.repositorio;

import java.util.List;
import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.objects.Filtros;

interface OprBitacoraRepositoryCustom {

	List<OprBitacora> oprBitacoraFilters(Filtros filtro, boolean bndFiltro);

	List<OprBitacora> oprBitacoraFilters2(Filtros filtro, boolean bndFiltro);

	List<OprBitacora> btRevicionFilters(Filtros filtro);

}