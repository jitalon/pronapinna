package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TmpDependeciaRepository;

import com.hellokoding.auth.modelo.entities.TmpDependencia;

import java.util.List;
import java.util.Optional;

@Component
public class TmpDependenciaService {

@Autowired 
TmpDependeciaRepository repository;

    public void add(TmpDependencia dto) {
        System.out.println("+++++++++++++++++++++++++++++++++++++ AQUI SAVE");
        repository.save(dto);
    }  
}