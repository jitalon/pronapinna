package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblUsuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface TblUsuarioRepository extends CrudRepository<TblUsuario, Integer> {

    @Query("SELECT d FROM TblUsuario d WHERE d.usuario = ?1")
    TblUsuario findByUserName(String username);
    
    public TblUsuario findByUsuario(String username);

}