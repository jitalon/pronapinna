package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatEstado;

public interface CatEstadoRepository extends RepositoryGeneric<CatEstado, Integer> {

}