package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.OprBitacoraRepository;

import com.hellokoding.auth.modelo.entities.OprBitacora;

import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

import java.util.List;
import java.util.Optional;
import java.util.HashMap;

@Component
public class OprBitacoraService {

@Autowired 
OprBitacoraRepository repository;

    public List<OprBitacora> getOprBitacora() {	
        return (List<OprBitacora>) repository.findAll();
    }

    public void add(OprBitacora dto) {
        DaoGenerico a = new DaoGenerico();
                    a.actualizaBT(dto.getOprActividad().getId(), Integer.parseInt(dto.getAccion()));
        repository.save(dto);
    }  
}