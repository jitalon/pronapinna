package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatPeriodoRepository;

import com.hellokoding.auth.modelo.entities.CatPeriodo;

import java.util.List;
import java.util.Optional;

@Component
public class CatPeriodoService {

@Autowired 
CatPeriodoRepository repository;

    public List<CatPeriodo> getCatPeriodo() {
        return (List<CatPeriodo>) repository.findAll();
    }

    public void add(CatPeriodo dto) {
        repository.save(dto);
    }
  
}