package com.hellokoding.auth.integracion.service.repositorio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.TypedQuery;

import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.objects.Filtros;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TblPlanNacionalAnualRepositoryImpl implements TblPlanNacionalAnualRepositoryCustom {

private static final Logger LOG = LoggerFactory.getLogger(TblPlanNacionalAnualRepositoryImpl.class);

@PersistenceContext 
private EntityManager em;

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
         public List<TblPlanNacionalAnual> tblPlanNacionalAnualFilters(Filtros filtro) {

                List<TblPlanNacionalAnual> lstTblPlanNacionalAnual = null;

		try {
			TypedQuery<TblPlanNacionalAnual> typedQuery = em.createQuery(this.generarQuery(filtro), TblPlanNacionalAnual.class);
			this.establecerparametros(filtro, typedQuery);
			lstTblPlanNacionalAnual = typedQuery.getResultList();

		}catch (Exception exc) {

		}finally {
			if (em != null) {
				em.close();
			}
		}
            return lstTblPlanNacionalAnual;
	}

	private String generarQuery(Filtros filtro) {
		StringBuilder query = new StringBuilder("SELECT tpna FROM TblPlanNacionalAnual tpna LEFT JOIN tpna.catDependenciaCoodinador cd " + 
				                                " LEFT JOIN tpna.catDependenciaCoodinadora cd2 LEFT JOIN tpna.catTipoRespuesta ctr " +
				                                " LEFT JOIN tpna.oprActividad act LEFT JOIN act.catPeriodoRealizacion pR  LEFT JOIN act.catPeriodoProgramacion pP");
		boolean primerParametro = true;

		if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" cd2.id = :dependencia");
		}
		if (Integer.valueOf(filtro.getPeriodoProgramado()) != null && Integer.valueOf(filtro.getPeriodoProgramado()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" pP.id = :periodoProgramado");
		}
		if (Integer.valueOf(filtro.getPeriodoRealizado()) != null && Integer.valueOf(filtro.getPeriodoRealizado()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" pR.id = :periodoRealizado");
		}
		if (Integer.valueOf(filtro.getEdad()) != null && Integer.valueOf(filtro.getEdad()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.tblUsuario.catDependencia.id = :id");
		}
		if (Integer.valueOf(filtro.getPoblacion()) != null && Integer.valueOf(filtro.getPoblacion()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" cp.id = :poblacion");
		}
		if (Integer.valueOf(filtro.getRespuesta()) != null && Integer.valueOf(filtro.getRespuesta()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.catTipoRespuesta.id = :respuesta");
		}
		if (Integer.valueOf(filtro.getIdEstatus()) != null && Integer.valueOf(filtro.getIdEstatus()) > 0) {
			primerParametro = this.agregarConectorinicial(primerParametro, query);
			query.append(" tpna.idEstatus = :idEstatus");
		}
                        LOG.debug("PLAN NACIONAL ------------------------------------- :::::::::::::::::::::  "+ query.toString());
		return query.toString();
	}

	private boolean agregarConectorinicial(boolean esPrimerParametro, StringBuilder query) {
		boolean primerParametro = esPrimerParametro;
		if (primerParametro) {
			query.append(" where");
			primerParametro = false;
		} else {
			query.append(" and");
		}
		return primerParametro;
	}

	private void establecerparametros(Filtros filtro, TypedQuery<TblPlanNacionalAnual> typedQuery) {
		if(filtro != null){
			if (Integer.valueOf(filtro.getDependencia()) != null && Integer.valueOf(filtro.getDependencia()) > 0) {
			    LOG.debug("----------------------------------- :: :: " + filtro.getDependencia());
				typedQuery.setParameter("dependencia", filtro.getDependencia());
			}
			if (Integer.valueOf(filtro.getPeriodoProgramado()) != null && Integer.valueOf(filtro.getPeriodoProgramado()) > 0) {
				LOG.debug("----------------------------------- :: :: " + filtro.getPeriodoProgramado());
				typedQuery.setParameter("periodoProgramado", filtro.getPeriodoProgramado());
			}
			if (Integer.valueOf(filtro.getPeriodoRealizado()) != null && Integer.valueOf(filtro.getPeriodoRealizado()) > 0) {
				LOG.debug("----------------------------------- :: :: " + filtro.getPeriodoRealizado());
				typedQuery.setParameter("periodoRealizado", filtro.getPeriodoRealizado());
			}
			if (Integer.valueOf(filtro.getEdad()) != null && Integer.valueOf(filtro.getEdad()) > 0) {
				typedQuery.setParameter("edad", filtro.getEdad());
			}
			if (Integer.valueOf(filtro.getPoblacion()) != null && Integer.valueOf(filtro.getPoblacion()) > 0) {
				LOG.debug("----------------------------------- :: :: " + filtro.getPoblacion());
				typedQuery.setParameter("poblacion", filtro.getPoblacion());
			}
			if (Integer.valueOf(filtro.getRespuesta()) != null && Integer.valueOf(filtro.getRespuesta()) > 0) {
				LOG.debug("----------------------------------- :: :: " + filtro.getRespuesta());
				typedQuery.setParameter("respuesta", filtro.getRespuesta());
			}
			if (Integer.valueOf(filtro.getIdEstatus()) != null && Integer.valueOf(filtro.getIdEstatus()) > 0) {
				LOG.debug("----------------------------------- :: :: " + filtro.getIdEstatus());
				typedQuery.setParameter("idEstatus", filtro.getIdEstatus());
			}
		}
	}
}