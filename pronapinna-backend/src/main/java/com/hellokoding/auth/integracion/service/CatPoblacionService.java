package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatPoblacionRepository;

import com.hellokoding.auth.modelo.entities.CatPoblacion;

import java.util.List;
import java.util.Optional;

@Component
public class CatPoblacionService {

@Autowired 
CatPoblacionRepository repository;

    public List<CatPoblacion> getCatPoblacion() {
        return (List<CatPoblacion>) repository.findAll();
    }

    public void add(CatPoblacion dto) {
        repository.save(dto);
    }
  
}