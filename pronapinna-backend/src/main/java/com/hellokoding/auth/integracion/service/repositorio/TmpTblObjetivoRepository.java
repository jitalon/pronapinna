package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TmpTblObjetivo;

public interface TmpTblObjetivoRepository extends RepositoryGeneric<TmpTblObjetivo, Integer> {

}