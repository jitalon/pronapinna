package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.hellokoding.auth.modelo.entities.CatCategoria;
import java.util.List;

public interface CatCategoriaRepository extends CrudRepository<CatCategoria, Integer> {

    @Query("SELECT d FROM CatCategoria d WHERE d.idEstatus = 1")
    List<CatCategoria> findAllActivo();

}