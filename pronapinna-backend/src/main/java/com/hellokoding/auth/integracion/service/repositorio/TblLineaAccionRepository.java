package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblLineaAccion;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface TblLineaAccionRepository extends RepositoryGeneric<TblLineaAccion, Integer> {

    @Query("SELECT d FROM TblLineaAccion d WHERE d.idEstatus = 1")
    List<TblLineaAccion> findAllActivo();

}