package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatCategoriaRepository;

import com.hellokoding.auth.modelo.entities.CatCategoria;

import java.util.List;
import java.util.Optional;

@Component
public class CatCategoriaService {

@Autowired 
CatCategoriaRepository repository;

    public List<CatCategoria> getCatCategoria() {
        return (List<CatCategoria>) repository.findAll();
    }

    public List<CatCategoria> getCatCategoriaActivo() {
        return (List<CatCategoria>) repository.findAllActivo();
    }

    public void add(CatCategoria dto) {
        repository.save(dto);
    }
  
}