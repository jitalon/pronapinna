package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblEstrategiaRepository;

import com.hellokoding.auth.modelo.entities.TblEstrategia;

import java.util.List;
import java.util.Optional;

@Component
public class TblEstrategiaService {

@Autowired 
TblEstrategiaRepository repository;

    public List<TblEstrategia> getTblEstrategia() {
        return (List<TblEstrategia>) repository.findAll();
    }

    public List<TblEstrategia> getTblEstrategiaActivo() {
        return (List<TblEstrategia>) repository.findAllActivo();
    }

    public void add(TblEstrategia dto) {
        repository.save(dto);
    }  
}