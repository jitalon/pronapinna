package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TmpTblLineaAccionRepository;

import com.hellokoding.auth.modelo.entities.TmpTblLineaAccion;

import java.util.List;
import java.util.Optional;

@Component
public class TmpTblLineaAccionService {

@Autowired 
TmpTblLineaAccionRepository repository;

    public void add(TmpTblLineaAccion dto) {
        repository.save(dto);
    }  
}