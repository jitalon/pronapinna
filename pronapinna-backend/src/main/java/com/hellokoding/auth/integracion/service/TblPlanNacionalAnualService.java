package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblPlanNacionalAnualRepository;

import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.modelo.entities.OprActividad;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Date;

@Component
public class TblPlanNacionalAnualService {

@Autowired 
TblPlanNacionalAnualRepository repository;

    public void add(TblPlanNacionalAnual dto) {
        repository.save(dto);
    }

    public List<TblPlanNacionalAnual> getTblPlanNacionalAnual(int idDependencia) {
        return (List<TblPlanNacionalAnual>) repository.findByIdDependencia(idDependencia);
    }  

    public List<OprActividad> getTblPlanNacionalAnualEstatus(Collection status) {
        return (List<OprActividad>) repository.findByIdActividadEstatus(status);
    }  

    public List<TblPlanNacionalAnual> getTblPlanNacionalAnual() {
        return (List<TblPlanNacionalAnual>) repository.findAllActivo();
    }

    public int planNacionalAnualCreado(Date fechaInicio, Date fechaFin) {
        return repository.planNacionalAnualCreado(fechaInicio, fechaFin);
    }
    
    public void actualizaFechas(Date fechaInicio, Date fechaFin) {
         repository.actualizaFechas(fechaInicio, fechaFin);
    }
    
}