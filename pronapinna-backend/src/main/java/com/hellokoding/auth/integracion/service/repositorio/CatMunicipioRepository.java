package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatMunicipio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface CatMunicipioRepository extends CrudRepository<CatMunicipio, Integer> {

    @Query("SELECT d FROM CatMunicipio d WHERE d.catEstado.id = ?1")
    List<CatMunicipio> findByIdEstado(int clave);
    
    @Query("SELECT d FROM CatMunicipio d WHERE d.clave = ?1")
    List<CatMunicipio> findByClave(String clave);

}