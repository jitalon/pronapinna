package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.OprActividadRepository;
import com.hellokoding.auth.integracion.service.repositorio.OprActividadCreateRepository;

import com.hellokoding.auth.modelo.entities.OprActividad;
import com.hellokoding.auth.modelo.entities.OprActividadCreate;

import java.util.List;
import java.util.Optional;

@Component
public class OprActividadService {

@Autowired 
OprActividadRepository repository;

@Autowired 
OprActividadCreateRepository repositoryCreate;

    public OprActividadCreate addCreate(OprActividadCreate dto) {
        return repositoryCreate.saveAndFlush(dto);
    }

    public OprActividad add(OprActividad dto) {
        return repository.saveAndFlush(dto);
    }

    public List<OprActividad> getOprActividad() {
        return (List<OprActividad>) repository.findAll();
    } 

    public List<OprActividad> findActividad(int IdPlanNacional, int IdActividad) {
        return (List<OprActividad>) repository.findActividad( IdPlanNacional, IdActividad);
    } 
     
    public void actualizaEstatusOprActividad(int estatus, int idActividad, int idPlanNacional) {
        repository.actualizaEstatusOprActividad(estatus, idActividad, idPlanNacional);
    } 

}