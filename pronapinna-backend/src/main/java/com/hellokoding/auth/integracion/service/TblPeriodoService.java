package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblPeriodoRepository;

import com.hellokoding.auth.modelo.entities.TblPeriodo;

import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

import java.util.List;
import java.util.Optional;

@Component
public class TblPeriodoService {

@Autowired 
TblPeriodoRepository repository;

    public List<TblPeriodo> getTblPeriodo(int idPlanNacional) {
        return (List<TblPeriodo>) repository.findAllActivo(idPlanNacional);
    }

    public List<TblPeriodo> getTblPeriodoActivo() {
        return (List<TblPeriodo>) repository.getTblPeriodoActivo();
    }

    public void add(TblPeriodo dto) {
        DaoGenerico a = new DaoGenerico();
        a.actualizaPNA(dto.getId());
        repository.save(dto);
    }
  
}