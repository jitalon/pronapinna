package com.hellokoding.auth.filter;

import java.io.IOException;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.hellokoding.auth.integracion.service.impl.JWTServiceImpl;
import com.hellokoding.auth.integracion.service.repositorio.JWTRepository;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter{
	private JWTRepository jWTRepository;
	public JWTAuthorizationFilter(AuthenticationManager authenticationManager,JWTRepository jWTRepository) {
		super(authenticationManager);
		this.jWTRepository = jWTRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String header = request.getHeader(JWTServiceImpl.HEADER_STRING);

		if (!requiresAuthentication(header)) {
			chain.doFilter(request, response);
			return;
		}
		
		
		
		UsernamePasswordAuthenticationToken authentication = null;
		
		if(jWTRepository.validate(header)) {
			
			authentication = new UsernamePasswordAuthenticationToken(jWTRepository.getUsername(header), null, jWTRepository.getRoles(header));
		}
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		chain.doFilter(request, response);
	}
	
	protected boolean requiresAuthentication(String header) {
		if (header == null || !header.startsWith(JWTServiceImpl.TOKEN_PREFIX)) {
			return false;
		}
		return true;
	}
}
