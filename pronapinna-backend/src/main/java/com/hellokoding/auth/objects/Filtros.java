package com.hellokoding.auth.objects;

import java.time.LocalDate;

public class Filtros {
	
	private int dependencia;
	private int periodoProgramado;
	private int periodoRealizado;
	private int edad;
	private int poblacion;
	private int respuesta;
	private int idEstatus;
	private int idEstatusPNA;

	public int getDependencia() {
		return dependencia;
	}
	public void setDependencia(int dependencia) {
		this.dependencia = dependencia;
	}
	public int getPeriodoProgramado() {
		return periodoProgramado;
	}
	public void setPeriodoProgramado(int periodoProgramado) {
		this.periodoProgramado = periodoProgramado;
	}
	public int getPeriodoRealizado() {
		return periodoRealizado;
	}
	public void setPeriodoRealizado(int periodoRealizado) {
		this.periodoRealizado = periodoRealizado;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}
	public int getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}
	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}
	public int getIdEstatusPNA() {
		return idEstatusPNA;
	}
	public void setIdEstatusPNA(int idEstatusPNA) {
		this.idEstatusPNA = idEstatusPNA;
	}		
}
