package com.hellokoding.auth.objects;

public class UploadedFile {
	 
    private int length;
    private byte[] bytes;
    private String name;
    private String type;
    private String baitesArchivo;
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBaitesArchivo() {
		return baitesArchivo;
	}
	public void setBaitesArchivo(String baitesArchivo) {
		this.baitesArchivo = baitesArchivo;
	}
	
	
	
    
//	public String getBaitesArchivo() {
//		return baitesArchivo;
//	}
//	public void setBaitesArchivo(String baitesArchivo) {
//		UploadedFile.baitesArchivo = baitesArchivo;
//	}
//	public int getLength() {
//		return length;
//	}
//	public void setLength(int length) {
//		UploadedFile.length = length;
//	}
//	public byte[] getBytes() {
//		return bytes;
//	}
//	public void setBytes(byte[] bytes) {
//		UploadedFile.bytes = bytes;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		UploadedFile.name = name;
//	}
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		UploadedFile.type = type;
//	}
    
    
}
