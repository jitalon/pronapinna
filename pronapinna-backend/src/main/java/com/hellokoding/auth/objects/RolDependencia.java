package com.hellokoding.auth.objects;

public class RolDependencia {

	private String dependencia;
	private String coordinada;
	private String coordinadora;
	
	public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}
	public String getCoordinada() {
		return coordinada;
	}
	public void setCoordinada(String coordinada) {
		this.coordinada = coordinada;
	}
	public String getCoordinadora() {
		return coordinadora;
	}
	public void setCoordinadora(String coordinadora) {
		this.coordinadora = coordinadora;
	}
	
	
}
