package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.OrderBy;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import java.util.List;
import java.util.ArrayList;

@Entity
@Table(name = "tbl_plan_nacional_anual")
public class TblPlanNacionalAnual implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_plan_nacional_anual")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_plan_nacional_anual", allocationSize = 1, name = "sec_tbl_plan_nacional_anual")
    private Integer id;

    @Column(name = "id_estatus")
    private int idEstatus;

    @Column(name = "fecha_inicio")
    private Date fechaInicio;

    @Column(name = "fecha_fin")
    private Date fechaFin;

    @JoinColumn(name="id_linea_accion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblLineaAccion tblLineaAccion;

    @JoinColumn(name="id_tipo_respuesta",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatTipoRespuesta catTipoRespuesta;

    @OneToMany(mappedBy="tblPlanNacionalAnual" )
    @OrderBy("catActividad ASC")
    private List<OprActividad> oprActividad;

	public List<OprActividad> getOprActividad() {
	    return oprActividad;
	}
	public void addOprActividad(OprActividad f) {
	    oprActividad.add(f);
	}
	public void setOprActividad(List<OprActividad> oprActividad) {
	    this.oprActividad = oprActividad;
        }

	@ManyToMany(cascade=CascadeType.REFRESH)
	@JoinTable(
		name="opr_plan_anual_depen_coordinada",
		joinColumns={@JoinColumn(name="id_plan_nacional_anual", referencedColumnName="id")},
		inverseJoinColumns={@JoinColumn(name="id_dependencia", referencedColumnName="id")})
	private List<CatDependencia> catDependenciaCoodinador;


	public List<CatDependencia> getCatDependenciaCoodinador()
	{
		return catDependenciaCoodinador;
	}

	public void setCatDependenciaCoodinador(List<CatDependencia> catDependenciaCoodinador)
	{
		this.catDependenciaCoodinador = catDependenciaCoodinador;
	}

        @ManyToMany(cascade=CascadeType.REFRESH)
	@JoinTable(
		name="opr_plan_anual_depen_coordinadora",
		joinColumns={@JoinColumn(name="id_plan_nacional_anual", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_dependencia", referencedColumnName="ID")})
	private List<CatDependencia> catDependenciaCoodinadora;


	public List<CatDependencia> getCatDependenciaCoodinadora()
	{
		return catDependenciaCoodinadora;
	}

	public void setCatDependenciaCoodinadora(List<CatDependencia> catDependenciaCoodinadora)
	{
		this.catDependenciaCoodinadora = catDependenciaCoodinadora;
	}

	public TblLineaAccion getTblLineaAccion() {
		return tblLineaAccion;
	}
	public void setTblLineaAccion(TblLineaAccion tblLineaAccion) {
		this.tblLineaAccion = tblLineaAccion;
	}

	public CatTipoRespuesta getCatTipoRespuesta() {
		return catTipoRespuesta;
	}
	public void setCatTipoRespuesta(CatTipoRespuesta catTipoRespuesta) {
		this.catTipoRespuesta = catTipoRespuesta;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

  public TblPlanNacionalAnual () {
		super();
        oprActividad=new ArrayList();
  }
}
