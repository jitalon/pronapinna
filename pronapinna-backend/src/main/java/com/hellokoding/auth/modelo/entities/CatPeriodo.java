package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;

import java.util.List;

@Entity
@Table(name = "cat_periodo")
public class CatPeriodo {

  @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_periodo")
    @SequenceGenerator(sequenceName = "pronapinna.sec_cat_periodo", allocationSize = 1, name = "sec_cat_periodo")
  private Integer id;
  private String clave;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }
  public CatPeriodo (Integer id, String descripcion, String clave) {
    super();
    this.id = id;
    this.descripcion = descripcion;
    this.clave = clave;
  }
  public CatPeriodo() {
    super();
  }
  
  
}