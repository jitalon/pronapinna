package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
@Entity
@Table(name = "tmp_tbl_estrategia")
public class TmpTblEstrategia {
  @Id
  private Integer id;
  @Column(name = "clave_estrategia")
  private String claveEstrategia;
  @Column(name = "clave_objetivo")
  private String claveObjetivo;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getClaveEstrategia() {
    return claveEstrategia;
  }
  public void setClaveEstrategia(String claveEstrategia) {
    this.claveEstrategia = claveEstrategia;
  }
  public String getClaveObjetivo() {
    return claveObjetivo;
  }
  public void setClaveObjetivo(String claveObjetivo) {
    this.claveObjetivo = claveObjetivo;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public TmpTblEstrategia (Integer id, String claveEstrategia, String claveObjetivo, String descripcion) {
    super();
    this.id = id;
    this.claveEstrategia = claveEstrategia;
    this.claveObjetivo = claveObjetivo;
    this.descripcion = descripcion;
  }
  public TmpTblEstrategia() {
    super();
  }
  
  
}