package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity

@Table(name = "tbl_objetivo")
public class TblObjetivo {

  @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_objetivo")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_objetivo", allocationSize = 1, name = "sec_tbl_objetivo")
    private Integer id;
    private String clave;
    private String descripcion;
    @Column(name = "id_estatus")
    private Integer idEstatus;

  @JoinColumn(name="id_plan_nacional",unique=true)
  @OneToOne(cascade=CascadeType.REFRESH)
  private TblPlanNacional tblPlanNacional;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public TblPlanNacional getTblPlanNacional() {
    return tblPlanNacional;
  }
  public void setTblPlanNacional(TblPlanNacional tblPlanNacional) {
    this.tblPlanNacional = tblPlanNacional;
  }

    public int getIdEstatus() {
      return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
      this.idEstatus = idEstatus;
    }  
 
  public TblObjetivo (Integer id, String clave, String Descripcion, TblPlanNacional tblPlanNacional, Integer idEstatus) {
    super();
    this.id = id;
    this.clave = clave;
    this.descripcion = descripcion;
    this.tblPlanNacional = tblPlanNacional;
    this.idEstatus = idEstatus;
  }
  public TblObjetivo() {
    super();
  }
  
  
}