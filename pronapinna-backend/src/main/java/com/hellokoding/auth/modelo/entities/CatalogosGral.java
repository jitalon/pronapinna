package com.hellokoding.auth.modelo.entities;

import java.util.List;


public class CatalogosGral {
  
   private List<CatDependencia> catDependencia;
   private List<CatPerfil> catPerfil;
   private List<CatPermiso> catPermiso;
   private List<CatEstado> catEstado;
   private List<CatPeriodo> catPeriodo;
   private List<CatClasificacionEdad> catClasificacionEdad;
   private List<CatPoblacion> catPoblacion;
   private List<CatActividad> catActividad;
   private List<CatTipoRespuesta> catTipoRespuesta;
   private List<CatDerecho> catDerecho;
   private List<CatCategoria> catCategoria;
   private String fecha;

   	public List<CatDerecho> getCatDerecho() {
		return catDerecho;
	}
	public void setCatDerecho(List<CatDerecho> catDerecho) {
		this.catDerecho = catDerecho;
	}

   	public List<CatPermiso> getCatPermiso() {
		return catPermiso;
	}
	public void setCatPermiso(List<CatPermiso> catPermiso) {
		this.catPermiso = catPermiso;
	}
   	public List<CatPerfil> getCatPerfil() {
		return catPerfil;
	}
	public void setCatPerfil(List<CatPerfil> catPerfil) {
		this.catPerfil = catPerfil;
	}
	public List<CatDependencia> getCatDependencia() {
		return catDependencia;
	}
	public void setCatDependencia(List<CatDependencia> catDependencia) {
		this.catDependencia = catDependencia;
	}

	public List<CatPeriodo> getCatPeriodo() {
		return catPeriodo;
	}
	public void setCatPeriodo(List<CatPeriodo> catPeriodo) {
		this.catPeriodo = catPeriodo;
	}

	public List<CatEstado> getCatEstado() {
		return catEstado;
	}
	public void setCatEstado(List<CatEstado> catEstado) {
		this.catEstado = catEstado;
	}

	public List<CatClasificacionEdad> getCatClasificacionEdad() {
		return catClasificacionEdad;
	}
	public void setCatClasificacionEdad(List<CatClasificacionEdad> catClasificacionEdad) {
		this.catClasificacionEdad = catClasificacionEdad;
	}

	public List<CatPoblacion> getCatPoblacion() {
		return catPoblacion;
	}
	public void setCatPoblacion(List<CatPoblacion> catPoblacion) {
		this.catPoblacion = catPoblacion;
	}

	public List<CatActividad> getCatActividad() {
		return catActividad;
	}
	public void setCatActividad(List<CatActividad> list) {
		this.catActividad = list;
	}

	public List<CatTipoRespuesta> getCatTipoRespuesta() {
		return catTipoRespuesta;
	}
	public void setCatTipoRespuesta(List<CatTipoRespuesta> catTipoRespuesta) {
		this.catTipoRespuesta = catTipoRespuesta;
	}

	public List<CatCategoria> getCatCategoria() {
		return catCategoria;
	}
	public void setCatCategoria(List<CatCategoria> catCategoria) {
		this.catCategoria = catCategoria;
	}

	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}