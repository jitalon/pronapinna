package com.hellokoding.auth.modelo.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "cat_perfil")
public class CatPerfil {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_perfil")
	@SequenceGenerator(sequenceName = "pronapinna.sec_cat_perfil", allocationSize = 1, name = "sec_cat_perfil")
	private Integer id;
	private String clave;
	private String descripcion;

	@ManyToMany(cascade=CascadeType.REFRESH)
	@JoinTable(
		name="opr_perfil_permiso",
		joinColumns={@JoinColumn(name="id_perfil", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_permiso", referencedColumnName="ID")})
	private List<CatPermiso> catPermiso;


	public List<CatPermiso> getCatPermiso()
	{
		return catPermiso;
	}

	public void setCatPermiso(List<CatPermiso> catPermiso)
	{
		this.catPermiso = catPermiso;
	}

	public CatPerfil() {
		super();
	}
	
	public CatPerfil(int id,String clave, String descripcion) {
		super();
		this.id = id;
		this.clave = clave;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}