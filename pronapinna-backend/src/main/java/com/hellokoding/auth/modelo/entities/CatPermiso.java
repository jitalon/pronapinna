package com.hellokoding.auth.modelo.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "cat_permiso")
public class CatPermiso {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_permiso")
    @SequenceGenerator(sequenceName = "pronapinna.sec_cat_permiso", allocationSize = 1, name = "sec_cat_permiso")
    private int id;
    @Column(name = "clave")
    private String clave;
    @Column(name = "descripcion")
    private String descripcion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public CatPermiso() {
	super();
    }

}