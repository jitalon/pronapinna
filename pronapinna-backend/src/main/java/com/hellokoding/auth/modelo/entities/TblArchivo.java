package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


import java.util.List;

@Entity
@Table(name = "tbl_archivo")
public class TblArchivo {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_Archivo")
  @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_archivo", allocationSize = 1, name = "sec_tbl_archivo")
  private Integer id;
  private String descripcion;
  private String archivo1;
  private String archivo2;
  private String archivo3;
  private Integer disponible;  

    @OneToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "id_actividad",unique=true)
    @JsonBackReference
    private OprActividad oprActividad;

	public void setOprActividad(OprActividad oprActividad) {
		this.oprActividad = oprActividad;
	}

	public OprActividad getOprActividad() {
		return oprActividad;
	}

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public int getDisponible() {
    return disponible;
  }
  public void setDisponible(int disponible) {
    this.disponible = disponible;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getArchivo1() {
    return archivo1;
  }
  public void setArchivo1(String archivo1) {
    this.archivo1 = archivo1;
  }
  public String getArchivo2() {
    return archivo2;
  }
  public void setArchivo2(String archivo2) {
    this.archivo2 = archivo2;
  }
  public String getArchivo3() {
    return archivo3;
  }
  public void setArchivo3(String archivo3) {
    this.archivo3 = archivo3;
  }
  public TblArchivo() {
    super();
  }
  
  
}