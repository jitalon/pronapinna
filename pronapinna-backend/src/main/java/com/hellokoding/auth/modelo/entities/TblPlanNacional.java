package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tbl_plan_nacional")
public class TblPlanNacional implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_plan_nacional")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_plan_nacional", allocationSize = 1, name = "sec_tbl_plan_nacional")
    private Integer id;
    private String clave;

    @Column(name = "fecha_carga")
    private Date fechaCarga;

    @Column(name = "id_estatus")
    private int idEstatus;

    @Column(name = "fecha_inicio")
    private int fechaInicio;

    @Column(name = "fecha_fin")
    private int fechaFin;

    private String documento;
   
    @JoinColumn(name="id_usuario",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblUsuario tblUsuario;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
        public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public TblUsuario getTblUsuario() {
		return tblUsuario;
	}
	public void setTblUsuario(TblUsuario tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}

	public int getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(int fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(int fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

  public TblPlanNacional() {
		super();
  }
}
