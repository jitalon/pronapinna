package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "tbl_estrategia")
public class TblEstrategia {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_estrategia")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_estrategia", allocationSize = 1, name = "sec_tbl_estrategia")
    private Integer id;
    private String clave;
    private String descripcion;
    @Column(name = "id_estatus")
    private Integer idEstatus;
    
    @JoinColumn(name="id_objetivo",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblObjetivo tblObjetivo;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public TblObjetivo getTblObjetivo() {
        return tblObjetivo;
    }
    public void setTblObjetivo(TblObjetivo tblObjetivo) {
        this.tblObjetivo = tblObjetivo;
    }

    public int getIdEstatus() {
      return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
      this.idEstatus = idEstatus;
    }

    public TblEstrategia (Integer id, String clave, String descripcion, TblObjetivo tblObjetivo, Integer idEstatus) {
        super();
        this.id = id;
        this.clave = clave;
        this.descripcion = descripcion;
        this.tblObjetivo = tblObjetivo;
        this.idEstatus = idEstatus;
    }

    public TblEstrategia() {
        super();
    }
 
}