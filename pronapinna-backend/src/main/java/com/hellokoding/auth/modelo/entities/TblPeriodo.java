package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;

import java.util.List;
import java.util.Date;

@Entity
@Table(name = "tbl_periodo")
public class TblPeriodo {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_periodo")
  @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_periodo", allocationSize = 1, name = "sec_tbl_periodo")

  private Integer id;
  private Integer idPlanNacional;
  @Column(name = "periodo_ini")
  private Date periodoIni;
  @Column(name = "periodo_fin")
  private Date periodoFin;
  private Integer idEstatus;
  
	public int getId() {
	    return id;
	}
	public void setId(int id) {
	    this.id = id;
	}
	public int getIdPlanNacional() {
	    return idPlanNacional;
	}
	public void setIdPlanNacional(int idPlanNacional) {
	    this.idPlanNacional = idPlanNacional;
	}
	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}

	public Date getPeriodoIni() {
		return periodoIni;
	}
	public void setPeriodoIni(Date periodoIni) {
		this.periodoIni = periodoIni;
	}

	public Date getPeriodoFin() {
		return periodoFin;
	}
	public void setPeriodoFin(Date periodoFin) {
		this.periodoFin = periodoFin;
	}

  public TblPeriodo() {
    super();
  }
  
  
}