package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
@Entity

@Table(name = "tmp_tbl_objetivo")
public class TmpTblObjetivo {
  @Id
  private Integer id;
  private String clave;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
 
  public TmpTblObjetivo (Integer id, String clave, String Descripcion) {
    super();
    this.id = id;
    this.clave = clave;
    this.descripcion = descripcion;
  }
  public TmpTblObjetivo() {
    super();
  }
  
  
}