package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
@Entity
//, schema="Pronapinna"
@Table(name = "tmp_dependencias")
public class TmpDependencia {
  @Id
  private Integer id;
  @Column(name = "clave_linea_accion")
  private String claveLineaAccion;
  @Column(name = "clave_dependencia")
  private String claveDependencia;
  private String descripcion;
  private String rol;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getClaveDependencia() {
    return claveDependencia;
  }
  public void setClaveDependencia(String claveDependencia) {
    this.claveDependencia = claveDependencia;
  }
  public String getClaveLineaAccion() {
    return claveLineaAccion;
  }
  public void setClaveLineaAccion(String claveLineaAccion) {
    this.claveLineaAccion = claveLineaAccion;
  }
  public String getRol() {
    return rol;
  }
  public void setRol(String rol) {
    this.rol = rol;
  }
  public TmpDependencia (Integer id, String claveLineaAccion, String claveDependencia, String rol, String descripcion) {
    super();
    this.id = id;
    this.claveLineaAccion = claveLineaAccion;
    this.claveDependencia = claveDependencia;
    this.descripcion = descripcion;
    this.rol = rol;
  }
  public TmpDependencia() {
    super();
  }
  
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
  
  
}