package com.hellokoding.auth.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
/*import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;*/
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.modelo.entities.CatDependencia;

public class InsertExcelTabla {

	@Autowired
	CatDependenciaService service;

	private static Logger logger = Logger.getAnonymousLogger();

	XSSFRow row;

	public void readFile(String fileName) throws FileNotFoundException, IOException, Exception {
		FileInputStream fis;
		XSSFWorkbook workbookRead = null;
		try {

			System.out.println(
					"-------------------------------READING THE SPREADSHEET-------------------------------------");
			fis = new FileInputStream(fileName);
			workbookRead = new XSSFWorkbook(fis);

			int numeroDeHojas = workbookRead.getNumberOfSheets(); // Obtenemos el número de hojas que contiene el
																	// documento

			if (numeroDeHojas < 4) {
				throw new Exception(
						"El plan Nacional debe contener las siguientes hojas (Objetivos,Estrategias,Lineas_Accion,Dependencias)");
			} else {
				for (int i = 0; i < numeroDeHojas; i++) {
					XSSFSheet spreadsheetRead = workbookRead.getSheetAt(i);

					Iterator<Row> rowIterator = spreadsheetRead.iterator();
					while (rowIterator.hasNext()) {
						row = (XSSFRow) rowIterator.next();

						if (row.getRowNum() == 0) {
							continue; // just skip the rows if row number is 0 or 1
						}

						if (workbookRead.getSheetName(i).equals("Dependencias")) {
							try {
								CatDependencia dependenciaNew = new CatDependencia();
								Iterator<Cell> cellIterator = row.cellIterator();
								while (cellIterator.hasNext()) {
									Cell cell = cellIterator.next();
								}

								dependenciaNew.setId(Integer.parseInt(row.getCell(0).getStringCellValue()));
								dependenciaNew.setClave(row.getCell(1).getStringCellValue());
								dependenciaNew.setDescripcion(row.getCell(2).getStringCellValue());

								System.out.println("************ Id : " + dependenciaNew.getId());
								System.out.println("************ Clave : " + dependenciaNew.getClave());
								System.out.println("************ Descripcion : " + dependenciaNew.getDescripcion());
								service.add(dependenciaNew);
							} catch (Exception e) {
								logger.log(Level.SEVERE, "Error", e);
								break;
								// throw new Exception("No se puede procesar Dependencias, revisar el archivo e
								// intentar de nuevo");
							}
						}
					} // while 1
					System.out.println("Values Inserted Successfully");
				} // for
				fis.close();
				workbookRead.close();
			} // else
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error", e);
		} finally {
			workbookRead.close();
		}
	}

}