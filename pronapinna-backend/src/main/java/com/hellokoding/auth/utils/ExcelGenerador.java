package com.hellokoding.auth.utils;
 
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;

import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.*;
 
import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.modelo.entities.CatDependencia;
import com.hellokoding.auth.modelo.entities.OprActividad;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
 
public class ExcelGenerador {

        //private static final Logger LOG = LoggerFactory.getLogger(ExcelGenerador.class);
		private static Logger logger = Logger.getAnonymousLogger();

        private static String FOLDER = "";
  
  public static ByteArrayInputStream bitacoraToExcel(List<OprBitacora> lstOprBitacora) throws IOException {
    String[] COLUMNs = {"id","Objetivo", "Estrategia", "Acci\u00f3n puntual", "Actividad", "Instancia", "Usuario", "TipoMovimiento", "Fecha" };
    try(
        Workbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
    ){
      CreationHelper createHelper = workbook.getCreationHelper();
   
      Sheet sheet = workbook.createSheet("Bitacora");
   
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerFont.setColor(IndexedColors.BLUE.getIndex());
   
      CellStyle headerCellStyle = workbook.createCellStyle();
      headerCellStyle.setFont(headerFont);
   
      // Row for Header
      Row headerRow = sheet.createRow(0);
   
      // Header
      for (int col = 0; col < COLUMNs.length; col++) {
        Cell cell = headerRow.createCell(col);
        cell.setCellValue(COLUMNs[col]);
        cell.setCellStyle(headerCellStyle);
      }
   
      // CellStyle for Age
      CellStyle ageCellStyle = workbook.createCellStyle();
      ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-mm-dd hh:mm:ss"));
   
      int rowIdx = 1;
      if(lstOprBitacora.size()>0){
      for (OprBitacora oprBitacora : lstOprBitacora) {
        Row row = sheet.createRow(rowIdx++);
   
        row.createCell(0).setCellValue(oprBitacora.getId());
        row.createCell(1).setCellValue(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getTblEstrategia().getTblObjetivo().getDescripcion());
        row.createCell(2).setCellValue(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getTblEstrategia().getDescripcion());
        row.createCell(3).setCellValue(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getDescripcion());
        row.createCell(4).setCellValue(oprBitacora.getOprActividad().getCatActividad().getClave());
        
        String strDependenciaCordinadora = "";
        for (CatDependencia catDependencia : oprBitacora.getOprActividad().getTblPlanNacionalAnual().getCatDependenciaCoodinadora()){
             strDependenciaCordinadora = strDependenciaCordinadora + catDependencia.getDescripcion() + " \n ";
        }
        
        row.createCell(5).setCellValue(strDependenciaCordinadora);
        row.createCell(6).setCellValue(oprBitacora.getTblUsuario().getNombre()+ " " + oprBitacora.getTblUsuario().getApellidoPaterno()+ " " + oprBitacora.getTblUsuario().getApellidoMaterno());

        String accion = "";
	switch (oprBitacora.getAccion()) {
	case "1":
		accion = "Captura";
		break;
	case "2":
		accion = "Validaci\u00f3n";
		break;
	case "3":
		accion = "Resultados";
		break;
	case "4":
		accion = "Autorizaci\u00f3n";
		break;
	default:
		accion = "Concluida";
		break;
	}

        row.createCell(7).setCellValue(accion);
   
        Cell ageCell = row.createCell(8);
        ageCell.setCellValue(oprBitacora.getFecha());
        ageCell.setCellStyle(ageCellStyle);
      }}
   
      workbook.write(out);
      return new ByteArrayInputStream(out.toByteArray());
    }
  }

  public static ByteArrayInputStream planNacionalAnualToExcel(List<TblPlanNacionalAnual> lstTblPlanNacionalAnual) throws IOException {

		Properties sipinnaSetup = new Properties();  
		try {
			sipinnaSetup.load(ExcelGenerador.class.getResourceAsStream("/application.properties"));

			FOLDER = sipinnaSetup.getProperty("path_prin");
                        
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error", e);
		}  

    String[] COLUMNs = {"No.","OBJETIVO","ESTRATEGIA", "LÍNEA DE ACCIÓN", "INSTANCIAS COORDINADAS", "ACTIVIDAD 1 PROGRAMADA", "RESULTADO 1", "ACTIVIDAD 2 PROGRAMADA", 
                        "RESULTADO 2", "ACTIVIDAD 3 PROGRAMADA", "RESULTADO 3", "ACTIVIDAD 4 PROGRAMADA", "RESULTADO 4"};

    String imgPathIzq = FOLDER +"logoIzquierda.png";
    String imgPathDer = FOLDER+ "logoDerecha.png";

    try(
        XSSFWorkbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
    ){
      CreationHelper createHelper = workbook.getCreationHelper();
      XSSFSheet sheet = workbook.createSheet("Plan_Nacional");

      InputStream inputStream = new FileInputStream(imgPathIzq);
      byte[] imageBytes = IOUtils.toByteArray(inputStream);
      int my_picture_id = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);
      inputStream.close();               
      /* Create the drawing container */
      XSSFDrawing drawing = sheet.createDrawingPatriarch();
      /* Create an anchor point */
      XSSFClientAnchor my_anchor = new XSSFClientAnchor();
      /* Define top left corner, and we can resize picture suitable from there */
      my_anchor.setCol1(0);
      my_anchor.setRow1(0);           
      /* Invoke createPicture and pass the anchor point and ID */
      XSSFPicture  my_picture = drawing.createPicture(my_anchor, my_picture_id);
      /* Call resize method, which resizes the image */
      my_picture.resize(3.5,4.5);

      InputStream inputStream2 = new FileInputStream(imgPathDer);
      byte[] imageBytes2 = IOUtils.toByteArray(inputStream2);
      int my_picture_id2 = workbook.addPicture(imageBytes2, Workbook.PICTURE_TYPE_PNG);
      inputStream2.close();               
      /* Create the drawing container */
      XSSFDrawing drawing2 = sheet.createDrawingPatriarch();
      /* Create an anchor point */
      XSSFClientAnchor my_anchor2 = new XSSFClientAnchor();
      /* Define top left corner, and we can resize picture suitable from there */
      my_anchor2.setCol1(8);
      my_anchor2.setRow1(0);           
      /* Invoke createPicture and pass the anchor point and ID */
      XSSFPicture  my_picture2 = drawing2.createPicture(my_anchor2, my_picture_id2);
      /* Call resize method, which resizes the image */
      my_picture2.resize(5.0,4.5);

      sheet.setColumnWidth(0, 3000);
          for(int i=1;i<=12;i++){
            sheet.setColumnWidth(i, 9000);
          }
String pattern = "yyyy-MM-dd";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
String date = simpleDateFormat.format(new Date());

      Row row_0 = sheet.createRow(0);
      Cell cell_0 = row_0.createCell(0);
      cell_0.setCellValue("");

      Row row_1 = sheet.createRow(6);
      Cell cell_1 = row_1.createCell(4);
      cell_1.setCellValue("Líneas de acción del PRONAPINNA coordinadas por la Secretaría del Trabajo y Previsión Social");
      Cell cell_2 = row_1.createCell(11);
      cell_2.setCellValue("Fecha de emisión del reporte:");
      Cell cell_3 = row_1.createCell(12);
      cell_3.setCellValue(date);

   
        // Set up font
        Font font = workbook.createFont();
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setBold(true);
        // set up background color
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillBackgroundColor(IndexedColors.RED.getIndex());
        //cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillPattern(FillPatternType.ALT_BARS);

      // Row for Header
      Row headerRow = sheet.createRow(8);
   
      // Header
      for (int col = 0; col < COLUMNs.length; col++) {
        Cell cell = headerRow.createCell(col);
        cell.setCellValue(COLUMNs[col]);
        cell.setCellStyle(cellStyle);
      }
   
      // CellStyle for Age
      CellStyle ageCellStyle = workbook.createCellStyle();
      ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
   
      int rowIdx = 9;
      int idRows = 1;
      if(null != lstTblPlanNacionalAnual && lstTblPlanNacionalAnual.size() > 0){
      for (TblPlanNacionalAnual tblPlanNacionalAnual : lstTblPlanNacionalAnual) {
        Row row = sheet.createRow(rowIdx++);
   
        //row.createCell(0).setCellValue(tblPlanNacionalAnual.getId());
        row.createCell(0).setCellValue(idRows);
        //row.createCell(1).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getTblEstrategia().getTblObjetivo().getClave());
        row.createCell(1).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getTblEstrategia().getTblObjetivo().getDescripcion());
        //row.createCell(3).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getTblEstrategia().getClave());
        row.createCell(2).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getTblEstrategia().getDescripcion());
        //row.createCell(5).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getClave());
        row.createCell(3).setCellValue(tblPlanNacionalAnual.getTblLineaAccion().getDescripcion());

        Cell cell = null;
        CellStyle cs = workbook.createCellStyle();
        cs.setWrapText(true);

/*           String strDependenciaCord = "";
           for (CatDependencia catDependencia : tblPlanNacionalAnual.getCatDependenciaCoodinador()){
                strDependenciaCord = strDependenciaCord + catDependencia.getClave() + " \n ";
           }

        cell = row.createCell(4);
        cell.setCellValue(strDependenciaCord);
        cell.setCellStyle(cs);*/

        String strDependenciaCordinadora = "";
           for (CatDependencia catDependencia : tblPlanNacionalAnual.getCatDependenciaCoodinadora()){
                strDependenciaCordinadora = strDependenciaCordinadora + catDependencia.getDescripcion() + " \n ";
           }
        cell = row.createCell(4);
        cell.setCellValue(strDependenciaCordinadora);
        cell.setCellStyle(cs);

/*        String descripcionTR = "";
        if(null != tblPlanNacionalAnual.getCatTipoRespuesta()) descripcionTR=tblPlanNacionalAnual.getCatTipoRespuesta().getDescripcion();

        row.createCell(9).setCellValue(descripcionTR);*/
           int celda = 5;
           int celda2 = 6;

           for (OprActividad oprActividad : tblPlanNacionalAnual.getOprActividad()){
                row.createCell(celda).setCellValue(oprActividad.getDescripcion());
                row.createCell(celda2).setCellValue(oprActividad.getResultado());
                celda = celda + 2;
                celda2 = celda2 + 2;
           }

        /*Cell ageCell = row.createCell(5);
        ageCell.setCellValue(oprBitacora.getFecha());
        ageCell.setCellStyle(ageCellStyle);*/
        idRows ++;
      }}
 
      workbook.write(out);

      return new ByteArrayInputStream(out.toByteArray());
    }
  }

}