package com.hellokoding.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;

import com.hellokoding.auth.filter.JWTAuthenticationFilter;
import com.hellokoding.auth.filter.JWTAuthorizationFilter;
import com.hellokoding.auth.integracion.service.JpaUserDetailsService;
import com.hellokoding.auth.integracion.service.repositorio.JWTRepository;

import java.io.IOException;
import java.util.Properties;
import javax.servlet.MultipartConfigElement;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
//    @Autowired
//    private DataSource dataSource;
	
	@Autowired
	private JpaUserDetailsService jpaUserDetailsService;
    
//    @Autowired
//    private LoginSuccessHandler loginSuccessHandler;
    
    @Autowired
    private JWTRepository jWTRepository;

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//            .authorizeRequests()
//                .antMatchers("/resources/**", "/recuperacion", "/passRecovery/**").permitAll()
//                .anyRequest().authenticated()
//                .and()
//            .formLogin()
//                .loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password")
//                .loginPage("/login")
//                .permitAll()
//                .and()
//            .logout()
//                .permitAll()
//                .and()
//            .csrf().disable();
//    }

    

    @Bean
    public PasswordEncoder passwordEncoder(){
    	PasswordEncoder encoder = new BCryptPasswordEncoder();
    	return encoder;
    }

    @Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().antMatchers("/resources/**", "/recuperacion", "/passRecovery/**","/api/login").permitAll()
//		.anyRequest().authenticated()
//		.and()
//		.addFilter(new JWTAuthenticationFilter(authenticationManager()))
//		.addFilter(new JWTAuthorizationFilter(authenticationManager()))
//		.csrf().disable();
    	
    	http
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.cors().and()
		.csrf().disable()
		.authorizeRequests()
			.antMatchers("/**","/resources/**", "/recuperacion", "/passRecovery/**").permitAll()
			.antMatchers(HttpMethod.POST, "/api/login").permitAll()
			.anyRequest().authenticated().and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager(), jWTRepository))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(), jWTRepository));
		
        http 
  	  	.headers()
  	  		.contentSecurityPolicy("script-src 'self'");
        http
        .headers()
            .referrerPolicy(ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);
        http
    	.headers()
    		.httpStrictTransportSecurity()
    			.includeSubDomains(true)
    			.maxAgeInSeconds(2592000);
        http.headers().xssProtection();
        
	}

	@Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    String password = getEncryptedPass();
	PasswordEncoder passwordEncoder1 = new BCryptPasswordEncoder();
	String hashedPassword = passwordEncoder1.encode(password);
	
	auth.userDetailsService(jpaUserDetailsService);

//        System.out.println("************************************************************ " + hashedPassword);
//
//        auth.jdbcAuthentication().dataSource(dataSource)
//        .usersByUsernameQuery("select usuario,password,true from pronapinna.Tbl_usuario where usuario=? and id_estatus=1")
//        .authoritiesByUsernameQuery("select nombre,b.clave from pronapinna.Tbl_usuario a, pronapinna.cat_perfil b where a.id_perfil = b.id and a.usuario=? and a.id_estatus=1");
//            
    }

	private String getEncryptedPass() {
		// TODO Auto-generated method stub
		return "password";
	}

	
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		Properties sipinnaSetup = new Properties();
		try {
			sipinnaSetup.load(WebSecurityConfig.class.getResourceAsStream("/application.properties"));

		} catch (IOException e) {
		}

	     MultipartConfigFactory factory = new MultipartConfigFactory();

	     factory.setMaxFileSize(sipinnaSetup.getProperty("spring.http.multipart.max-file-size"));

	     factory.setMaxRequestSize(sipinnaSetup.getProperty("spring.http.multipart.max-request-size"));

	     return factory.createMultipartConfig();

	}
}