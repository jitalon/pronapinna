package com.hellokoding.auth.web;

//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileOutputStream;
//import java.io.BufferedReader;
import java.io.IOException;
//import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

//import java.util.Arrays;
//import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.multipart.MultipartHttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hellokoding.auth.objects.Respuesta;
import com.hellokoding.auth.objects.UploadedFile;
//import org.apache.commons.codec.binary.Base64;
//import com.hellokoding.auth.utils.InsertExcelTabla;

//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.util.List;

//import org.json.JSONArray;
//import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.integracion.service.TmpDependenciaService;
import com.hellokoding.auth.integracion.service.TmpTblObjetivoService;
import com.hellokoding.auth.integracion.service.TmpTblEstrategiaService;
import com.hellokoding.auth.integracion.service.TmpTblLineaAccionService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalService;

import com.hellokoding.auth.modelo.entities.CatDependencia;
import com.hellokoding.auth.modelo.entities.TmpDependencia;
import com.hellokoding.auth.modelo.entities.TmpTblObjetivo;
import com.hellokoding.auth.modelo.entities.TmpTblEstrategia;
import com.hellokoding.auth.modelo.entities.TmpTblLineaAccion;

@Controller
@SessionAttributes("user")
public class FileUploadController {

	@Autowired
	CatDependenciaService service;
	@Autowired
	TmpDependenciaService tpmDependenciaService;
	@Autowired
	TmpTblObjetivoService tmpTblObjetivoService;
	@Autowired
	TmpTblEstrategiaService tmpTblEstrategiaService;
	@Autowired
	TmpTblLineaAccionService tmpTblLineaAccionService;
	@Autowired
	TblPlanNacionalService tblPlanNacionalService;

	private Logger logger = Logger.getAnonymousLogger();
//        private static final Logger LOG = LoggerFactory.getLogger(FileUploadController.class);

	private String path_prin = null;

	public FileUploadController() {
		Properties sipinnaSetup = new Properties();
		try {
			sipinnaSetup.load(FileUploadController.class.getResourceAsStream("/sipinna.properties"));

			path_prin = sipinnaSetup.getProperty("path_prin");

		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
	}

	UploadedFile ufile;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@RequestMapping(value = "/upload/plan_nacional.do", method = RequestMethod.POST, consumes = {
			"multipart/form-data" })
	public ResponseEntity<?> upload(@RequestParam("file0") MultipartFile reapExcelDataFile) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();

		try {

			tblPlanNacionalService.truncateTmp();

			XSSFWorkbook workbookRead = new XSSFWorkbook(reapExcelDataFile.getInputStream());

			int numeroDeHojas = workbookRead.getNumberOfSheets(); // Obtenemos el número de hojas que contiene el
																	// documento
//	            LOG.debug("Número Hojas: "+numeroDeHojas);

			if (numeroDeHojas < 4) {
				throw new Exception(
						"El plan Nacional debe contener las siguientes hojas (Objetivos,Estrategias,Lineas_Accion,Dependencias)");
			} else {
				String depFaltantes = null;
				for (int j = 0; j < numeroDeHojas; j++) {

//                    LOG.debug("Hoja #: "+j+" - Nombre: "+workbookRead.getSheetName(j));
					XSSFSheet worksheet = workbookRead.getSheetAt(j);

					for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {

						TmpDependencia tmpDependenciaNew = new TmpDependencia();
						TmpTblObjetivo tmpTblObjetivoNew = new TmpTblObjetivo();
						TmpTblEstrategia tmpTblEstrategiaNew = new TmpTblEstrategia();
						TmpTblLineaAccion tmpTblLineaAccionNew = new TmpTblLineaAccion();

						XSSFRow row = worksheet.getRow(i);

						if (row.getRowNum() == 0) {
							continue; // just skip the rows if row number is 0 or 1
						}

						if (workbookRead.getSheetName(j).equals("Objetivos")) {
							tmpTblObjetivoNew.setId(i);
							tmpTblObjetivoNew.setClave(row.getCell(0).getStringCellValue());
							tmpTblObjetivoNew.setDescripcion(row.getCell(1).getStringCellValue());

							tmpTblObjetivoService.add(tmpTblObjetivoNew);
						} // if

						if (workbookRead.getSheetName(j).equals("Estrategias")) {
							tmpTblEstrategiaNew.setId(i);
							tmpTblEstrategiaNew.setClaveEstrategia(row.getCell(0).getStringCellValue());
							tmpTblEstrategiaNew.setDescripcion(row.getCell(1).getStringCellValue());
							tmpTblEstrategiaNew.setClaveObjetivo(row.getCell(2).getStringCellValue());

							tmpTblEstrategiaService.add(tmpTblEstrategiaNew);
						} // if

						if (workbookRead.getSheetName(j).equals("Lineas_Accion")) {
							tmpTblLineaAccionNew.setId(i);
							tmpTblLineaAccionNew.setClaveLineaAccion(row.getCell(0).getStringCellValue());
							tmpTblLineaAccionNew.setDescripcion(row.getCell(1).getStringCellValue());
							tmpTblLineaAccionNew.setClaveEstrategia(row.getCell(2).getStringCellValue());

							tmpTblLineaAccionService.add(tmpTblLineaAccionNew);
						} // if

						if (workbookRead.getSheetName(j).equals("Dependencias")) {
							tmpDependenciaNew.setId(i);
							tmpDependenciaNew.setClaveLineaAccion(row.getCell(0).getStringCellValue());
							tmpDependenciaNew.setClaveDependencia(row.getCell(1).getStringCellValue());
							tmpDependenciaNew.setDescripcion(row.getCell(2).getStringCellValue());
							tmpDependenciaNew.setRol(row.getCell(3).getStringCellValue());
							// tmpDependenciaNew.setRol(row.getCell(2).getStringCellValue());

							System.out.println("************ Id : " + tmpDependenciaNew.getId());
							System.out
									.println("************ LineDeAccion : " + tmpDependenciaNew.getClaveLineaAccion());
							System.out.println("************ Dependencia : " + tmpDependenciaNew.getClaveDependencia());
							System.out.println("************ Rol : " + tmpDependenciaNew.getRol());
							tpmDependenciaService.add(tmpDependenciaNew);

							List<CatDependencia> dependencia = service
									.getCatDependenciaByClave(row.getCell(1).getStringCellValue());
							if (dependencia.size() > 0) {

							} else {
								depFaltantes = depFaltantes + " , " + row.getCell(1).getStringCellValue();
							}
						} // if
					} // for 2
				} // for 1
//                    LOG.debug("************************ " + depFaltantes);
			}
			respuesta.setClave("200");
			respuesta.setMsg("Proceso Correcto");
		} catch (Exception e) {
			respuesta.setClave("0");
			respuesta.setMsg(
					"Problemas al subir el archivo en la clase FileUploadController.java, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

}