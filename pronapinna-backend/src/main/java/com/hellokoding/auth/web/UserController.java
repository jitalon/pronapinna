package com.hellokoding.auth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.Principal;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.hellokoding.auth.integracion.service.TblUsuarioService;
import com.hellokoding.auth.modelo.entities.TblUsuario;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {


    @Autowired 
    TblUsuarioService tblUsuarioService;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    
    @GetMapping("/login")
    public String login(
    		@RequestParam(value="error",required=false) String error,
    		@RequestParam(value="logout",required=false) String logout,
    		Model model, 
    		Principal principal, 
    		RedirectAttributes flash) {
    	
    	if(principal!=null) {
    		flash.addFlashAttribute("info","Ya existe una sesión iniciada");
    		return "redirect:/";
    	}
    	
    	if(error!=null) {
    		model.addAttribute("error", "El usuario o clave de acceso son inv&aacute;lidas.");
    	}
    	
    	if(logout!=null) {
    		model.addAttribute("message", "Has cerrado sesión correctamente.");
    	}
    	return "login";
    }

//    @GetMapping("/login")
//    public String login(Model model, String error, String logout) {
//        LOG.debug("****************************************** Login debug message " + error);
//        if (error != null)
//            model.addAttribute("error", "El usuario o clave de acceso son inv&aacute;lidas.");
//
//        if (logout != null)
//            model.addAttribute("message", "Has cerrado sesión correctamente.");
//
//        return "login";
//    }
//
//    @GetMapping({"/", "/index"})
//    public String index(Model model,HttpServletRequest request) {
//        LOG.debug("****************************************** Index Inicio ");
//
//        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String name = user.getUsername();
//
//        HttpSession session = request.getSession();
//        ObjectMapper Obj = new ObjectMapper(); 
//        TblUsuario tblUsuario = null;
//                   tblUsuario = tblUsuarioService.getTblUsuarioByUserName(name);        
//  
//        session.setAttribute("userData", tblUsuario);
//
//        return "index";
//    }
}