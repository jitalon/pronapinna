package com.hellokoding.auth.web;

import com.hellokoding.auth.integracion.service.CatalogosService;
import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.integracion.service.CatPerfilService;
import com.hellokoding.auth.integracion.service.CatPermisoService;
import com.hellokoding.auth.integracion.service.CatPeriodoService;
import com.hellokoding.auth.integracion.service.TblPeriodoService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalService;
import com.hellokoding.auth.integracion.service.TblObjetivoService;
import com.hellokoding.auth.integracion.service.TblEstrategiaService;
import com.hellokoding.auth.integracion.service.TblLineaAccionService;
import com.hellokoding.auth.integracion.service.CatMunicipioService;
import com.hellokoding.auth.integracion.service.CatEstadoService;
import com.hellokoding.auth.integracion.service.CatTipoRespuestaService;
import com.hellokoding.auth.integracion.service.CatDerechoService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalAnualService;
import com.hellokoding.auth.integracion.service.OprActividadService;
import com.hellokoding.auth.integracion.service.CatClasificacionEdadService;
import com.hellokoding.auth.integracion.service.CatPoblacionService;
import com.hellokoding.auth.integracion.service.CatCategoriaService;
import com.hellokoding.auth.integracion.service.CatActividadService;
import com.hellokoding.auth.integracion.service.TblMensajeService;
import com.hellokoding.auth.integracion.service.repositorio.OprBitacoraRepository;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import com.hellokoding.auth.modelo.entities.CatDependencia;
import java.util.List;
import java.util.logging.Logger;
import java.util.Collection;

@RestController
@RequestMapping("/api/catalogos")
public class CatalogosRestController {

//	private static final Logger LOG = LoggerFactory.getLogger(CatalogosController.class);
	private Logger logger = Logger.getAnonymousLogger();

	@Autowired
	OprBitacoraRepository oprBitacoraRepository;

	@Autowired
	CatDependenciaService service;

	@Autowired
	CatPerfilService catPerfilService;

	@Autowired
	CatPermisoService catPermisoService;

	@Autowired
	CatPeriodoService catPeriodoService;

	@Autowired
	TblPeriodoService tblPeriodoService;

	@Autowired
	CatPoblacionService catPoblacionService;

	@Autowired
	CatCategoriaService catCategoriaService;

	@Autowired
	CatActividadService catActividadService;

	@Autowired
	CatDerechoService catDerechoService;

	@Autowired
	CatClasificacionEdadService catClasificacionEdadService;

	@Autowired
	TblPlanNacionalService tblPlanNacionalService;

	@Autowired
	TblEstrategiaService tblEstrategiaService;

	@Autowired
	TblObjetivoService tblObjetivoService;

	@Autowired
	TblLineaAccionService tblLineaAccionService;

	@Autowired
	CatMunicipioService catMunicipioService;

	@Autowired
	CatEstadoService catEstadoService;

	@Autowired
	CatTipoRespuestaService catTipoRespuestaService;

	@Autowired
	TblPlanNacionalAnualService tblPlanNacionalAnualService;

	@Autowired
	OprActividadService oprActividadService;

	@Autowired
	TblMensajeService tblMensajeService;

	@Autowired
	CatalogosService graficaService;

	@Autowired
	CatDependenciaService catDependenciaService;
	
	@GetMapping(value = "/Dependencia.do")
	public List<CatDependencia> consulta(/*HttpServletRequest request*/) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		
//		if(auth!=null) {
//			logger.info("Hola usuario authenticado, tu username es: ".concat(auth.getName()));
//		}
//		
//		if(hasRole("OPERADOR")) {
//			logger.info("Hola usuario ".concat(auth.getName()).concat(" tienes acceso"));
//		}else {
//			logger.info("Hola usuario ".concat(auth.getName()).concat(" no tienes acceso"));
//		}
//		
//		SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request, "");
//		if(securityContext.isUserInRole("OPERADOR")) {
//			logger.info("awardRequest Hola usuario ".concat(auth.getName()).concat(" tienes acceso"));
//		}else {
//			logger.info("awardRequest Hola usuario ".concat(auth.getName()).concat(" no tienes acceso"));
//		}
		return catDependenciaService.getCatDependencia();
	}
	
	private boolean hasRole(String role) {
		SecurityContext context = SecurityContextHolder.getContext();
		
		if(context==null) {
			return false;
		}
		
		Authentication auth = context.getAuthentication();
		
		if(auth==null) {
			return false;
		}
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
		
		return authorities.contains(new SimpleGrantedAuthority(role));
	}
}