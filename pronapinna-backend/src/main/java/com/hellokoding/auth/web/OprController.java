package com.hellokoding.auth.web;

//import com.hellokoding.auth.integracion.service.CatalogosService;
import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.integracion.service.CatPerfilService;
import com.hellokoding.auth.integracion.service.CatPermisoService;
import com.hellokoding.auth.integracion.service.CatPeriodoService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalService;
import com.hellokoding.auth.integracion.service.TblObjetivoService;
import com.hellokoding.auth.integracion.service.TblEstrategiaService;
import com.hellokoding.auth.integracion.service.TblLineaAccionService;
import com.hellokoding.auth.integracion.service.CatMunicipioService;
import com.hellokoding.auth.integracion.service.CatEstadoService;
import com.hellokoding.auth.integracion.service.CatTipoRespuestaService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalAnualService;
import com.hellokoding.auth.integracion.service.OprActividadService;
import com.hellokoding.auth.integracion.service.OprBitacoraService;
import com.hellokoding.auth.integracion.service.CatClasificacionEdadService;
import com.hellokoding.auth.integracion.service.CatPoblacionService;
import com.hellokoding.auth.integracion.service.CatActividadService;

import com.hellokoding.auth.integracion.service.repositorio.OprBitacoraRepository;

import java.math.BigDecimal;
//import java.util.Enumeration;
//import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hellokoding.auth.objects.Respuesta;
import com.hellokoding.auth.objects.TmpLoadFile;
import com.hellokoding.auth.objects.Filtros;
import com.hellokoding.auth.objects.Notificacion;

//import org.json.JSONArray;
import org.json.JSONObject;

import com.hellokoding.auth.modelo.entities.CatDependencia;
//import com.hellokoding.auth.modelo.entities.CatPerfil;
//import com.hellokoding.auth.modelo.entities.CatPermiso;
//import com.hellokoding.auth.modelo.entities.CatalogosGral;
import com.hellokoding.auth.modelo.entities.TblPlanNacional;
import com.hellokoding.auth.modelo.entities.TblUsuario;
//import com.hellokoding.auth.modelo.entities.TblObjetivo;
//import com.hellokoding.auth.modelo.entities.TblEstrategia;
import com.hellokoding.auth.modelo.entities.TblLineaAccion;
import com.hellokoding.auth.modelo.entities.CatEstado;
import com.hellokoding.auth.modelo.entities.CatMunicipio;
import com.hellokoding.auth.modelo.entities.CatTipoRespuesta;
import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.modelo.entities.OprActividad;
import com.hellokoding.auth.modelo.entities.OprActividadCreate;
import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.modelo.entities.CatPeriodo;
import com.hellokoding.auth.modelo.entities.CatActividad;
import com.hellokoding.auth.modelo.entities.CatDerecho;
//import com.hellokoding.auth.modelo.entities.CatClasificacionEdad;
//import com.hellokoding.auth.modelo.entities.CatPoblacion;
import com.hellokoding.auth.modelo.entities.CatCategoria;
import com.hellokoding.auth.modelo.entities.OprPoblacionObjetivo;

import com.hellokoding.auth.utils.EnvioNotificacion;

//import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes("user")
public class OprController {

	private Logger logger = Logger.getAnonymousLogger();
//	private static final Logger LOG = LoggerFactory.getLogger(OprController.class);

	@Autowired
	CatDependenciaService service;

	@Autowired
	CatPerfilService catPerfilService;

	@Autowired
	CatPermisoService catPermisoService;

	@Autowired
	CatPeriodoService catPeriodoService;

	@Autowired
	CatPoblacionService catPoblacionService;

	@Autowired
	CatActividadService catActividadService;

	@Autowired
	CatClasificacionEdadService catClasificacionEdadService;

	@Autowired
	TblPlanNacionalService tblPlanNacionalService;

	@Autowired
	TblEstrategiaService tblEstrategiaService;

	@Autowired
	TblObjetivoService tblObjetivoService;

	@Autowired
	TblLineaAccionService tblLineaAccionService;

	@Autowired
	CatMunicipioService catMunicipioService;

	@Autowired
	CatEstadoService catEstadoService;

	@Autowired
	CatTipoRespuestaService catTipoRespuestaService;

	@Autowired
	TblPlanNacionalAnualService tblPlanNacionalAnualService;

	@Autowired
	OprActividadService oprActividadService;

	@Autowired
	OprBitacoraService oprBitacoraService;

	@Autowired
	OprBitacoraRepository oprBitacoraRepository;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@RequestMapping(value = "/accion/modifica/informacion/updateEstatusTblPlanNacional.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> updateEstatusTblPlanNacional(HttpServletRequest request) throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();

		tblPlanNacionalService.actualizaEstatusTblPlanNacional();

		respuesta.setClave("GUARDADO");
		respuesta.setMsg("OK");

		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/cntTblPlanNacionalActivo.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> cntTblPlanNacionalActivo(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;

		Integer cnt = tblPlanNacionalService.planNacionalActivo();
		return new ResponseEntity<>(cnt, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/cntPlanNacionalAnualCreado.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> cntPlanNacionalAnualCreado(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		Integer cnt = 0;
		JSONObject jsonObj = null;

		try {

			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				Date date1 = null;
				Date date2 = null;
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

				date1 = simpleDateFormat.parse(jsonObj.get("fechaInicio").toString());
				date2 = simpleDateFormat.parse(jsonObj.get("fechaFin").toString());

				cnt = tblPlanNacionalAnualService.planNacionalAnualCreado(date1, date2);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
		}

		return new ResponseEntity<>(cnt, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/tblPlanNacional.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblPlanNacional(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;

		List<TblPlanNacional> tblPlanNacional = tblPlanNacionalService.getTblPlanNacional();
		return new ResponseEntity<>(tblPlanNacional, status);
	}

	@RequestMapping(value = "/accion/guardar/tblPlanNacional.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblPlanNacionalSave(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				TblPlanNacional tblPlanNacionalNew = new TblPlanNacional();
				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					tblPlanNacionalNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				if (jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty())
					tblPlanNacionalNew.setClave(jsonObj.get("clave").toString());
				if (jsonObj.has("documento") && !jsonObj.get("documento").toString().isEmpty())
					tblPlanNacionalNew.setDocumento(jsonObj.get("documento").toString());
				if (jsonObj.has("fechaInicio") && !jsonObj.get("fechaInicio").toString().isEmpty())
					tblPlanNacionalNew.setFechaInicio(Integer.parseInt(jsonObj.get("fechaInicio").toString()));
				if (jsonObj.has("fechaFin") && !jsonObj.get("fechaFin").toString().isEmpty())
					tblPlanNacionalNew.setFechaFin(Integer.parseInt(jsonObj.get("fechaFin").toString()));
				if (jsonObj.has("idEstatus") && !jsonObj.get("idEstatus").toString().isEmpty())
					tblPlanNacionalNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				tblPlanNacionalNew.setFechaCarga(new Date());

				TblUsuario tblUsuario = new TblUsuario();
				tblUsuario.setId(1);

				tblPlanNacionalNew.setTblUsuario(tblUsuario);

				tblPlanNacionalService.add(tblPlanNacionalNew);

				tblPlanNacionalService.insertObjetos(jsonObj.get("clave").toString());
			}
		} catch (Exception e) {
//			LOG.debug(e.getMessage());
			logger.log(Level.SEVERE, "Error", e);
		}

		respuesta.setClave("GUARDADO");
		respuesta.setMsg("OK");
		return new ResponseEntity<>(respuesta, status);

	}

	@RequestMapping(value = "/accion/guardar/oprPlanNacional.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> oprPlanNacionalSave(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				tblPlanNacionalService.crearOprPlanNacional(jsonObj.get("fechaInicio").toString(),
						jsonObj.get("fechaFin").toString());
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}

		respuesta.setClave("GUARDADO");
		respuesta.setMsg("OK");
		return new ResponseEntity<>(respuesta, status);

	}

	@RequestMapping(value = "/accion/consulta/informacion/tblPlanNacionalAnual.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblPlanNacionalAnual(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblPlanNacionalAnual> tblPlanNacionalAnual = null;
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				TblUsuario tblUsuario = (TblUsuario) request.getSession().getAttribute("usuario");
				if (tblUsuario.getCatPerfil().getClave().equals("ADMINISTRADOR")) {
					// if(tblUsuario.getCatPerfil().getClave().equals("VERIFICADOR") ||
					// tblUsuario.getCatPerfil().getClave().equals("AUTORIZADOR")){
					tblPlanNacionalAnual = tblPlanNacionalAnualService.getTblPlanNacionalAnual();
				} else {
					tblPlanNacionalAnual = tblPlanNacionalAnualService
							.getTblPlanNacionalAnual(Integer.parseInt(jsonObj.get("id").toString()));
				}
			}
		} catch (Exception e) {

		}

		return new ResponseEntity<>(tblPlanNacionalAnual, status);
	}

	@RequestMapping(value = "/accion/guardar/tblPlanNacionalAnual.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> savetblPlanNacionalAnual(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				TblPlanNacionalAnual tblPlanNacionalAnualNew = new TblPlanNacionalAnual();
				tblPlanNacionalAnualNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				CatTipoRespuesta catTipoRespuesta = new CatTipoRespuesta();
				catTipoRespuesta
						.setId(Integer.parseInt(jsonObj.getJSONObject("catTipoRespuesta").get("id").toString()));
				tblPlanNacionalAnualNew.setCatTipoRespuesta(catTipoRespuesta);
				TblLineaAccion tblLineaAccion = new TblLineaAccion();
				tblLineaAccion.setId(Integer.parseInt(jsonObj.getJSONObject("tblLineaAccion").get("id").toString()));
				tblPlanNacionalAnualNew.setTblLineaAccion(tblLineaAccion);
				tblPlanNacionalAnualNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				List<CatDependencia> lstCatDependenciaCoodinador = new ArrayList<CatDependencia>();

				for (int i = 0; i < jsonObj.getJSONArray("catDependenciaCoodinador").length(); i++) {
					CatDependencia catDependenciaCoodinador = new CatDependencia();
					catDependenciaCoodinador.setId(Integer.parseInt(
							jsonObj.getJSONArray("catDependenciaCoodinador").getJSONObject(i).get("id").toString()));
					lstCatDependenciaCoodinador.add(catDependenciaCoodinador);
				}
				tblPlanNacionalAnualNew.setCatDependenciaCoodinador(lstCatDependenciaCoodinador);

				List<CatDependencia> lstCatDependenciaCoodinadora = new ArrayList<CatDependencia>();

				for (int i = 0; i < jsonObj.getJSONArray("catDependenciaCoodinadora").length(); i++) {
					CatDependencia catDependenciaCoodinadora = new CatDependencia();
					catDependenciaCoodinadora.setId(Integer.parseInt(
							jsonObj.getJSONArray("catDependenciaCoodinadora").getJSONObject(i).get("id").toString()));
					lstCatDependenciaCoodinadora.add(catDependenciaCoodinadora);
				}
				tblPlanNacionalAnualNew.setCatDependenciaCoodinadora(lstCatDependenciaCoodinadora);

				Date date = null;
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

				date = simpleDateFormat.parse(jsonObj.get("fechaInicio").toString());
				tblPlanNacionalAnualNew.setFechaInicio(date);
				date = simpleDateFormat.parse(jsonObj.get("fechaFin").toString());
				tblPlanNacionalAnualNew.setFechaFin(date);

				tblPlanNacionalAnualService.add(tblPlanNacionalAnualNew);

			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}

		respuesta.setClave("GUARDADO");
		respuesta.setMsg("OK");
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/oprActividadFiltro.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> oprActividad(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		JSONObject jsonObj = null;
		List<OprActividad> oprActividad = null;
		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);
				oprActividad = oprActividadService.findActividad(
						Integer.parseInt(jsonObj.get("idPlanNacionalAnual").toString()),
						Integer.parseInt(jsonObj.get("idActividad").toString()));
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
		return new ResponseEntity<>(oprActividad, status);
	}

	@RequestMapping(value = "/accion/guardar/oprActividad.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveOprActividad(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;

		JSONObject jsonObj = null;
		OprActividad opr = new OprActividad();

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				OprActividad oprActividadNew = new OprActividad();

				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					oprActividadNew.setId(Integer.parseInt(jsonObj.get("id").toString()));

				if (jsonObj.has("catActividad") && !jsonObj.isNull("catActividad")) {
					CatActividad catActividad = new CatActividad();
					catActividad.setId(Integer.parseInt(jsonObj.getJSONObject("catActividad").get("id").toString()));
					oprActividadNew.setCatActividad(catActividad);
				}

				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					oprActividadNew.setDescripcion(jsonObj.get("descripcion").toString());
				if (jsonObj.has("identificacion") && !jsonObj.get("identificacion").toString().isEmpty())
					oprActividadNew.setIdentificacion(jsonObj.get("identificacion").toString());
				if (jsonObj.has("unidadResponsable") && !jsonObj.get("unidadResponsable").toString().isEmpty())
					oprActividadNew.setUnidadResponsable(jsonObj.get("unidadResponsable").toString());

				TblPlanNacionalAnual tblPlanNacionalAnual = new TblPlanNacionalAnual();
				tblPlanNacionalAnual
						.setId(Integer.parseInt(jsonObj.getJSONObject("tblPlanNacionalAnual").get("id").toString()));

				oprActividadNew.setTblPlanNacionalAnual(tblPlanNacionalAnual);

				if (jsonObj.has("catPeriodoRealizacion") && !jsonObj.isNull("catPeriodoRealizacion")) {
					CatPeriodo catPeriodoRealizacion = new CatPeriodo();
					catPeriodoRealizacion.setId(
							Integer.parseInt(jsonObj.getJSONObject("catPeriodoRealizacion").get("id").toString()));
					oprActividadNew.setCatPeriodoRealizacion(catPeriodoRealizacion);
				}
				if (jsonObj.has("catPeriodoProgramacion") && !jsonObj.isNull("catPeriodoProgramacion")) {
					CatPeriodo catPeriodoProgramacion = new CatPeriodo();
					catPeriodoProgramacion.setId(
							Integer.parseInt(jsonObj.getJSONObject("catPeriodoProgramacion").get("id").toString()));
					oprActividadNew.setCatPeriodoProgramacion(catPeriodoProgramacion);
				}

				if (jsonObj.has("resultado") && !jsonObj.get("resultado").toString().isEmpty())
					oprActividadNew.setResultado(jsonObj.get("resultado").toString());
				if (jsonObj.has("observacion") && !jsonObj.get("observacion").toString().isEmpty())
					oprActividadNew.setObservacion(jsonObj.get("observacion").toString());
				if (jsonObj.has("cobertura") && !jsonObj.get("cobertura").toString().isEmpty())
					oprActividadNew.setCobertura(jsonObj.get("cobertura").toString());
				if (jsonObj.has("importeAsignado") && !jsonObj.get("importeAsignado").toString().isEmpty() && jsonObj.get("importeAsignado").toString()!="null")
					oprActividadNew.setImporteAsignado(new BigDecimal(jsonObj.get("importeAsignado").toString()));
				oprActividadNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				String dateStr = jsonObj.get("fecha").toString();
				String s = dateStr.replace("T", " ");
				s = s.substring(0, 19);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(s));

				oprActividadNew.setFecha(c.getTime());

				if (jsonObj.has("catMunicipio") && !jsonObj.isNull("catMunicipio")) {
					List<CatMunicipio> lstCatMunicipio = new ArrayList<CatMunicipio>();

					for (int i = 0; i < jsonObj.getJSONArray("catMunicipio").length(); i++) {
						CatMunicipio catMunicipio = new CatMunicipio();
						catMunicipio.setId(Integer
								.parseInt(jsonObj.getJSONArray("catMunicipio").getJSONObject(i).get("id").toString()));
						lstCatMunicipio.add(catMunicipio);
					}
					oprActividadNew.setCatMunicipio(lstCatMunicipio);
				}

				if (jsonObj.has("catEstado") && !jsonObj.isNull("catEstado")) {
					List<CatEstado> lstCatEstado = new ArrayList<CatEstado>();

					for (int i = 0; i < jsonObj.getJSONArray("catEstado").length(); i++) {
						CatEstado catEstado = new CatEstado();
						catEstado.setId(Integer
								.parseInt(jsonObj.getJSONArray("catEstado").getJSONObject(i).get("id").toString()));
						lstCatEstado.add(catEstado);
					}
					oprActividadNew.setCatEstado(lstCatEstado);
				}

				if (jsonObj.has("oprPoblacionObjetivo") && !jsonObj.isNull("oprPoblacionObjetivo")) {
					List<OprPoblacionObjetivo> lstOprPoblacionObjetivo = new ArrayList<OprPoblacionObjetivo>();

					for (int i = 0; i < jsonObj.getJSONArray("oprPoblacionObjetivo").length(); i++) {

						ObjectMapper mapper = new ObjectMapper();
						OprPoblacionObjetivo oprPoblacionObjetivo = mapper.readValue(
								jsonObj.getJSONArray("oprPoblacionObjetivo").getJSONObject(i).toString(),
								OprPoblacionObjetivo.class);

						lstOprPoblacionObjetivo.add(oprPoblacionObjetivo);
					}
					oprActividadNew.setOprPoblacionObjetivo(lstOprPoblacionObjetivo);
				}

				if (jsonObj.has("catCategoria") && !jsonObj.isNull("catCategoria")) {
					CatCategoria catCategoria = new CatCategoria();
					catCategoria.setId(Integer.parseInt(jsonObj.getJSONObject("catCategoria").get("id").toString()));
					oprActividadNew.setCatCategoria(catCategoria);
				}

				if (jsonObj.has("catDerecho") && !jsonObj.isNull("catDerecho")) {
					List<CatDerecho> lstCatDerecho = new ArrayList<CatDerecho>();

					for (int i = 0; i < jsonObj.getJSONArray("catDerecho").length(); i++) {
						CatDerecho catDerecho = new CatDerecho();
						catDerecho.setId(Integer
								.parseInt(jsonObj.getJSONArray("catDerecho").getJSONObject(i).get("id").toString()));
						lstCatDerecho.add(catDerecho);
					}
					oprActividadNew.setCatDerecho(lstCatDerecho);
				}

				opr = oprActividadService.add(oprActividadNew);

				TblUsuario tblUsuario = (TblUsuario) request.getSession().getAttribute("usuario");

				OprBitacora oprBitacora = new OprBitacora();
				oprBitacora.setId(0);
				oprBitacora.setTblUsuario(tblUsuario);
				oprBitacora.setOprActividad(opr);
				oprBitacora.setAccion("3");
				oprBitacora.setIdEstatus(1);
				oprBitacora.setMensaje("Se capturan resultados");
				oprBitacora.setFecha(Calendar.getInstance().getTime());
				this.guardaBitacora(oprBitacora);

			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}

		return new ResponseEntity<>(opr, status);
	}

	@RequestMapping(value = "/accion/guardar/oprActividadCreate.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> createOprActividad(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;

		JSONObject jsonObj = null;
		OprActividadCreate opr = new OprActividadCreate();

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				OprActividadCreate oprActividadNew = new OprActividadCreate();

				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					oprActividadNew.setId(Integer.parseInt(jsonObj.get("id").toString()));

				if (jsonObj.has("catActividad") && !jsonObj.isNull("catActividad")) {
					CatActividad catActividad = new CatActividad();
					catActividad.setId(Integer.parseInt(jsonObj.getJSONObject("catActividad").get("id").toString()));
					oprActividadNew.setCatActividad(catActividad);
				}

				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					oprActividadNew.setDescripcion(jsonObj.get("descripcion").toString());
				if (jsonObj.has("identificacion") && !jsonObj.get("identificacion").toString().isEmpty())
					oprActividadNew.setIdentificacion(jsonObj.get("identificacion").toString());

				TblPlanNacionalAnual tblPlanNacionalAnual = new TblPlanNacionalAnual();
				tblPlanNacionalAnual
						.setId(Integer.parseInt(jsonObj.getJSONObject("tblPlanNacionalAnual").get("id").toString()));

				oprActividadNew.setTblPlanNacionalAnual(tblPlanNacionalAnual);

				if (jsonObj.has("catPeriodoProgramacion") && !jsonObj.isNull("catPeriodoProgramacion")) {
					CatPeriodo catPeriodoProgramacion = new CatPeriodo();
					catPeriodoProgramacion.setId(
							Integer.parseInt(jsonObj.getJSONObject("catPeriodoProgramacion").get("id").toString()));
					oprActividadNew.setCatPeriodoProgramacion(catPeriodoProgramacion);
				}

				if (jsonObj.has("catCategoria") && !jsonObj.isNull("catCategoria")) {
					CatCategoria catCategoria = new CatCategoria();
					catCategoria.setId(Integer.parseInt(jsonObj.getJSONObject("catCategoria").get("id").toString()));
					oprActividadNew.setCatCategoria(catCategoria);
				}

				if (jsonObj.has("cobertura") && !jsonObj.get("cobertura").toString().isEmpty())
					oprActividadNew.setCobertura(jsonObj.get("cobertura").toString());
				oprActividadNew.setIdEstatus(1);

				if (jsonObj.has("catDerecho") && !jsonObj.isNull("catDerecho")) {
					List<CatDerecho> lstCatDerecho = new ArrayList<CatDerecho>();

					for (int i = 0; i < jsonObj.getJSONArray("catDerecho").length(); i++) {
						CatDerecho catDerecho = new CatDerecho();
						catDerecho.setId(Integer
								.parseInt(jsonObj.getJSONArray("catDerecho").getJSONObject(i).get("id").toString()));
						lstCatDerecho.add(catDerecho);
					}
					oprActividadNew.setCatDerecho(lstCatDerecho);
				}

				oprActividadNew.setFecha(Calendar.getInstance().getTime());

				opr = oprActividadService.addCreate(oprActividadNew);

				TblUsuario tblUsuario = (TblUsuario) request.getSession().getAttribute("usuario");

				OprBitacora oprBitacora = new OprBitacora();
				oprBitacora.setId(0);
				oprBitacora.setTblUsuario(tblUsuario);
				OprActividad tmp = new OprActividad();
				tmp.setId(opr.getId());
				oprBitacora.setOprActividad(tmp);
				oprBitacora.setAccion("1");
				oprBitacora.setIdEstatus(1);
				oprBitacora.setMensaje("Se crea actividad");
				oprBitacora.setFecha(Calendar.getInstance().getTime());
				this.guardaBitacora(oprBitacora);

			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}

		return new ResponseEntity<>(opr, status);
	}

	@RequestMapping(value = "/accion/consulta/registro/oprBitacora.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> oprBitacora(HttpServletRequest request) {
		return this.oprBitacoraBnd(request, true);
	}

	public ResponseEntity<?> oprBitacoraBnd(HttpServletRequest request, boolean bnd) {
		HttpStatus status = HttpStatus.OK;
		Filtros filtros = new Filtros();
		List<Notificacion> lstNotificacion = new ArrayList<Notificacion>();
		List<OprBitacora> lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.oprBitacoraFilters(filtros, bnd);
		for (OprBitacora oprBitacora : lstOprBitacora) {
			Notificacion notificacion = new Notificacion();
			
			if(oprBitacora.getTblUsuario()!=null) {
				notificacion.setDependencia(oprBitacora.getTblUsuario().getCatDependencia().getClave());
				notificacion.setDependenciaDescripcion(oprBitacora.getTblUsuario().getCatDependencia().getDescripcion());
				notificacion.setUsuario(oprBitacora.getTblUsuario().getNombre() + " "
						+ oprBitacora.getTblUsuario().getApellidoPaterno() + " "
						+ oprBitacora.getTblUsuario().getApellidoMaterno());
			}
				
			
			notificacion.setClaveAccion(
					oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getClave());
			notificacion.setAccionPuntual(
					oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getDescripcion());
			notificacion.setClaveEst(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
					.getTblEstrategia().getClave());
			notificacion.setEstrategia(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
					.getTblEstrategia().getDescripcion());
			notificacion.setClaveObj(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
					.getTblEstrategia().getTblObjetivo().getClave());
			notificacion.setObjetivo(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
					.getTblEstrategia().getTblObjetivo().getDescripcion());
			notificacion.setActividad(oprBitacora.getOprActividad().getCatActividad().getDescripcion());
			notificacion.setComentario(oprBitacora.getMensaje());
			String accion = "";
			switch (oprBitacora.getAccion()) {
			case "1":
				accion = "Captura";
				break;
			case "2":
				accion = "Validacion";
				break;
			case "3":
				accion = "Resultados";
				break;
			case "4":
				accion = "Autorizacion";
				break;
			default:
				accion = "Concluida";
				break;
			}
			notificacion.setEstatus(accion);
			notificacion.setFecha(oprBitacora.getFecha());
			
			notificacion.setIdActividad(oprBitacora.getOprActividad().getCatActividad().getId());
			notificacion.setIdPlanNacionalAnual(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getId());
			if (null != oprBitacora.getOprActividad().getTblPlanNacionalAnual().getCatTipoRespuesta())
				notificacion.setTipoRespuesta(
						oprBitacora.getOprActividad().getTblPlanNacionalAnual().getCatTipoRespuesta().getDescripcion());
			lstNotificacion.add(notificacion);
		}
		return new ResponseEntity<>(lstNotificacion, status);
	}

	@RequestMapping(value = "/accion/consulta/registro/oprBitacoraMovimiento.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> oprBitacoraMovimiento(HttpServletRequest request) {
		return this.oprBitacoraBnd(request, false);
	}

	@RequestMapping(value = "/accion/actualiza/estatus/oprActividad/{tblPlanNacionalAnualId}/{oprActividadIdActividad}/{idEstatus}", method = {
			RequestMethod.GET, RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> actualizaEstatusOprActividad(@RequestBody String dataRow, HttpServletRequest request,
			@PathVariable int tblPlanNacionalAnualId, @PathVariable int oprActividadIdActividad,
			@PathVariable int idEstatus) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);
				String mensaje = jsonObj.get("mensaje").toString();

				List<OprActividad> oprActividad = oprActividadService.findActividad(tblPlanNacionalAnualId,
						oprActividadIdActividad);
				OprActividad opr = oprActividad.get(0);
				opr.setIdEstatus(idEstatus);
				opr = oprActividadService.add(opr);

				TblUsuario tblUsuario = (TblUsuario) request.getSession().getAttribute("usuario");

				OprBitacora oprBitacora = new OprBitacora();
				oprBitacora.setId(0);
				oprBitacora.setTblUsuario(tblUsuario);
				oprBitacora.setOprActividad(opr);
				oprBitacora.setAccion("" + idEstatus);
				oprBitacora.setMensaje(mensaje);
				oprBitacora.setIdEstatus(1);
				oprBitacora.setFecha(Calendar.getInstance().getTime());

				this.guardaBitacora(oprBitacora);

			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}

		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/notificacion/envio.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> enviaNotificacion(@RequestBody String dataRow, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				String mensaje = jsonObj.get("mensaje").toString();
				String email = jsonObj.get("email").toString();
				int estatus = Integer.parseInt(jsonObj.get("estatus").toString());

				EnvioNotificacion.Envio(estatus, mensaje, email);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
		return new ResponseEntity<>(respuesta, status);
	};

	@RequestMapping(value = "/accion/consulta/informacion/tmpLoadfile.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tmpLoadfile(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TmpLoadFile> lstTmpLoadFile = new ArrayList<TmpLoadFile>();
		try {

			lstTmpLoadFile = tblPlanNacionalService.tmpLoadFile();

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
		return new ResponseEntity<>(lstTmpLoadFile, status);
	};

	private void guardaBitacora(OprBitacora oprBitacora) {
		oprBitacoraService.add(oprBitacora);
	}

	@RequestMapping(value = "/accion/consulta/bitacoraFiltro.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> excelBitacoraReportFilters(@RequestBody String dataRow, HttpServletRequest request) {

		JSONObject jsonObj = null;
		HttpStatus status = HttpStatus.OK;
		List<OprBitacora> lstOprBitacora = null;
		List<Notificacion> lstNotificacion = new ArrayList<Notificacion>();

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				Filtros filtro = mapper.readValue(jsonObj.toString(), Filtros.class);

				lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.btRevicionFilters(filtro);

				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

				for (OprBitacora oprBitacora : lstOprBitacora) {
					Notificacion notificacion = new Notificacion();
					String depCoordinada = "";
					for (CatDependencia dependenciaCoordinador : oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getCatDependenciaCoodinador()) {
						depCoordinada = depCoordinada + dependenciaCoordinador.getDescripcion();
					}

					notificacion.setDependenciaCoodinador(depCoordinada);

					String depCoordinadora = "";
					for (CatDependencia dependenciaCoordinadora : oprBitacora.getOprActividad()
							.getTblPlanNacionalAnual().getCatDependenciaCoodinadora()) {
						depCoordinadora = depCoordinadora + dependenciaCoordinadora.getDescripcion();
					}

					notificacion.setDependencia(depCoordinadora);

					notificacion.setClaveAccion(
							oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getClave());
					notificacion.setAccionPuntual(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getTblLineaAccion().getDescripcion());
					notificacion.setClaveEst(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getClave());
					notificacion.setEstrategia(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getTblLineaAccion().getTblEstrategia().getDescripcion());

					String msj = " \"Correspondiente al periodo ( ";
					msj = msj + simpleDateFormat
							.format(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getFechaInicio());
					msj = msj + " al " + simpleDateFormat
							.format(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getFechaFin());
					msj = msj + " ) perteneciente al Programa Nacional : "
							+ oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
									.getTblEstrategia().getTblObjetivo().getTblPlanNacional().getClave()
							+ "\"";

					notificacion.setClaveObj(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getTblObjetivo().getClave());
					notificacion.setObjetivo(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getTblObjetivo().getDescripcion());
					notificacion
							.setActividad(oprBitacora.getOprActividad().getCatActividad().getDescripcion() + " " + msj);
					notificacion.setComentario(oprBitacora.getMensaje());
					String accion = "";
					switch (oprBitacora.getAccion()) {
					case "1":
						accion = "Captura";
						break;
					case "2":
						accion = "Validaci\u00f3n";
						break;
					case "3":
						accion = "Resultados";
						break;
					case "4":
						accion = "Autorizaci\u00f3n";
						break;
					default:
						accion = "Concluida";
						break;
					}
					notificacion.setEstatus(accion);
					notificacion.setFecha(oprBitacora.getFecha());
					if(oprBitacora.getTblUsuario()!=null) {
							notificacion.setUsuario(oprBitacora.getTblUsuario().getNombre() + " "
							+ oprBitacora.getTblUsuario().getApellidoPaterno() + " "
							+ oprBitacora.getTblUsuario().getApellidoMaterno());
					}
					notificacion.setIdActividad(oprBitacora.getOprActividad().getCatActividad().getId());
					notificacion
							.setIdPlanNacionalAnual(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getId());
					if (null != oprBitacora.getOprActividad().getTblPlanNacionalAnual().getCatTipoRespuesta())
						notificacion.setTipoRespuesta(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
								.getCatTipoRespuesta().getDescripcion());

					lstNotificacion.add(notificacion);
				}
			}

		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
		}
		return new ResponseEntity<>(lstNotificacion, status);
	}

}