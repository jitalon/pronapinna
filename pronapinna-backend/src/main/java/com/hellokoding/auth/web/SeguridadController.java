package com.hellokoding.auth.web;

import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.integracion.service.CatPerfilService;
import com.hellokoding.auth.integracion.service.TblUsuarioService;
import com.hellokoding.auth.integracion.service.TblUsuarioCreateService;

import java.math.BigDecimal;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hellokoding.auth.objects.Respuesta;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hellokoding.auth.modelo.entities.CatDependencia;
import com.hellokoding.auth.modelo.entities.CatPerfil;
import com.hellokoding.auth.modelo.entities.CatPermiso;
import com.hellokoding.auth.modelo.entities.TblUsuario;
import com.hellokoding.auth.modelo.entities.TblUsuarioCreate;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Controller
@SessionAttributes("user" )
public class SeguridadController {

    private static final Logger LOG = LoggerFactory.getLogger(SeguridadController.class);

        @Autowired 
        CatDependenciaService catDependenciaService;

        @Autowired 
        CatPerfilService catPerfilService;

        @Autowired 
        TblUsuarioService tblUsuarioService;

        @Autowired 
        TblUsuarioCreateService tblUsuarioCreateService;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}
        
        private String hasPassword(String password){

           PasswordEncoder passwordEncoder1 = new BCryptPasswordEncoder();
	   String hashedPassword = passwordEncoder1.encode(password);

          return hashedPassword;
        }

	@RequestMapping(value = "/accion/consulta/seguridad/usuario.do", method = {RequestMethod.GET,RequestMethod.POST}, produces="application/json",consumes="application/json")
	public ResponseEntity<?> consulta(HttpServletRequest request) {

               HttpStatus status = HttpStatus.OK;

               List<TblUsuario> tblUsuario = tblUsuarioService.getTblUsuario();

               return new ResponseEntity<>(tblUsuario, status);
        }

	@RequestMapping(value = "/accion/consulta/seguridad/usuarioByUserName.do", method = {RequestMethod.GET,RequestMethod.POST}, produces="application/json",consumes="application/json")
	public ResponseEntity<?> consultaByUserName(@RequestBody String dataRow, HttpServletRequest request) {

               HttpStatus status = HttpStatus.OK;
               HttpSession session = request.getSession();

               JSONObject jsonObj = null;
               TblUsuario tblUsuario = null;


   	       try{
                   if(null != dataRow){
                      LOG.debug("************** " + dataRow);
                      jsonObj = new JSONObject(dataRow);
                      tblUsuario = tblUsuarioService.getTblUsuarioByUserName(jsonObj.get("username").toString());
                      session.setAttribute("usuario", tblUsuario);
                   } 
               }catch(Exception e){

               }
               return new ResponseEntity<>(tblUsuario, status);
        }

        @RequestMapping(value = "/calatolos/guarda/seguridad/usuario.do",  method = RequestMethod.POST, produces="application/json", consumes="application/json")
	public ResponseEntity<?> save(@RequestBody String dataRow, HttpServletRequest request) throws Exception  {

                HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
                JSONObject jsonObj = null;

		try{
                   if(null != dataRow){
                   LOG.debug("************** " + dataRow);
                   jsonObj = new JSONObject(dataRow);

                   TblUsuarioCreate tblUsuarioNew = new TblUsuarioCreate();
                   if(jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty()) tblUsuarioNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
                   if(jsonObj.has("usuario") && !jsonObj.get("usuario").toString().isEmpty()) tblUsuarioNew.setUsuario(jsonObj.get("usuario").toString()); 
                   if(jsonObj.has("nombre") && !jsonObj.get("nombre").toString().isEmpty()) tblUsuarioNew.setNombre(jsonObj.get("nombre").toString()); 
                   if(jsonObj.has("apellidoPaterno") && !jsonObj.get("apellidoPaterno").toString().isEmpty()) tblUsuarioNew.setApellidoPaterno(jsonObj.get("apellidoPaterno").toString()); 
                   if(jsonObj.has("apellidoMaterno") && !jsonObj.get("apellidoMaterno").toString().isEmpty()) tblUsuarioNew.setApellidoMaterno(jsonObj.get("apellidoMaterno").toString()); 
                   if(jsonObj.has("email") && !jsonObj.get("email").toString().isEmpty()) tblUsuarioNew.setEmail(jsonObj.get("email").toString()); 
                   if(jsonObj.has("telefono") && !jsonObj.get("telefono").toString().isEmpty()) tblUsuarioNew.setTelefono(jsonObj.get("telefono").toString()); 
                   if(jsonObj.has("idEstatus") && !jsonObj.get("idEstatus").toString().isEmpty()) tblUsuarioNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString())); 
                   
                   if(jsonObj.has("catPerfil")){
                     CatPerfil catPerfil = new CatPerfil();
                               catPerfil.setId(Integer.parseInt(jsonObj.getJSONObject("catPerfil").get("id").toString()));
                               catPerfil.setClave(jsonObj.getJSONObject("catPerfil").get("clave").toString());
                               catPerfil.setDescripcion(jsonObj.getJSONObject("catPerfil").get("descripcion").toString());
                     tblUsuarioNew.setCatPerfil(catPerfil);
                   }
                   if(jsonObj.has("catDependencia")){
                     CatDependencia catDependencia = new CatDependencia();
                               catDependencia.setId(Integer.parseInt(jsonObj.getJSONObject("catDependencia").get("id").toString()));
                               catDependencia.setClave(jsonObj.getJSONObject("catDependencia").get("clave").toString());
                               catDependencia.setDescripcion(jsonObj.getJSONObject("catDependencia").get("descripcion").toString());
                     tblUsuarioNew.setCatDependencia(catDependencia);
                   }
                   
                   if(jsonObj.has("password") && !jsonObj.get("password").toString().isEmpty()){
                      LOG.debug("Aqui ------------------");
                      tblUsuarioNew.setPassword(this.hasPassword(jsonObj.get("password").toString()));
                      tblUsuarioCreateService.actualizaPassword(tblUsuarioNew);
                   }else{
                     tblUsuarioNew.setPassword("$2a$10$iUlr.IyYkVT8THc7ggK07evedT1jvImC9zIBPXkTejGF5pfmlkm7m");
                     tblUsuarioCreateService.add(tblUsuarioNew);
                   }                  

                   }
               }catch(Exception e){
                 e.getMessage();
               }

               respuesta.setClave("GUARDADO");
               respuesta.setMsg("OK");
               return new ResponseEntity<>(respuesta, status);
        }

        @RequestMapping(value = "/accion/consulta/seguridad/catPerfil.do", method = {RequestMethod.GET,RequestMethod.POST}, produces="application/json",consumes="application/json")
	public ResponseEntity<?> consultaPerfil(HttpServletRequest request) {

               HttpStatus status = HttpStatus.OK;

               List<CatPerfil> catPerfil = catPerfilService.getCatPerfil();

               return new ResponseEntity<>(catPerfil, status);
        }

        @RequestMapping(value = "/calatolos/guarda/seguridad/perfil.do",  method = RequestMethod.POST, produces="application/json", consumes="application/json")
	public ResponseEntity<?> savePerfil(@RequestBody String dataRow, HttpServletRequest request) throws Exception  {

                HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
                JSONObject jsonObj = null;

		try{
                   if(null != dataRow){
                   LOG.debug("************** " + dataRow);
                   jsonObj = new JSONObject(dataRow);

                   CatPerfil catPerfilNew = new CatPerfil();
                   if(jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty()) catPerfilNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
                   if(jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty()) catPerfilNew.setClave(jsonObj.get("clave").toString()); 
                   if(jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty()) catPerfilNew.setDescripcion(jsonObj.get("descripcion").toString()); 
                   if(jsonObj.has("catPermiso")){
	                    JSONArray jAPermisos = (JSONArray) jsonObj.getJSONArray("catPermiso");
	                    List<CatPermiso> lstCatPermiso = new ArrayList<CatPermiso>();

	   		    for(int i=0;i<jAPermisos.length();i++){
	                        CatPermiso catPermiso = new CatPermiso();
                                catPermiso.setId(Integer.parseInt(jAPermisos.getJSONObject(i).get("id").toString()));
                                catPermiso.setClave(jAPermisos.getJSONObject(i).get("clave").toString());
                                catPermiso.setDescripcion(jAPermisos.getJSONObject(i).get("descripcion").toString());
             		        lstCatPermiso.add(catPermiso);
			    }
                        catPerfilNew.setCatPermiso(lstCatPermiso);                       
                   }

                   catPerfilService.add(catPerfilNew);
                   }
               }catch(Exception e){

               }

               respuesta.setClave("GUARDADO");
               respuesta.setMsg("OK");
               return new ResponseEntity<>(respuesta, status);
        }


}