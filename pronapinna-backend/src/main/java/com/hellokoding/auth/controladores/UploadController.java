package com.hellokoding.auth.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hellokoding.auth.objects.Respuesta;

import com.hellokoding.auth.objects.UploadedFile;

import com.hellokoding.auth.integracion.service.TblArchivoService;
import com.hellokoding.auth.modelo.entities.TblArchivo;
import com.hellokoding.auth.modelo.entities.OprActividad;
import java.util.Optional;

@Controller
public class UploadController {

	private static String UPLOADED_FOLDER = "";
	private static String path_separador = "";
	private Logger logger = Logger.getAnonymousLogger();

	public UploadController() {
		Properties sipinnaSetup = new Properties();
		try {
			sipinnaSetup.load(UploadController.class.getResourceAsStream("/application.properties"));

			UPLOADED_FOLDER = sipinnaSetup.getProperty("path_prin");
			path_separador = sipinnaSetup.getProperty("path_separador");

		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error", e);
		}
	}

	UploadedFile ufile;

	@Autowired
	TblArchivoService tblArchivoService;

	@ResponseBody
	@RequestMapping(value = "/uploadFileActividad", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	public ResponseEntity<?> upload(MultipartHttpServletRequest request, HttpServletResponse response) {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();

		Iterator<String> itr = request.getFileNames();
		int idActividad = Integer.parseInt(request.getParameter("actividad"));
		String descripcion = request.getParameter("descripcion");
		String id = "";

		TblArchivo tblArchivoNew = new TblArchivo();

		System.out.println("id......: " + request.getParameter("id"));

		if (null != request.getParameter("id") && !request.getParameter("id").equals("0")) {
			id = request.getParameter("id");

			Optional<TblArchivo> value = tblArchivoService.getTblArchivoByI(Integer.parseInt(id));
			if (value.isPresent()) {
				tblArchivoNew = value.get();
			}
//			tblArchivoNew = tblArchivoService.getTblArchivoByI(Integer.parseInt(id)).get();
			OprActividad oprActividad = new OprActividad();
			oprActividad.setId(idActividad);
			tblArchivoNew.setOprActividad(oprActividad);
			tblArchivoNew.setDisponible(2);
		} else {
			id = "0";
			tblArchivoNew.setId(Integer.parseInt(id));
			tblArchivoNew.setDescripcion(descripcion);
			OprActividad oprActividad = new OprActividad();
			oprActividad.setId(idActividad);
			tblArchivoNew.setOprActividad(oprActividad);
			tblArchivoNew.setDisponible(2);
		}

		this.CreaDirectorio(idActividad);
		int i = 0;

		while (itr.hasNext()) {
			MultipartFile mpf = request.getFile(itr.next());
			BufferedOutputStream stream = null;
			try {

				System.out.println(
						"************************************* Documento Observaciones Pruebas while " + mpf.getName());
				ufile = new UploadedFile();

				ufile.setLength(mpf.getBytes().length);
				ufile.setBytes(mpf.getBytes());
				ufile.setBaitesArchivo(Base64.encodeBase64String(mpf.getBytes()));
				ufile.setType(mpf.getContentType());
				ufile.setName(mpf.getOriginalFilename());

				byte[] bytes = ufile.getBytes();

				String rootPath = UPLOADED_FOLDER + idActividad + path_separador;

				File dir = new File(rootPath);

				String nombre = mpf.getOriginalFilename().replaceAll(" ", "_");
				File serverFile = new File(dir.getAbsolutePath() + File.separator + nombre);
				stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				if (mpf.getName().equals("file0")) {
					System.out.println("*************************************entro if 0 ");
					tblArchivoNew.setArchivo1(nombre);
					tblArchivoNew.setDisponible(2);
				}
				if (mpf.getName().equals("file1")) {
					System.out.println("*************************************entro if 1 ");
					tblArchivoNew.setArchivo2(nombre);
					tblArchivoNew.setDisponible(1);
				}
				if (mpf.getName().equals("file2")) {
					System.out.println("*************************************entro if 2 ");
					tblArchivoNew.setArchivo3(nombre);
					tblArchivoNew.setDisponible(0);
				}

			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error", e);
			} finally {
				try {
					if (stream != null) {
						stream.close();
					}
				} catch (IOException e) {
				}
			}
			i = i + 1;
		}
		if (i > 0) {
			System.out.println("*************************************entro if insert");
			tblArchivoService.insertArchivo(tblArchivoNew);
		}
		respuesta.setClave("UPLOAD");
		respuesta.setMsg("OK");
		return new ResponseEntity<>(respuesta, status);
	}

	public int CreaDirectorio(int idActividad) {
		int respuesta = 0;
		Path path = Paths.get(UPLOADED_FOLDER + idActividad + path_separador);

		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
				respuesta = 1;
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error", e);
			}
		}
		return respuesta;
	}

}