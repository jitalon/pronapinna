package com.hellokoding.auth.controladores;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DownloadController {

	private static final int BUFFER_SIZE = 1555024;
	private static String UPLOADED_FOLDER = "";

	private Logger logger = Logger.getAnonymousLogger();

	public DownloadController() {
		Properties sipinnaSetup = new Properties();
		try {
			sipinnaSetup.load(DownloadController.class.getResourceAsStream("/application.properties"));
			UPLOADED_FOLDER = sipinnaSetup.getProperty("path_prin");
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error", e);
		}
	}

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@RequestMapping(value = "/descarga/documento/{actividad}/{archivo}", method = RequestMethod.GET)
	public void getFileInTextFormatF(@PathVariable String actividad, @PathVariable String archivo,
			HttpServletRequest request, HttpServletResponse response) {

		ServletContext context = request.getServletContext();
		FileInputStream inputStream = null;
		try {
			String fullPath = UPLOADED_FOLDER + actividad + "//" + archivo;
			File downloadFile = new File(fullPath);
			inputStream = new FileInputStream(downloadFile);

			String mimeType = context.getMimeType(fullPath);
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			outStream.close();

		} catch (Exception e) {

		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
			}
		}
	}

}
