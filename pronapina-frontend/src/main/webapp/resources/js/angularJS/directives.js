

function validarEdad () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.adult = function (modelValue, viewValue) {

                if (control.$isEmpty(modelValue)) {
                    return true;
                }
                
                var age = Number(viewValue);
                
                if (age >= 18 && age <= 100) {
                    return true;
                }
               
                return false; // wrong value
            };
        }
    };
}


angular
  .module("pronappinaApp")
  .directive('adult', validarEdad);
  


