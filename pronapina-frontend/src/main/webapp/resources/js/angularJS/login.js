angular.module("pronappinaApp")
  .controller("AppLoginCtrl", ["$scope", "$rootScope", "$http", function AppLoginCtrl($scope, $rootScope, $http) {
	  $scope.message=null;
	  $scope.error=null;
	  $scope.username=null;
	  $scope.password=null;
	  $(".oculta").removeClass("oculta");
$scope.login = function(){
	
	console.log("login");
	
	console.log("usuario: "+$scope.username);
	console.log("password: "+$scope.password);
	
      $http({
		url: '/pronapinnaback/api/login',
		method: "POST",
		data: {username:$scope.username,password:$scope.password},
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
      }).then(function onSuccess(response) {
    	  window.localStorage.setItem('USER',response.data.user.username);
      	  window.localStorage.setItem('PERFIL',response.data.user.authorities[0]["authority"]);
      	  window.localStorage.setItem('TOKEN',response.data.token);
      	  window.location.href="/pronapinna/#!/home";
      }).catch(function onError(response) {
    	  console.log(response);
    	  $scope.message=response.data.mensaje;
    	  $scope.error=response.data.error;

      }).finally(function() {
	 	
		
      });        

};



}]);
  