angular.module("pronappinaApp")
  .controller("AppRecuperacionCtrl", ["$scope", "$rootScope", "$http", function AppRecuperacionCtrl($scope, $rootScope, $http) {
$scope.recuperacion = function(){
	$scope.success = ""
	  $scope.error = ""
	if($scope.email==undefined){
		$scope.error = "inserte un correo electronico"
			return false
	}
	console.log("login");
	
      $http({
		url: '/pronapinnaback/recuperacion',
		method: "POST",
		data: {email:$scope.email},
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
      }).then(function onSuccess(response) {
    	  $scope.code = response.data.clave
    	  if($scope.code == "200"){
        	  $scope.success = response.data.msg
    	  }else{
        	  $scope.error = response.data.msg
    	  }
      }).catch(function onError(response) {
    	  $scope.error = response.data.msg
    	  $scope.code = response.data.clave
      }).finally(function() {
	 	
		
      });        

};



}]);
  