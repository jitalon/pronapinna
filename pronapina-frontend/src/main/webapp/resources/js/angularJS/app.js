"use strict";

angular
  .module("pronappinaApp", [
    "ngSanitize",
    "ngAnimate",
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad"
  ]);