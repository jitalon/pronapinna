var app = angular.module("pronappinaApp");

app.service("DependenciasService", ["$q", "$timeout",
  function DependenciasService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("DependenciasCtrl", ["$scope", "$uibModal", "DependenciasService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function DependenciasCtrl($scope, mo, DependenciasService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

    $scope.sidebar = {};
    $scope.sidebar.isActive = true;

    $scope.data = [];
    $scope.object = {};
    $scope.registroEdit = new Object();

var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajeDependencia = function(mensaje, type){
	BootstrapDialog.show({
                type: type,
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/catDependencia.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     defer.resolve(result.data);
                   }).catch(function onError(response) {
  		     $scope.mensajeDependencia('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
		   });
                   return defer.promise;
};

$scope.guardar = function(){
      	$http({
		url: '/pronapinnaback/accion/guardar/catDependencia.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    $scope.mensajeDependencia('El registro se guard\u00F3 correctamente','BootstrapDialog.TYPE_SUCCESS');
		    $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
  		    $scope.mensajeDependencia('Problemas al guardar el registro, notifique al administrador','BootstrapDialog.TYPE_DANGER');
              });
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data) {
	$.each($scope.data, function(key,item){
		if(item.id == data){
		    $scope.registroEdit.id= item.id;
		    $scope.registroEdit.clave= item.clave;
		    $scope.registroEdit.descripcion= item.descripcion;
		}
	});

        $scope.someClickHandler(data);
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "DependenciasEditFrm.html",
          controller: "ModalInstanceCtrlCatDependencia",
          controllerAs: "mo",
          width: "400px",
          scope: $scope,
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  

$scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "asc"]
        ])
        .withOption("responsive", true);    


$scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("clave").withTitle("Clave"),
        DTColumnBuilder.newColumn("descripcion").withTitle("Descripci&oacute;n"),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n") 
	   .renderWith(function(data, type, row, meta) {
               return "<button ng-click='editarRegistro("+data.id+")' class='btn btn-primary'><span class='glyphicon glyphicon-pencil' aria-hidden='true' tooltip-animation='true' tooltip-placement='left' tooltip='Modificar'></span></button>";
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];
}]);

// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlCatDependencia", function($scope,$uibModalInstance) {
    var mo = this;    
    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
    
    
    mo.formValidation = function() {
      
      if ($scope.catDependenciasFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.catDependenciasFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});

app.directive('fileInput', ['$parse', function ($parse) {
		return {
			restrict: 'A',
			link: function($scope, elm, attrs) {
				elm.bind('change', function(){
					$parse(attrs.fileInput)
					.assign($scope,elm[0].files)
					$scope.$apply()
				});
			}
		}
}]);

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});

