var app = angular.module("pronappinaApp",["checklist-model"]);

app.service("RolesService", ["$q", "$timeout",
  function RolesService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("RolesCtrl", ["$scope", "$uibModal", "RolesService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function RolesCtrl($scope, mo, RolesService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {
      

    $scope.sidebar = {};
    $scope.sidebar.isActive = true;

    $scope.data = [];
    $scope.object = {};
    $scope.registroEdit = new Object();
    $scope.lstCatPermiso = [];
    $scope.lstCatPermisoTmp = [];

$scope.recuperaCatalogos = function(){
           $http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    // Handle success
                    console.log(response);
		    $scope.lstCatPermiso = response.data.catPermiso;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    // Handle error
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
};    
    
$scope.recuperaCatalogos();

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/seguridad/catPerfil.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     defer.resolve(result.data);
                   });
                   return defer.promise;
};

$scope.guardar = function(){

      	$http({
		url: '/pronapinnaback/calatolos/guarda/seguridad/perfil.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
		    $scope.mensajeRoles('Se produjo un error durante la ejecuci&oacute;n\n'+ JSON.stringify(data),BootstrapDialog.TYPE_DANGER);
// alert('ERROR' + JSON.stringify(data));
              });
};

var types = [BootstrapDialog.TYPE_SUCCESS,BootstrapDialog.TYPE_DANGER];

$scope.mensajeRoles = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data,accion) {
     $scope.lstCatPermisoTmp = [];
     $scope.registroEdit = new Object();

     if(accion == 'M'){
	$.each($scope.data, function(key,item){
		if(item.id == data){
                    $scope.registroEdit = angular.copy(item);
                    $scope.lstCatPermisoTmp = angular.copy($scope.lstCatPermiso);

		    $.each($scope.lstCatPermisoTmp, function(key1,catPermiso){
 			    $.each(item.catPermiso, function(key2,permiso){
				if(catPermiso.id == permiso.id){
                                    $scope.lstCatPermisoTmp[key1].check = 'true';
				}
	    		    });  
		    });
		}
	});
      }else{
         $scope.lstCatPermisoTmp = angular.copy($scope.lstCatPermiso);
         $scope.registroEdit.id = 0;
      }
  // $scope.recuperaCatalogos();
      $scope.someClickHandler(data);
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          animation: true,
          backdrop: true,
          keyboard: true,
          templateUrl: "PerfilEditFrm.html",
          controller: "ModalInstanceCtrlRoles",
          scope: $scope,
          parent: angular.element(document.body),
          controllerAs: "mo",
          width: "400px",
          size: 15
/*
 * ,resolve: { $scope : function () { return $scope; } }
 */
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          // $log.info("Modal dismissed at: " + new Date());
        });
};  

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "desc"]
        ])
        .withOption("responsive", true);    


   $scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("clave").withTitle("Clave Rol"),
        DTColumnBuilder.newColumn("descripcion").withTitle("Descripci&oacute;n"),
        DTColumnBuilder.newColumn("catPermiso").withTitle("Privilegios")       
	   .renderWith(function(data, type, row, meta) {
               var html = '<div class="form-group">';                   
                   html = html +'   <div class="col-sm-12">';
		   $.each(data, function(key,item){
			html = html +'	<label>';
			html = html +'  	<input type="checkbox" disabled checked > '+item.descripcion ;
			html = html +'	</label>';
                   });
                   html = html +'   </div>';
                   html = html +' </div>';
               return html
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n") 
	   .renderWith(function(data, type, row, meta) {
               return "<button type='button' class='btn btn-default' ng-click='editarRegistro("+data.id+",\"M\")'>Editar</button>"
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];
}]);

// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlRoles", function($scope,$uibModalInstance,$timeout) {

    var mo = this; 

    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
// $scope.reloadData();
      $scope.registroEdit = new Object();
      $scope.lstCatPermisoTmp = angular.copy($scope.lstCatPermiso);
      $uibModalInstance.dismiss("cancel");
      
    };    
    
    mo.formValidation = function() {
      
      if ($scope.catPerfilFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.catPerfilFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});

