var app = angular.module("pronappinaApp");

app.service("RolDependenciaService", ["$q", "$timeout",
  function RolDependenciaService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("RolDependenciaCtrl", ["$scope", "$uibModal", "RolDependenciaService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function RolDependenciaCtrl($scope, mo, RolDependenciaService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

	$scope.mensajeDependencia = function(mensaje, type){
		BootstrapDialog.show({
	                type: type,
			closable: true,
			closeByBackdrop: false,
			closeByKeyboard: false,
			title: 'Mensaje del Sistema',
			message: mensaje,
			buttons: [{
				label: 'Aceptar',
				action: function (dialog) {
					dialog.close();
				}
			}]
		});
	};	
	
function getData() {
        var defer = $q.defer();
        $http({
	url: '/pronapinnaback/accion/consulta/dependenciaRol.do',
	method: "GET",
	data: null,
	headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	   }).then(function onSuccess(result) {
          $scope.data = result.data;
          defer.resolve(result.data);
        }).catch(function onError(response) {
    $scope.mensajeDependencia('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
});
        return defer.promise;
};

$scope.dtInstance = {};

$scope.dtOptions = DTOptionsBuilder
.fromFnPromise(getData)
    .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
             <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
    .withBootstrap()
    .withLanguage({
      paginate: {
        previous: "&laquo;",
        next: "&raquo;",
      },
      search: "_INPUT_",
      searchPlaceholder: "Buscar",
      sEmptyTable:     "No existen datos",
      sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
      sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
      sLengthMenu:     "Mostrar _MENU_ registros",
      sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
      sLoadingRecords: "Cargando...",
      sZeroRecords: "No se encontraron registros coincidentes"
    })
    .withOption("order", [
      [1, "asc"]
    ])
    .withOption("responsive", true);    


$scope.dtColumns = [
    DTColumnBuilder.newColumn(null).withTitle('No')
  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
  })  
  .renderWith(function(data, type, full, meta) {
    return (meta.row+1);
  }),
    DTColumnBuilder.newColumn("dependencia").withTitle("Dependencia"),
    DTColumnBuilder.newColumn("coordinadora").withTitle("Coordinadora"),
    DTColumnBuilder.newColumn("coordinada").withTitle("Coordinada")
  ];

  
}]);