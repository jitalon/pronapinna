var app = angular.module("pronappinaApp",["checklist-model"]);

app.service("EjecucionDetalleService", ["$q", "$timeout",
  function AutorizacionDetalleService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("EjecucionDetalleCtrl", ["$scope", "$rootScope", "$uibModal", "EjecucionDetalleService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function EjecucionDetalleCtrl($scope, $rootScope, mo, EjecucionDetalleService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

// setTimeout("location.reload()", 1000);

/*
 * Estatus Actividad 1 Captura 2 Validacion 3 Resultados 4 Autorizacion 5
 * Concluida
 * 
 * Los estatus de una actividad son los siguientes: 1 Captura: cuando la
 * instancia coordinadora debe capturar la actividad, solo debe estar visible la
 * primera pestaña de actividad 2 Validación: cuando la instancia terminó de
 * captura la primera pestaña "actividad" y seleccionó la opción enviar
 * actividad 3 Resultados: cuando el administrador ya validó la actividad y el
 * sistema está listo para que la instancia coordinadora capture la información
 * que se presenta en las pestañas de resultados y población 4 Autorización:
 * cuando la instancia terminó de capturar los resultados y población y
 * seleccionó la opción "enviar resultados" 5 Concluida: cualdo el administrador
 * autoriza los resultados enviados por la intancia coordinadora
 * 
 * 1,3 Operador () 2,4,5 Administrador
 * 
 */

$scope.autorizacion = new Object();
$scope.autorizacion = $rootScope.autorizacion;
$scope.operador = false;
$scope.admin = false;

if($rootScope.user.perfil.clave == 'ADMINISTRADOR'){
	$scope.admin = true;
}else{
	$scope.operador = true;
}

if($.isEmptyObject($rootScope.catActividad)){
  if($rootScope.user.perfil.clave == 'ADMINISTRADOR'){
    window.location.href = '#!/ejecucion2';
  }else{
    window.location.href = '#!/ejecucion';
  }
}

$scope.fecha = ""; 
$scope.verActividad1 =  $rootScope.catActividad;

$scope.tblPlanNacionalAnual = $rootScope.tblPlanNacionalAnual;

$scope.tblActividad1 = new Object();
$scope.tblActividad1.id = 0;
$scope.tblActividad1.idEstatus = 0;
$scope.tblActividad1.cobertura = "N";
$scope.tblActividad1.catDerecho = [];
$scope.tblActividad1.catCategoria = new Object();
$scope.tblActividad1.tblArchivo = new Object();
$scope.tblActividad1.tblArchivo.id = 0;

$scope.lstCatPeriodoProgramacion = [];
$scope.lstCatPeriodoRealizacion = [];

$scope.lstEstado = [];
$scope.lstCatClasificacionEdad = [];
$scope.lstCatPoblacion = [];
$scope.lstCatDerecho = [];
$scope.lstCatCategoria = [];
$scope.tableAll = []; 

$scope.files = [];

$scope.recuperaCatalogos = function(id){
		$http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.lstEstado = response.data.catEstado;
                    $scope.lstCatPeriodoProgramacion = response.data.catPeriodo;
                    $scope.lstCatPeriodoRealizacion = response.data.catPeriodo;
                    $scope.lstCatClasificacionEdad = response.data.catClasificacionEdad;
                    $scope.lstCatPoblacion = response.data.catPoblacion; 
                    $scope.lstCatDerecho = response.data.catDerecho; 
                    $scope.lstCatCategoria = response.data.catCategoria; 
                    $scope.fecha = response.data.fecha;

		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;

                }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
                }).finally(function() {
                   console.log('This finally block');
                   console.log('-----------Actividad ------------ : ' + $rootScope.catActividad.id + '---------------- idPlanNacionalAnual : ' + $scope.tblPlanNacionalAnual.id);
                   $http({
			url: '/pronapinnaback/accion/consulta/informacion/oprActividadFiltro.do',
			method: "POST",
			data: {'idActividad':$rootScope.catActividad.id,'idPlanNacionalAnual':$scope.tblPlanNacionalAnual.id},
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
		   }).then(function onSuccess(response) {
		       $scope.lstTblActividad = response.data;
                       if(!$.isEmptyObject($scope.lstTblActividad[0])){
                           $scope.tblActividad1 = $scope.lstTblActividad[0];
                           if($.isEmptyObject($scope.tblActividad1.tblArchivo)){
                              $scope.tblActividad1.tblArchivo.id = 0;
                           }
                           console.log("Actividad :::::::::::::: " + $scope.tblActividad1);
		    	   var status = response.status;
		    	   var statusText = response.statusText;
		    	   var headers = response.headers;
		    	   var config = response.config;
	 	       }else{
			   console.log('-----------Error----------- '+$scope.tblActividad1);                                
                       }
		   }).catch(function onError(response) {
		       var data = response.data;
		       var status = response.status;
		       var statusText = response.statusText;
		       var headers = response.headers;
		       var config = response.config;
                   }).finally(function() {
                       $.each($scope.lstCatPoblacion, function(key,item){
				$scope.tblRow = new Object();
				$scope.tblRow.id= item.id;
				$scope.tblRow.clave= item.clave;
				$scope.tblRow.descripcion= item.descripcion;
				$scope.tblRow.lstEdad =  $scope.lstCatClasificacionEdad;

				$scope.tableAll.push($scope.tblRow);
			});
                        if(id==0){
                           $scope.generaTabla();
                        }else{
                          // $scope.calculaTotalPoblacion();
                        }
		   });

                });

}; 

$scope.fncShow = function(){
	return ($scope.tblActividad1.idEstatus == 1 || $scope.tblActividad1.idEstatus == 3)?false:true;
};   
    
$scope.recuperaCatalogos(0);

$scope.ApruebaActividad = function() {
	$('#modal-aprobacion').modal({ backdrop: 'static', keyboard: false });
};

$scope.ConfirmacionAprobacion = function() {
            if (!$scope.aprobacionFrm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(3,$scope.envioMail.observacionA,$scope.envioMail.emailA)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n para su autorizaci\u00f3n.');
                }
            }
};

$scope.RechazaActividad = function() {
	$('#modal-rechazo').modal({ backdrop: 'static', keyboard: false });
};

$scope.EnviaValidacion = function() {
	$('#modal-validacion').modal({ backdrop: 'static', keyboard: false });
};

$scope.ConfirmacionRechazo = function() {
            if (!$scope.rechazoFrm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(1,$scope.envioMail.observacionR)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n al responsable de la actividad.');
                }
            }
};

$scope.RechazaActividad2 = function() {
	$('#modal-rechazo2').modal({ backdrop: 'static', keyboard: false });
};

$scope.ConfirmacionRechazo2 = function() {
            if (!$scope.rechazo2Frm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(3,$scope.envioMail.observacionR2)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n al responsable de la actividad.');
                }
            }
};

$scope.ConfirmacionValidacion = function() {
            if (!$scope.validacionFrm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(2,$scope.envioMail.observacionV)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n para dar inicio a la validaci\u00f3n de la actividad.');
                }
            }
};


$scope.EnviaAutorizacion = function() {
	$('#modal-autoriza').modal({ backdrop: 'static', keyboard: false });
};

$scope.ConfirmacionAutorizacion = function() {
            if (!$scope.autorizaFrm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(5,$scope.envioMail.observacionA)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n, que la actividad ha concluido.');
                }
            }
};

$scope.EnviaResultado = function() {
	$('#modal-resultado').modal({ backdrop: 'static', keyboard: false });
};

$scope.ConfirmacionResultado = function() {
            if (!$scope.resultadoFrm.$invalid) {
                $("[data-dismiss=modal]").trigger({ type: "click" });
                if ($scope.fnCambioEstatus(4,$scope.envioMail.observacionRe)) {
		    $scope.mensajeEstatus('Se ha enviado la notificaci\u00f3n para dar inicio a la revici\u00f3n de resultados sobre la actividad.');
                }
            }
};

$scope.mensajeEstatus = function(mensaje){
	BootstrapDialog.show({
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
			  dialog.close();
			  if($rootScope.user.perfil.clave == 'ADMINISTRADOR'){
			    window.location.href = '#!/ejecucion2';
			  }else{
			    window.location.href = '#!/ejecucion';
			  }
			}
		}]
	});
};

$scope.mensaje = function(mensaje){
	BootstrapDialog.show({
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.fnCambioEstatus = function (estatus,mensaje) {
            var bol = true;
	$http({
		url: '/pronapinnaback/accion/actualiza/estatus/oprActividad/' + $scope.tblPlanNacionalAnual.id+'/'+$rootScope.catActividad.id+'/'+estatus,
		method: "POST",
		data: {'mensaje':mensaje},
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	}).then(function onSuccess(response) {

	}).catch(function onError(response) {
            bol = false;
	});

	return bol;
};

$scope.GenerarActividad = function() {

    if($scope.tblActividad1.cobertura == 'N'){
       $scope.tblActividad1.catEstado = [];
       $scope.tblActividad1.catMunicipio = [];
    }

$scope.tblActividad1.tblPlanNacionalAnual=new Object();
$scope.tblActividad1.tblPlanNacionalAnual.id = $rootScope.tblPlanNacionalAnual.id;
$scope.tblActividad1.catActividad = new Object();
$scope.tblActividad1.catActividad.id = $rootScope.catActividad.id;

$scope.recuperaPoblacion();
// console.log("ID ::::::::::::::::::::::::: " + $scope.tblActividad1.id);

     if($scope.tblActividad1.catDerecho.length<2){
        $scope.mensaje('Por favor, selecciona al menos 2 derechos.');
     }else{
       if($scope.tblActividad1.id == 0){
       	  $http({
		url: '/pronapinnaback/accion/guardar/oprActividadCreate.do',
		method: "POST",
		data: $scope.tblActividad1,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.tblActividad1 = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
                    $scope.recuperaCatalogos(1);
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
       }else{
      	  $http({
		url: '/pronapinnaback/accion/guardar/oprActividad.do',
		method: "POST",
		data: $scope.tblActividad1,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.tblActividad1 = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
                    $scope.recuperaCatalogos(1);
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
      }
    }
};

$scope.lstMunicipiosEstado=[];
$scope.selectedUsers = [];

$scope.selEstado = function(valor,id){
        $scope.lstMunicipiosEstado = angular.copy([]);
	$scope.estadoSeleccionado = valor;
        $scope.verBtnMunicipio = false;
        $scope.verMunicipio = true;
        $scope.viewMunicipio3(id);
};

$scope.verBtnMunicipio = true;
$scope.verMunicipio = true;
$scope.mostrarEstado = true;

$scope.viewMunicipio = function(){
	
	$http({
  		url: '/pronapinnaback/accion/consulta/informacion/catMunicipio/'+$scope.estadoSeleccionado.id+'.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
                    console.log(response);
		    $scope.lstMunicipiosEstado = response.data;
                    $scope.verMunicipio = false;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
};

$scope.viewMunicipio3 = function(estado){
	if($("#checkEstado"+estado).prop("checked")){
		$http({
	  		url: '/pronapinnaback/accion/consulta/informacion/catMunicipio/'+estado+'.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	                }).then(function onSuccess(response) {
			    $scope.lstMunicipiosEstado = response.data;
	                    $scope.verMunicipio = false;
			    var status = response.status;
			    var statusText = response.statusText;
			    var headers = response.headers;
			    var config = response.config;
	               }).catch(function onError(response) {
			    var data = response.data;
			    var status = response.status;
			    var statusText = response.statusText;
			    var headers = response.headers;
			    var config = response.config;
	              });
	}
	
};
$scope.viewMunicipio2 = function(estado){
	if(!$("#checkEstado"+estado).prop("checked")){
		$http({
	  		url: '/pronapinnaback/accion/consulta/informacion/catMunicipio/'+estado+'.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	                }).then(function onSuccess(response) {
	                	$("#checkEstado"+estado).prop("checked",true)
			    $scope.lstMunicipiosEstado = response.data;
	                    $scope.verMunicipio = false;
			    var status = response.status;
			    var statusText = response.statusText;
			    var headers = response.headers;
			    var config = response.config;
	               }).catch(function onError(response) {
			    var data = response.data;
			    var status = response.status;
			    var statusText = response.statusText;
			    var headers = response.headers;
			    var config = response.config;
	              });
	}else{
    	$("#checkEstado"+estado).prop("checked",false)
        $scope.verMunicipio = true;
	}
	
};


$scope.recuperaPoblacion = function(){
console.log("Id ++++++++++++++ " + $scope.tblActividad1.id);
             $scope.tblActividad1.oprPoblacionObjetivo = [];

             $.each($scope.tableAll, function(key,item){
                    var idPoblacion = item.id;
                    $.each(item.lstEdad, function(key,item2){
                           var poblacionObjetivo = new Object();
                           var idClasificacionEdad = item2.id;
                           poblacionObjetivo.id = $('#h_'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_H').val();
                           poblacionObjetivo.oprActividad = new Object();
                           poblacionObjetivo.oprActividad.id = $scope.tblActividad1.id; 
                           poblacionObjetivo.catPoblacion = new Object();
                           poblacionObjetivo.catPoblacion.id = idPoblacion;
                           poblacionObjetivo.catClasificacionEdad = new Object(); 
                           poblacionObjetivo.catClasificacionEdad.id = idClasificacionEdad;
                           poblacionObjetivo.totalM = $('#'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_M').val();
                           poblacionObjetivo.totalH = $('#'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_H').val();
                    $scope.tblActividad1.oprPoblacionObjetivo.push(poblacionObjetivo);                                                   
                    });   

             });
            // console.log(JSON.stringify($scope.tblActividad1.oprPoblacionObjetivo));
};

$scope.generaTabla = function(){

  var html = '<table class="table table-striped table-bordered" id="tblPoblacion">'+
             '<thead class="thead-light">'+
             '<tr>' +
             '    <th> TIPO DE POBLACI\u00D3N </th>';
                  $.each($scope.lstCatClasificacionEdad, function(key,item){
                         html = html + '<th colspan=3>('+item.clave+')</th>';
                  });
html = html + '</tr>';

html = html + '<tr>'+
             '    <th></th>';
                  $.each($scope.lstCatClasificacionEdad, function(key,item){
                         html = html + '<th>HOMBRE</th>'+
                                       '<th>MUJER</th>'+
                                       '<th>TOTAL</th>';
                  });
html = html + '</tr>';

html = html + '</thead>'+
              '<tbody>';
$scope.nFilas = 0;
             $.each($scope.tableAll, function(key,item){
                   console.log(item);
                    html = html + '<tr>';
                    html = html + '   <td>'+item.descripcion+'</td>';
                    var idPoblacion = item.id;
                    $.each(item.lstEdad, function(key,item2){
                           var idClasificacionEdad = item2.id;
                           html = html + '<td><input type="hidden" id="h_'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_H" ' +
                                         ' value="0" ng-disabled="'+$scope.autorizacion.ejecucion.inputActividad+'"/> ';
                           if($scope.autorizacion.ejecucion.inputActividad){
                           html = html + ' <label for="valor" id="'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_H">0</label>';                                         
                           }else{
                           html = html + ' <input type="number" class="form-control" min="0" id="'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_H" value="0" '+
                                         ' ng-disabled="'+$scope.autorizacion.ejecucion.inputActividad+'" ng-blur="calculaTotalPoblacion(\''+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_\',0)"/>';
                           }
                           html = html + '</td>';
                           html = html + '<td><input type="hidden" id="h_'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_M" '+
                                         ' value="0" ng-disabled="'+$scope.autorizacion.ejecucion.inputActividad+'"/> ';
                           if($scope.autorizacion.ejecucion.inputActividad){
                           html = html + ' <label for="valor" id="'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_M">0</label>';                                         
                           }else{
                           html = html + ' <input type="number" class="form-control" min="0" id="'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_M" value="0" '+
                                         ' ng-disabled="'+$scope.autorizacion.ejecucion.inputActividad+'" ng-blur="calculaTotalPoblacion(\''+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_\',0)"/>';
                           }
                           html = html + '</td>';
                           html = html + '<td>';
                           html = html + ' <label for="valor" id="'+$scope.tblActividad1.id+'_'+idPoblacion+'_'+idClasificacionEdad+'_TC">0</label>';                                         
                           html = html + '</td>';
  
                    });   
                    html = html + '</tr>';
                    $scope.nFilas++;
             });
html = html + '<tr><td>Poblaci\u00F3n Total</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>';
html = html +'</tbody></table>';

			var sourceHtml = $('#divTmp').append(html).html();
			var compiledHtml = $compile(sourceHtml)($scope);
                        $('#divTmp').remove();
			$('#divTabla').append(compiledHtml);

                           var oprPoblacionObjetivo = $scope.tblActividad1.oprPoblacionObjetivo;

                           $.each(oprPoblacionObjetivo, function(key,item){
                                  var idInputH = $scope.tblActividad1.id+"_"+item.catPoblacion.id+"_"+item.catClasificacionEdad.id+"_H";
                                  var idInputM = $scope.tblActividad1.id+"_"+item.catPoblacion.id+"_"+item.catClasificacionEdad.id+"_M";
                                  var idInputTC = $scope.tblActividad1.id+"_"+item.catPoblacion.id+"_"+item.catClasificacionEdad.id+"_TC";
	                           if($scope.autorizacion.ejecucion.inputActividad){
	                                  $('#'+idInputH).text(item.totalH);
                       		          $('#'+idInputM).text(item.totalM);
                	                  $('#'+idInputTC).text(parseInt(item.totalH)+parseInt(item.totalM));
        	                   }else{
	                                  $('#'+idInputH).val(item.totalH);
                                	  $('#h_'+idInputH).val(item.id);  
                        	          $('#'+idInputM).val(item.totalM);
                	                  $('#h_'+idInputM).val(item.id);
        	                          $('#'+idInputTC).text(parseInt(item.totalH)+parseInt(item.totalM));
	                           }         
                           });

   $scope.calculaTotalPoblacion();

};


$scope.exportaPoblacion = function(){

var csvContent = "data:text/csv;charset=utf-8,%EF%BB%BF";
var blob = new Blob([document.getElementById('divTabla').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8,%EF%BB%BF" + encodeURI(csvContent)
        });
        saveAs(blob, "Report.xls");

/*
 * $tabla = document.querySelector("#divTabla");
 * 
 * var tableExport = new TableExport($tabla, { exportButtons: false, // No
 * queremos botones filename: "Mi tabla de Excel", //Nombre del archivo de Excel
 * sheetname: "Mi tabla de Excel", //Título de la hoja }); var datos =
 * tableExport.getExportData(); var preferenciasDocumento = "datos.tabla.xlsx";
 * tableExport.export2file(preferenciasDocumento.data,
 * preferenciasDocumento.mimeType, preferenciasDocumento.filename,
 * preferenciasDocumento.fileExtension, preferenciasDocumento.merges,
 * preferenciasDocumento.RTL, preferenciasDocumento.sheetname);
 */

/*
 * window.open('data:application/vnd.ms-excel,' + encodeURIComponent(
 * $('div[id$=divTabla]').html())); e.preventDefault();
 */
};

$scope.calculaTotalPoblacion = function(totalColumna,origen){

  var nColumnas = $("#tblPoblacion tr:last td").length;
  console.log('*************************  :::::::::  ' + nColumnas);

  var sum1 = 0;
  var sum2 = 0;
  var sum3 = 0;
  var sum4 = 0;
  var sum5 = 0;
  var sum6 = 0;
  var sum7 = 0;
  var sum8 = 0;
  var sum9 = 0;

     var nFilasTmp = 0;

     $('#tblPoblacion tbody').find('tr').each(function() {
      if($scope.nFilas == nFilasTmp){
        return false;
      }
       if($scope.autorizacion.ejecucion.inputActividad){              
          sum1 += parseInt($('td:eq(1)',$(this)).find('label[for="valor"]').text());
          sum2 += parseInt($('td:eq(2)',$(this)).find('label[for="valor"]').text());
          sum3 += parseInt($('td:eq(3)',$(this)).find('label[for="valor"]').text());
          sum4 += parseInt($('td:eq(4)',$(this)).find('label[for="valor"]').text());
          sum5 += parseInt($('td:eq(5)',$(this)).find('label[for="valor"]').text());
          sum6 += parseInt($('td:eq(6)',$(this)).find('label[for="valor"]').text());
          sum7 += parseInt($('td:eq(7)',$(this)).find('label[for="valor"]').text());
          sum8 += parseInt($('td:eq(8)',$(this)).find('label[for="valor"]').text());
          sum9 += parseInt($('td:eq(9)',$(this)).find('label[for="valor"]').text());
       }else{
          sum1 += parseInt($('td:eq(1)',$(this)).find(':input[type="number"]').val());
          sum2 += parseInt($('td:eq(2)',$(this)).find(':input[type="number"]').val());
          sum3 += parseInt($('td:eq(3)',$(this)).find('label[for="valor"]').text());
          sum4 += parseInt($('td:eq(4)',$(this)).find(':input[type="number"]').val());
          sum5 += parseInt($('td:eq(5)',$(this)).find(':input[type="number"]').val());
          sum6 += parseInt($('td:eq(6)',$(this)).find('label[for="valor"]').text());
          sum7 += parseInt($('td:eq(7)',$(this)).find(':input[type="number"]').val());
          sum8 += parseInt($('td:eq(8)',$(this)).find(':input[type="number"]').val());
          sum9 += parseInt($('td:eq(9)',$(this)).find('label[for="valor"]').text());
       }
       nFilasTmp++;
     });

     /*
		 * $('tr:not(:first)').each(function(){ sum +=
		 * parseFloat($('td:eq(1)',$(this)).find('input').val()); sum2 +=
		 * parseFloat($('td:eq(2)',$(this)).find('input').val()); });
		 */

     $('#tblPoblacion tr:last > td:eq(1)').html(sum1);
     $('#tblPoblacion tr:last > td:eq(2)').html(sum2);
     $('#tblPoblacion tr:last > td:eq(3)').html(sum3);
     $('#tblPoblacion tr:last > td:eq(4)').html(sum4);
     $('#tblPoblacion tr:last > td:eq(5)').html(sum5);
     $('#tblPoblacion tr:last > td:eq(6)').html(sum6);
     $('#tblPoblacion tr:last > td:eq(7)').html(sum7);
     $('#tblPoblacion tr:last > td:eq(8)').html(sum8);
     $('#tblPoblacion tr:last > td:eq(9)').html(sum9);
     
     if(origen==0){
       $scope.calculaTotalPoblacionCol(totalColumna);
     }
};

$scope.calculaTotalPoblacionCol = function(totalColumna){
     var totalCol = parseInt($('#'+totalColumna+'H').val()) + parseInt($('#'+totalColumna+'M').val());
     $('#'+totalColumna+'TC').text(totalCol);
     $scope.calculaTotalPoblacion(totalColumna,1);
};

$scope.upload = function(){

   var totalSize = 0;

  for (var i = 0; i < $scope.files.length; i++) {
    totalSize += $scope.files[i].size;
  }

  if((totalSize / 1000)<= 10000){
	var fd = new FormData();
	var i = 0;

        console.log("Actividad :::::::::::::: " + JSON.stringify($scope.tblActividad1.tblArchivo));
        if(!$scope.tblActividad1.tblArchivo.id){
          $scope.tblActividad1.tblArchivo.id = 0;
        }
        console.log("Actividad :::::::::::::: " + JSON.stringify($scope.tblActividad1.tblArchivo));

        fd.append('actividad', $scope.tblActividad1.id);
        fd.append('descripcion', $scope.tblActividad1.tblArchivo.descripcion);
        fd.append('id', $scope.tblActividad1.tblArchivo.id);
	angular.forEach($scope.files,function(file){
          if(!$scope.tblActividad1.tblArchivo.archivo1){
          }else{
        	  i = i + 1;
          }
          if(!$scope.tblActividad1.tblArchivo.archivo2){
          }else{
        	  i = i + 1;
          }
		
          console.log("archivo " + i);
          
		  fd.append('file' + i,file);

		i = i + 1;        
	})

        console.log("Actividad :::::::::::::: " + JSON.stringify($scope.tblActividad1.tblArchivo));

      if($scope.files.length > 0){   				
	$http.post('/pronapinnaback/uploadFileActividad',fd,
	{
		transformRequest:angular.identity,
		headers:{'Content-Type':undefined}
	}).then(function(d){
		$scope.mensaje('Archivo cargado correctamente al sistema');
		/*
		 * $http({ url: '/pronapinnaback/accion/guardar/oprActividad.do', method:
		 * "POST", data: $scope.tblActividad1, headers: {'Content-Type':
		 * 'application/json'} }).then(function onSuccess(response) {
		 * $scope.tblActividad1 = response.data; }).catch(function
		 * onError(response) { var data = response.data; });
		 */
	})
      }else{
         $scope.mensaje("Debe seleccionar por lo menos un archivo");
      }
  }else{
     var totalMbUpload = ((totalSize / 1000) / 1000).toFixed(2);
     //totalMbUpload.toFixed(2);
     $scope.mensaje("El limite maximo de carga es de 10 MB, usted esta intentando subir " + totalMbUpload);
  }
};

$scope.filesChanged = function(elm){
    var totalFiles = 3;
	$scope.files = elm.files
	    if(!$scope.tblActividad1.tblArchivo.disponible){
	    	totalFiles = 3;
	    }else{
	    	totalFiles = $scope.tblActividad1.tblArchivo.disponible;
	    }
	
        if(parseInt($scope.files.length) > totalFiles ){ 
           $scope.mensaje("El numero maximo de archivos a cargar son " + totalFiles); 
           $scope.files = [];
           document.querySelector('input[type="file"]').value = null;
        }else{
  	      $scope.$apply();
        }
};


}]);

app.directive('restrictTo', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        var re = RegExp(attrs.restrictTo);
        var exclude = /Backspace|Enter|Tab|Delete|Del|ArrowUp|Up|ArrowDown|Down|ArrowLeft|Left|ArrowRight|Right/;

        element[0].addEventListener('keydown', function(event) {
            var v=element[0].value + event.key;
            if (!exclude.test(event.key) && !re.test(v)) {
                event.preventDefault();
            }
        });
    }
  }
});

app.directive('fileInput2', ['$parse', function ($parse) {
		return {
			restrict: 'A',
			link: function($scope, elm, attrs) {
				elm.bind('change', function(){
					$parse(attrs.fileInput2)
					.assign($scope,elm[0].files)
					$scope.$apply()
				});
			}
		}
}]);