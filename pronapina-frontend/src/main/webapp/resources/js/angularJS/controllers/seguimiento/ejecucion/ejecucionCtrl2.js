var app = angular.module("pronappinaApp");

app.service("EjecucionService", ["$q", "$timeout",
  function EjecucionService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);


app.controller("EjecucionCtrl", ["$scope","$rootScope", "$uibModal", "EjecucionService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "$http", "$timeout" ,
  function EjecucionCtrl($scope, $rootScope, mo, EjecucionService,$q, $compile,DTOptionsBuilder, DTColumnBuilder,  $http, $timeout) {

/*
Estatus Plan Nacional Anual
 1 Activo
 2 Historico
*/

$scope.data = [];
$scope.lstTipoRespuesta = [];
$scope.lstCatActividad = [];
$scope.user = $rootScope.user;
$scope.verDataTable = false;


$scope.autorizacion = new Object();
$scope.autorizacion = $rootScope.autorizacion;

$scope.recuperaCatalogos = function(){

		$http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
                    console.log(response.data);
                    $scope.lstTipoRespuesta = response.data.catTipoRespuesta;
                    $scope.lstCatActividad = response.data.catActividad;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              }).finally(function() {
                  $scope.verDataTable = true;
              });
};    

$scope.recuperaCatalogos();

function load(){
console.log($scope.user);
console.log($scope.user.dependencia);

                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/informacion/tblMensajeBitacora/ADMINISTRADOR.do',
			method: "POST",
			data: $scope.user.dependencia,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     console.log(result.data);
                     defer.resolve(result.data);
                   });
                   return defer.promise;
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.ejecucionDetalle = function(id,actividad) {
 $rootScope.catActividad = new Object();
 $rootScope.catActividad.id = actividad;

        	$http({
			url: '/pronapinnaback/accion/consulta/informacion/oprActividadFiltro.do',
			method: "POST",
			data: {'idActividad':actividad,'idPlanNacionalAnual':id},
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
		}).then(function onSuccess(response) {
                    
		   $scope.lstTblActividad = response.data;
                   var idEstatus = $scope.lstTblActividad[0].idEstatus;
                   console.log('&&&&&&&&&&&& ' + idEstatus);
			$.each($scope.data, function(key,item){
				if(item.idPlanNacionalAnual == id){
				$rootScope.tblPlanNacionalAnual = new Object();
				$rootScope.tblPlanNacionalAnual.id= item.idPlanNacionalAnual;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion = new Object(); 
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.clave = item.claveAccion;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia = new Object(); 
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.clave = item.claveEst;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo = new Object();
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.clave = item.claveObj;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.descripcion = item.objetivo;
				}
			});
        	        window.location.href = '#!/ejecucionDetalle';
		}).catch(function onError(response) {

                  if($scope.user.perfil.clave == 'OPERADOR'){
			$.each($scope.data, function(key,item){
				if(item.idPlanNacionalAnual == id){
				$rootScope.tblPlanNacionalAnual = new Object();
				$rootScope.tblPlanNacionalAnual.id= item.idPlanNacionalAnual;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion = new Object(); 
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.clave = item.claveAccion;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia = new Object(); 
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.clave = item.claveEst;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo = new Object();
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.clave = item.claveObj;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.descripcion = item.objetivo;
				}
			});

			window.location.href = '#!/ejecucionDetalle';
			var data = response.data;
			var status = response.status;
			var statusText = response.statusText;
			var headers = response.headers;
			var config = response.config;
                    }
		}); 

};

$scope.mensajePerfiles = function(mensaje){
	BootstrapDialog.show({
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.guardarLinea = function(idLinea,idTipoRespuesta) {
  $scope.objTmp = new Object();
  $.each($scope.data, function(key,item){
	if(item.id == idLinea){
	    $scope.objTmp = item;
	}
  });

  if(!$.isEmptyObject($scope.objTmp.catTipoRespuesta)){
    $scope.objTmp.catTipoRespuesta.id=idTipoRespuesta;
  }else{
   $scope.objTmp.catTipoRespuesta = new Object();
   $scope.objTmp.catTipoRespuesta.id=idTipoRespuesta;
  }
  

      	$http({
		url: '/pronapinnaback/accion/guardar/tblPlanNacionalAnual.do',
		method: "POST",
		data: $scope.objTmp,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
  		    $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });

}

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(load())
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("responsive", true)
        .withOption("order", [
          [2, "asc"]
        ])
        .withLightColumnFilter({
            '0' : { type : 'text' },
            '1' : { type : 'text' },
            '2' : { type : 'text' },
            '3' : { type : 'text' },
            '4' : { type : 'text' },
            '5' : { type : 'text' },
            '6' : { type : 'text' },
            '7' : { type : 'text' }
        });

$scope.dtColumns = [
//        DTColumnBuilder.newColumn("objetivo").withTitle("Objetivo"),
        DTColumnBuilder.newColumn(null).withTitle("Objetivo")
	   .renderWith(function(data, type, row, meta) {
               return row.claveObj+" "+row.objetivo;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
//        DTColumnBuilder.newColumn("estrategia").withTitle("Estrategia"),
	   DTColumnBuilder.newColumn(null).withTitle("Estrategia")
	   .renderWith(function(data, type, row, meta) {
               return row.claveEst+" "+row.estrategia;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
//        DTColumnBuilder.newColumn("accionPuntual").withTitle("Acci&oacute;n puntual"),
	   DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n puntual")
	   .renderWith(function(data, type, row, meta) {
               return row.claveAccion+" "+row.accionPuntual;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
        DTColumnBuilder.newColumn("actividad").withTitle("Actividad"),
        DTColumnBuilder.newColumn("dependencia").withTitle("Intancia Coordinadora"),
        DTColumnBuilder.newColumn("tipoRespuesta").withTitle("Tipo Respuesta"),
        DTColumnBuilder.newColumn("estatus").withTitle("Estatus"),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n")
	   .renderWith(function(data, type, row, meta) {
               var html = '<div class="form-group">';                   
                   html = html +'   <div class="col-sm-12">';    
			html = html +'<div><button type="button" class="btn btn-default" ng-click="ejecucionDetalle('+row.idPlanNacionalAnual+','+row.idActividad+')">Ver</button>';
                   html = html +'   </div>';
                   html = html +' </div>';
               return html
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);
	   }),
        DTColumnBuilder.newColumn("dependenciaCoodinador").withTitle("Instancias Coordinadoras")
      ];

}]);

      
