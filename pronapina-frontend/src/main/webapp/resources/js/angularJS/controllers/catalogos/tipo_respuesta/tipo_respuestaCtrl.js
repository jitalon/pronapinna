var app = angular.module("pronappinaApp");

app.service("TipoRespuestaService", ["$q", "$timeout",
  function Tipo_respuestaService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("Tipo_respuestaCtrl",  ["$scope", "$uibModal", "TipoRespuestaService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function Tipo_respuestaCtrl($scope, mo, TipoRespuestaService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

    $scope.sidebar = {};
    $scope.sidebar.isActive = true;
    $scope.accion = '';

    $scope.data = [];
    $scope.object = {};
    $scope.registroEdit = new Object();
    $scope.lstCatPerfil = [];
    $scope.lstCatDependencia = [];

var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajeRespuestas = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.mensaje = function(mensaje){
	BootstrapDialog.show({
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/informacion/catTipoRespuesta.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     defer.resolve(result.data);
                   }).catch(function onError(response) {
  		     $scope.mensajeRespuestas('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
		   });
                   return defer.promise;
};

$scope.guardar = function(accionEGB){
    
    if (accionEGB == undefined) {
      accionEGB = 1;
    }
    var existe = false;

    if($scope.accion == 'N'){
       $.each($scope.data, function(key,item){
            console.log(item.clave +" {{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}} " + $scope.registroEdit.clave);
	  if(item.clave == $scope.registroEdit.clave){
             existe = true;   
             return false;
	  }
       });
     }
     console.log(existe);
     if(existe){
        $scope.mensaje("El registro ya existe no se puede crear");
     }else{
      	$http({
		url: '/pronapinnaback/accion/guardar/catTipoRespuesta.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    if (accionEGB == 1) {
          $scope.mensajeRespuestas('El registro se guard\u00F3 correctamente', BootstrapDialog.TYPE_SUCCESS);
        } else if (accionEGB == 2) {
          $scope.mensajeRespuestas('El registro se elimin\u00F3 correctamente', BootstrapDialog.TYPE_SUCCESS);
        }
        $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
  		    $scope.mensajeRespuestas('Problemas al guardar el registro, notifique al administrador',BootstrapDialog.TYPE_DANGER);
              });
     }
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data,accion) {
   $scope.accion = accion;
   if(accion == 'D'){
     $.each($scope.data, function(key,item){
	if(item.id == data){
	    $scope.registroEdit.id= item.id;
	    $scope.registroEdit.clave= item.clave;
	    $scope.registroEdit.descripcion= item.descripcion;
            $scope.registroEdit.idEstatus= 2;
            $scope.geb = 2;
            $scope.guardar($scope.geb);
	}
     });
   }else{
     if(accion == 'M'){
	$.each($scope.data, function(key,item){
		if(item.id == data){
		    $scope.registroEdit.id= item.id;
		    $scope.registroEdit.clave= item.clave;
		    $scope.registroEdit.descripcion= item.descripcion;
		    $scope.registroEdit.idEstatus= 1;
		}
	});
      }else{
         $scope.registroEdit = new Object();
         $scope.registroEdit.id = 0;
         $scope.registroEdit.idEstatus= 1;
      }

      $scope.someClickHandler(data);
   }
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "TipoRespuestaEditFrm.html",
          controller: "ModalInstanceCtrlTipoRespuesta",
          controllerAs: "mo",
          width: "400px",
          scope: $scope,
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "desc"]
        ])
        .withOption("responsive", true);    


   $scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("clave").withTitle("Clave"),
        DTColumnBuilder.newColumn("descripcion").withTitle("Descripci&oacute;n"),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n") 
	   .renderWith(function(data, type, row, meta) {
               var html = "";
               html = html +"<button ng-click='editarRegistro("+data.id+",\"M\")' class='btn btn-primary'><span class='glyphicon glyphicon-pencil' aria-hidden='true' tooltip-animation='true' tooltip-placement='left' tooltip='Modificar'></span></button>&nbsp;&nbsp;";
               html = html +"<button confirmed-click='editarRegistro("+data.id+",\"D\")' class='btn btn-primary' ng-confirm-click='Esta seguro de querer eliminar este registro?'>"
                           +"<span class='glyphicon glyphicon-trash' aria-hidden='true' tooltip-animation='true' tooltip-placement='right' tooltip='Eliminar'>"
                           +"</span></button>";
               return html;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];
}]);

// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlTipoRespuesta", function($scope,$uibModalInstance) {
    var mo = this;    
    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
    
    
    mo.formValidation = function() {
      
      if ($scope.catTipoRespuestaFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.catTipoRespuestaFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});


