var app = angular.module("pronappinaApp");

app.service("PeriodosService", ["$q", "$timeout",
  function PeriodosService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("PeriodosCtrl", ["$scope", "$uibModal", "PeriodosService", "DTOptionsBuilder", "DTColumnBuilder" ,
  function PeriodosCtrl($scope, mo, PeriodosService,DTOptionsBuilder, DTColumnBuilder) {
      
    $scope.contacts = [];
    $scope.groupedContacts = [];
    $scope.currentContact;

    $scope.sidebar = {};
    $scope.sidebar.isActive = true;
    
    

    var dt = this;
    
    dt.message = '';
    dt.someClickHandler = someClickHandler;

      dt.options = DTOptionsBuilder
        .fromSource("js/angularJS/json/catalogos/periodos.json")
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
        <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar…",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "desc"]
        ])
        .withOption('rowCallback', rowCallback)
        .withOption("responsive", true);

      dt.columns = [
        DTColumnBuilder.newColumn("id").withTitle("Clave"),
        DTColumnBuilder.newColumn("descripcion").withTitle("Descripci&oacute;n"),
        DTColumnBuilder.newColumn('id', '').withTitle("Acci&oacute;n")
          .renderWith(function (id) {
                console.log(id);
               return "<button type='button' class='btn btn-default'>Editar</button>"
        })
      ];
      
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        // Unbind first in order to avoid any duplicate handler (see
		// https://github.com/l-lin/angular-datatables/issues/87)
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function() {
            $scope.$apply(function() {
                dt.someClickHandler(aData);
            });
        });
        return nRow;
    }
  
function someClickHandler(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "periodosEditFrm.html",
          controller: "ModalInstanceCtrlPeriodo",
          controllerAs: "mo",
          width: "600px",
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          // $log.info("Modal dismissed at: " + new Date());
        });
    }
    
    
  }
]);


// / Controlador del Formulario de tipo Modal
 app.controller("ModalInstanceCtrlPeriodo", function($scope,$uibModalInstance) {
    var mo = this;
    var registro  = {"descripcion" : "Alguna"};
    
    
    mo.ok = function() {
      $uibModalInstance.close();
    };

    mo.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
    
    var types = [BootstrapDialog.TYPE_SUCCESS,BootstrapDialog.TYPE_DANGER];

    $scope.mensajeDialog = function(mensaje, type){
    	BootstrapDialog.show({
                    type: types[type],
    		closable: true,
    		closeByBackdrop: false,
    		closeByKeyboard: false,
    		title: 'Mensaje del Sistema',
    		message: mensaje,
    		buttons: [{
    			label: 'Aceptar',
    			action: function (dialog) {
    				dialog.close();
    			}
    		}]
    	});
    };
    
    mo.formValidation = function() {
      
      if ($scope.catPeriodosFrm.$invalid) {
        // Submit as normal
// alert('No Enviar');
        $scope.mensajeDialog('No enviar',BootstrapDialog.TYPE_DANGER);
      } else {
        $scope.catPeriodosFrm.submitted = true;
// alert('Enviar');
        $scope.mensajeDialog('Enviar',BootstrapDialog.TYPE_DANGER);
      }
    }
    
  });

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});





