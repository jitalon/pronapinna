var app = angular.module("pronappinaApp");


app.controller("RevisionCtrl", ["$scope", "$rootScope", "$uibModal", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function RevisionCtrl($scope, $rootScope, mo, $q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {


$scope.btnConsulta = true;
$scope.lstCatPeriodoProgramacion = [];
$scope.lstCatActividad = [];
$scope.lstCatTipoRespuesta = [];
$scope.data = [];

$scope.user = $rootScope.user;
console.log("root :::::::::::::::::::: " + $rootScope.user);
console.log(" USER controller :::::::::::::::::::: " + JSON.stringify($scope.user));

$scope.tInformacion = 0;
$scope.filtros = new Object();
$scope.filtros.idEstatusPNA = 1 ;

$scope.lstCatEstatusActividad = [{'id':1,'descripcion':'Captura'},{'id':2,'descripcion':'Validaci\u00f3n'},{'id':3,'descripcion':'Resultados'},{'id':4,'descripcion':'Autorizaci\u00f3n'},{'id':5,'descripcion':'Concluida'}];

$scope.recuperaCatalogos = function(){
		$http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
                    $scope.lstCatDependencia = response.data.catDependencia
		    $scope.lstCatEstado = response.data.catEstado;
                    $scope.lstCatPeriodoProgramacion = response.data.catPeriodo;
                    $scope.lstCatPeriodoRealizacion = response.data.catPeriodo;
                    $scope.lstCatClasificacionEdad = response.data.catClasificacionEdad;
                    $scope.lstCatPoblacion = response.data.catPoblacion;
		    $scope.lstCatActividad = response.data.catActividad;
		    $scope.lstCatTipoRespuesta = response.data.catTipoRespuesta;

		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;

                }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
                }).finally(function() {
                   console.log('This finally block');
       		       if($scope.user.perfil.clave == 'OPERADOR'){
    		    	  $scope.filtros.dependencia = $scope.user.dependencia.id;
    		       }                   
                });

};    
    
$scope.recuperaCatalogos();
/*Data Tables */

$scope.dtInstance = {};
$scope.searchOptions={};

$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.infoTabla = function(data){
  $scope.dtOptions = DTOptionsBuilder.newOptions()
//    .fromFnPromise(load())
        .withOption('data',data)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("responsive", true)
        .withOption("order", [
          [2, "asc"]
        ]);

        $scope.dtColumns = [
        DTColumnBuilder.newColumn("objetivo").withTitle("Objetivo").withOption('width', '15%'),
        DTColumnBuilder.newColumn("estrategia").withTitle("Estrategia").withOption('width', '10%'),
        DTColumnBuilder.newColumn("accionPuntual").withTitle("Acci&oacute;n puntual").withOption('width', '10%'),
        DTColumnBuilder.newColumn("actividad").withTitle("Actividad").withOption('width', '10%'),
        DTColumnBuilder.newColumn("dependencia").withTitle("Instancia Coordinadora").withOption('width', '10%'),
        DTColumnBuilder.newColumn("tipoRespuesta").withTitle("Tipo Respuesta").withOption('width', '10%'),
        DTColumnBuilder.newColumn("estatus").withTitle("Estatus").withOption('width', '5%'),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n").withOption('width', '10%')
	   .renderWith(function(data, type, row, meta) {
               var html = '<div class="form-group">';                   
                   html = html +'   <div class="col-sm-12">';    
			html = html +'<div><button type="button" class="btn btn-default" ng-click="ejecucionDetalle('+row.idPlanNacionalAnual+','+row.idActividad+')">Ver</button>';
                   html = html +'   </div>';
                   html = html +' </div>';



               return html
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
        DTColumnBuilder.newColumn("dependenciaCoodinador").withTitle("Instancias Coordinadas").withOption('width', '10%')
        ];
};

$scope.infoTabla([]);

$scope.buscar = function(){
     console.log($scope.filtros);
     $http({
		url: '/pronapinnaback/accion/consulta/bitacoraFiltro.do',
		method: "POST",
		data: $scope.filtros,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	  }).then(function onSuccess(result) {
                $scope.data = result.data;
                $scope.infoTabla(result.data); 
          });
};

$scope.ejecucionDetalle = function(id,actividad) {
 $rootScope.catActividad = new Object();
 $rootScope.catActividad.id = actividad;

        	$http({
			url: '/pronapinnaback/accion/consulta/informacion/oprActividadFiltro.do',
			method: "POST",
			data: {'idActividad':actividad,'idPlanNacionalAnual':id},
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
		}).then(function onSuccess(response) {
                    
		   $scope.lstTblActividad = response.data;
                   var idEstatus = $scope.lstTblActividad[0].idEstatus;
                   
			$.each($scope.data, function(key,item){
				if(item.idPlanNacionalAnual == id){
				$rootScope.tblPlanNacionalAnual = new Object();
				$rootScope.tblPlanNacionalAnual.id= item.idPlanNacionalAnual;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion = new Object(); 
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.clave = item.claveAccion;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia = new Object(); 
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.clave = item.claveEst;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.descripcion = item.estrategia;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo = new Object();
                                $rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.clave = item.claveObj;
				$rootScope.tblPlanNacionalAnual.tblLineaAccion.tblEstrategia.tblObjetivo.descripcion = item.objetivo;
				}
			});

			window.location.href = '#!/revisionDetalle';

		}).catch(function onError(response) {

		}); 

};

}]);





