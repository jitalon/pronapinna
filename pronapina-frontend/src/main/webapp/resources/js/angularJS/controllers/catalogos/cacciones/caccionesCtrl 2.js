var app = angular.module("pronappinaApp");

app.service("CaccionesService", ["$q", "$timeout",
  function CaccionesService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("CaccionesCtrl", ["$scope", "$uibModal", "CaccionesService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function CaccionesCtrl($scope, mo, CaccionesService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {
      
    $scope.data = [];
    $scope.object = {};
    $scope.registroEdit = new Object();
    $scope.lstTblLineaAccion = [];
    $scope.lstTblLineaAccion = [];

var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajeCacciones = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};
      
function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/informacion/tblLineaAccion.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     defer.resolve(result.data);
                   }).catch(function onError(response) {
  		     $scope.mensajeCacciones('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
		   });
                   return defer.promise;
};

$scope.guardar = function(){

      	$http({
		url: '/pronapinnaback/calatolos/guarda/seguridad/tblLineaAccion.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    $scope.mensajeCacciones('El registro se guard\u00F3 correctamente',BootstrapDialog.TYPE_SUCCESS);
		    $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
  		    $scope.mensajeCacciones('Problemas al guardar el registro, notifique al administrador',BootstrapDialog.TYPE_DANGER);
              });
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data,accion) {
   if(accion == 'D'){
     $.each($scope.data, function(key,item){
	if(item.id == data){
	    $scope.registroEdit.id= item.id;
	    $scope.registroEdit.clave= item.clave;
	    $scope.registroEdit.descripcion= item.descripcion;
	    $scope.registroEdit.tblEstrategia= item.tblEstrategia;
            $scope.registroEdit.idEstatus= 2;
            $scope.guardar();
	}
     });
   }else{
     if(accion == 'M'){
	$.each($scope.data, function(key,item){
		if(item.id == data){
		    $scope.registroEdit.id= item.id;
		    $scope.registroEdit.clave= item.clave;
		    $scope.registroEdit.descripcion= item.descripcion;
		    $scope.registroEdit.tblEstrategia= item.tblEstrategia;
                    $scope.registroEdit.idEstatus= 1;
		}
	});
      }else{
         $scope.registroEdit = new Object();
         $scope.registroEdit.id = 0;
         $scope.registroEdit.idEstatus= 2;
      }

      $scope.someClickHandler(data);
   }
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          animation: true,
          backdrop: true,
          keyboard: true,
          templateUrl: "caccionessEditFrm.html",
          controller: "ModalInstanceCtrlccAcciones",
          scope: $scope,
          parent: angular.element(document.body),
          controllerAs: "mo",
          width: "400px",
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "asc"]
        ])
        .withOption("responsive", true);    


   $scope.dtColumns = [
        /*DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '5%')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),*/
        DTColumnBuilder.newColumn("tblEstrategia.clave").withTitle("Clave Estrategia").withOption('width', '5%'),
        DTColumnBuilder.newColumn("tblEstrategia.descripcion").withTitle("Estrategia").withOption('width', '35%'),
        DTColumnBuilder.newColumn("clave").withTitle("Clave").withOption('width', '10%'),
        DTColumnBuilder.newColumn("descripcion").withTitle("Descripci&oacute;n").withOption('width', '35%'),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n").withOption('width', '10%') 
	   .renderWith(function(data, type, row, meta) {
               var html = "";
               html = html +"<button ng-click='editarRegistro("+data.id+",\"M\")' class='btn btn-primary'><span class='glyphicon glyphicon-pencil' aria-hidden='true' tooltip-animation='true' tooltip-placement='left' tooltip='Modificar'></span></button>&nbsp;&nbsp;";
               /*html = html +"<button confirmed-click='editarRegistro("+data.id+",\"D\")' class='btn btn-primary' ng-confirm-click='Are you sure?'>"+
                            "<span class='glyphicon glyphicon-trash' aria-hidden='true' tooltip-animation='true' tooltip-placement='right' tooltip='Eliminar'>"
                            "</span></button>";*/
               return html;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];
}]);

// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlccAcciones", function($scope,$uibModalInstance,$timeout) {

    var mo = this; 

    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $scope.registroEdit = new Object();
      $uibModalInstance.dismiss("cancel");
      
    };    
    
    mo.formValidation = function() {
      
      if ($scope.tblLineaAccionFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.tblLineaAccionFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});