var app = angular.module("pronappinaApp");

app.service("CambioPasswordService", ["$q", "$timeout",
  function CambioPasswordService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("CambioPasswordCtrl", ["$scope", "$state", "$rootScope", "$stateParams", "$uibModal", "CambioPasswordService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function UsuariosCtrl($scope, $state, $rootScope, $stateParams, mo, CambioPasswordService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {
$scope.userTmp = new Object();

$scope.user = $rootScope.user;
$scope.cambiaPassword = function(){
       $scope.user.password = $scope.userTmp.password;
           $http({
  		url: '/pronapinnaback/calatolos/guarda/seguridad/usuario.do',
		method: "POST",
		data: $scope.user,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
                    console.log(response);
                    $scope.mensaje("El password fue actualizado correctamente.");
                    $scope.userTmp = new Object();
               }).catch(function onError(response) {
		    var data = response.data;
              });
};  

$scope.mensaje = function(mensaje){
	BootstrapDialog.show({
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};  
    
}]);