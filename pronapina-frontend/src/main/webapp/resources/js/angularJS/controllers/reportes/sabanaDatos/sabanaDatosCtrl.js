var app = angular.module("pronappinaApp");

app.service("SabanaDatosService", ["$q", "$timeout",
  function SabanaDatosService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("SabanaDatosCtrl", ["$scope", "$rootScope", "$uibModal", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function SabanaDatosCtrl($scope, $rootScope, mo,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

$scope.reporteBtn = true;
$scope.lstCatEstado = [];
$scope.lstCatDependencia = [];
$scope.lstCatPeriodoProgramacion = [];
$scope.lstCatPeriodoRealizacion = [];
$scope.lstCatClasificacionEdad = [];
$scope.lstCatPoblacion = [];
$scope.lstCatActividad = [];
$scope.lstCatTipoRespuesta = [];
$scope.filtros = new Object();
$scope.filtros.dependencia = null;
$scope.filtros.periodoProgramado = null;
$scope.filtros.periodoRealizado = null;
$scope.filtros.edad = null;
$scope.filtros.poblacion = null;
$scope.filtros.respuesta = null;
$scope.filtros.idEstatus = null;
$scope.filtros.idEstatusPNA = null;

$scope.lstCatEstatusActividad = [
	{'id':1,'descripcion':'Captura'},
	{'id':2,'descripcion':'Validacion'},
	{'id':3,'descripcion':'Resultados'},
	{'id':4,'descripcion':'Autorizacion'},
	{'id':5,'descripcion':'Concluida'}];

     $scope.dSelectCE=true;
     $scope.dSelectCD=true;
     $scope.dSelectCPe=true;
     $scope.dSelectCC=true;
     $scope.dSelectCPo=true;
     $scope.dSelectCA=true;
     $scope.dSelectCTr=true;
     $scope.dSelectCEa=true;

$scope.habiliatSelect = function(){
$scope.reporteBtn = false;
  if($scope.reporte=='BT'){
     $scope.dSelectCE=true;
     $scope.dSelectCD=false;
     $scope.dSelectCPe=true;
     $scope.dSelectCC=true;
     $scope.dSelectCPo=true;
     $scope.dSelectCA=true;
     $scope.dSelectCTr=true;
//     $scope.dSelectCEa=false;
     $scope.dSelectCEa=true;
  }else{
     $scope.dSelectCE=false;
     $scope.dSelectCD=false;
     $scope.dSelectCPe=false;
     $scope.dSelectCC=false;
     $scope.dSelectCPo=false;
     $scope.dSelectCA=false;
     $scope.dSelectCTr=false;
//     $scope.dSelectCEa=true;
     $scope.dSelectCEa=false;
  }
};

$scope.recuperaCatalogos = function(){
		$http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
                	console.log(response.data);
                    $scope.lstCatDependencia = response.data.catDependencia
		    $scope.lstCatEstado = response.data.catEstado;
                    $scope.lstCatPeriodoProgramacion = response.data.catPeriodo;
                    $scope.lstCatPeriodoRealizacion = response.data.catPeriodo;
                    $scope.lstCatClasificacionEdad = response.data.catClasificacionEdad;
                    $scope.lstCatPoblacion = response.data.catPoblacion;
		    $scope.lstCatActividad = response.data.catActividad;
		    $scope.lstCatTipoRespuesta = response.data.catTipoRespuesta;

		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;

                }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
                }).finally(function() {
                   console.log('This finally block');
                });

};    
    
$scope.recuperaCatalogos();

$scope.downloadPlanNacional = function (){
  console.log('aqui');

              $http({
                     method:'POST',
                     url:'/pronapinnaback/api/customers/download/lineaAccion.xlsx',
                     responseType: 'blob',
                     data:$scope.filtros
                   }).then(function onSuccess(response) {
                    //,params: {testName: testName}},{responseType: 'blob'}).then(function (response) {
                       console.log("happy");
                       console.log(response);
                       var a = document.createElement("a");
                       document.body.appendChild(a);
                       a.style = "display: none";
                       var blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'});
                       var fileURL = window.URL.createObjectURL(blob);
                           a.href = fileURL;
                           a.download = 'PlanNacional';
                           a.click();
                   }).catch(function(response){
                       console.log(response);
                   });

};

$scope.download = function(){
  if($scope.reporte=='BT'){
      $scope.downloadBitacora();
  }else{
      $scope.downloadPlanNacional(); 
  }   
};

$scope.downloadBitacora = function (){
  console.log('aqui');

              $http({
                     method:'POST',
                     url:'/pronapinnaback/api/customers/download/bitacoraFiltro.xlsx',
                     responseType: 'blob',
                     data:$scope.filtros
                   }).then(function onSuccess(response) {
                    //,params: {testName: testName}},{responseType: 'blob'}).then(function (response) {
                       console.log("happy");
                       console.log(response);
                       var a = document.createElement("a");
                       document.body.appendChild(a);
                       a.style = "display: none";
                       var blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'});
                       var fileURL = window.URL.createObjectURL(blob);
                           a.href = fileURL;
                           a.download = 'Bitacora';
                           a.click();
                   }).catch(function(response){
                       console.log(response);
                   });

};
      
    
    
}]);
