var app = angular.module("pronappinaApp",['angularSoap']);

app.service("InicioService", ["$q", "$timeout",
  function InicioService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
// this.getSoap = function getContacts() {
// return soap.setWSDL('http://localhost:8080/WebServicesExample/hello?wsdl',
// result.data);
// };
// $soap.post(base_url,"HelloWorld");
  }
]);

// myModule.service(['myModule.soap-interceptor', function(soap){
// $http.get('http://www.myveryfakedomain.com/CRMAPIWS74?wsdl',
// { isJSON: true }).then(function(result){
// soap.setWSDL('http:/www.myveryfakedomain.com/CRMAPIWS74', result.data);
// });
// }]);

app.controller("InicioCtrl", ["$scope", "$rootScope", "$uibModal", "InicioService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,"$soap",
  function InicioCtrl($scope, $rootScope, mo, InicioService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout,$soap) {

   $scope.clavePerfil = $rootScope.user.perfil.clave;
   $scope.filtros = new Object();
   $scope.filtros.idUsuario = 0;
// console.log("***********************************");
// console.log($rootScope.user);
// console.log($rootScope.user.perfil.clave);
// console.log("-----------------------------------");
   $scope.filtros.idDependencia = $rootScope.user.dependencia.id;
   

function load(){
//	$soap.post("http://localhost:8080/WebServicesExample/hello?wsdl").then(function onSuccess(result) {
//        console.log(result);
//      }).catch(function onError(result) {
//    	  console.log(result);
//	});;
	
	$scope.clavePerfil = $rootScope.user.perfil.clave;
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/informacion/tblMensajeBitacora/'+$scope.clavePerfil+'.do',
			method: "POST",
			data: $scope.filtros,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     console.log(result.data);
                     defer.resolve(result.data);
                   });
                   return defer.promise;
};

$scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(load())
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [5, "desc"]
        ])
        .withOption("responsive", true);   

$scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("dependencia").withTitle("Dependencia"),
        DTColumnBuilder.newColumn("accionPuntual").withTitle("Acci&oacute;n Puntual"),
        DTColumnBuilder.newColumn("actividad").withTitle("Actividad"),
        DTColumnBuilder.newColumn("comentario").withTitle("Comentario"),
        DTColumnBuilder.newColumn("estatus").withTitle("Estatus"),
        DTColumnBuilder.newColumn("fechaTabla").withTitle("Fecha")
      ];
    
}]);
