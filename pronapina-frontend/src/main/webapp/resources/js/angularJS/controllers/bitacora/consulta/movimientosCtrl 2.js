var app = angular.module("pronappinaApp");

app.service("MovimientosService", ["$q", "$timeout",
  function MovimientosService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("MovimientosCtrl", ["$scope", "$uibModal", "MovimientosService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function MovimientosCtrl($scope, mo, MovimientosService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/registro/oprBitacoraMovimiento.do',
  			//url: '/pronapinnaback/accion/consulta/registro/oprBitacora.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
//                     console.log("***************************************");
//                     console.log($scope.data);
//                     console.log("----------------------------------------");
                     defer.resolve(result.data);
                   });
                   return defer.promise;
};

$scope.dtInstance = {};      
 
$scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "desc"]
        ])
        .withOption("responsive", true);    


$scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("objetivo").withTitle("Objetivo").withOption('width', '10%'),
        DTColumnBuilder.newColumn("estrategia").withTitle("Estrategia").withOption('width', '15%'),
        DTColumnBuilder.newColumn("accionPuntual").withTitle("Acci&oacute;n Puntual").withOption('width', '15%'),
        DTColumnBuilder.newColumn("actividad").withTitle("Actividad").withOption('width', '15%'),
        DTColumnBuilder.newColumn("dependenciaDescripcion").withTitle("Responsables").withOption('width', '15%'),
        DTColumnBuilder.newColumn("usuario").withTitle("Usuario").withOption('width', '8%'),
        DTColumnBuilder.newColumn("estatus").withTitle("Estatus").withOption('width', '8%'),
        DTColumnBuilder.newColumn(null).withTitle("Fecha").withOption('width', '14%')
	   .renderWith(function(data, type, row, meta) {
               var strFecha = "";
               var date = new Date(row.fecha);
               strFecha = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
               return strFecha;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];

}]);
