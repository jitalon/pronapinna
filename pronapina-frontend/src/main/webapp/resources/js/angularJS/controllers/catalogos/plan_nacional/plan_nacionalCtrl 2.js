var app = angular.module("pronappinaApp");

app.service("Plan_nacionalService", ["$q", "$timeout",
  function Plan_nacionalService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("Plan_nacionalCtrl", ["$scope", "$uibModal", "Plan_nacionalService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function Plan_nacionalCtrl($scope, mo, Plan_nacionalService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {


/* ----------------- # CARGA DE DOCUMENTOS TMP Y ADD JSON # ---------------- */
/* Desarrollador : LUIS G. ESCAMILLA FRIAS                                   */
/* Funciones : $scope.filesChanged, $scope.upload, directive                 */ 
/* Fecha : 23/10/2019                                                        */
/* ------------------------------------------------------------------------- */

/*
Estatus para Plan Nacional
 1 Vigente
 2 Historico
*/
$scope.tblPeriodos = [];
$scope.documentos = [];
$scope.data = [];
$scope.planNacionalSexenio = 0;
$scope.mostrar = false;
$scope.verPeriodo = false;
$scope.tblPeriodo = {};
$scope.planNacional = new Object();

$scope.object = {};
$scope.registroEdit = new Object();

$scope.sidebar = {};
$scope.sidebar.isActive = true;
var types = [        BootstrapDialog.TYPE_SUCCESS,  
    BootstrapDialog.TYPE_DANGER];

$('#periodoIni').datepicker({
	format: 'yyyy-mm-dd'
});

$('#periodoFin').datepicker({
	format: 'yyyy-mm-dd'
});

$('#fechaInicio').datepicker({
	format: 'yyyy-mm-dd'
});

$('#fechaFin').datepicker({
	format: 'yyyy-mm-dd'
});

$scope.planNacionalActivo = function(){

      	$http({
		url: '/pronapinnaback/accion/consulta/informacion/cntTblPlanNacionalActivo.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.planNacionalSexenio = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });

};

$scope.planNacionalActivo();
/*
BootstrapDialog.TYPE_DEFAULT, 
BootstrapDialog.TYPE_INFO, 
BootstrapDialog.TYPE_PRIMARY, 
BootstrapDialog.TYPE_WARNING,
*/
var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajePlanNacional = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.upload = function(){

	var fd = new FormData()
	var i = 0;
	angular.forEach($scope.files,function(file){
		fd.append('file' + i,file);
		i = i + 1;
	})
				
	$http.post('/pronapinnaback/upload/plan_nacional.do',fd,
	{
		transformRequest:angular.identity,
		headers:{'Content-Type':undefined}
	}).then(function(d){
              console.log(JSON.stringify(d.data));
              if(d.data.clave != 0){
		$scope.mensajePlanNacional('Archivo cargado correctamente al sistema');
                $scope.tmpLoadFile();
              }else{
                console.log(d.data.msg);
                $scope.mensajePlanNacional('Problemas al cargar el archivo al sistema, notificar al administrador');
              }
	})
};
      
$scope.filesChanged = function(elm){
	$scope.files =elm.files
	$scope.$apply();
};

$scope.dataTmpFile = [];

$scope.tmpLoadFile = function(){
      	$http({
		url: '/pronapinnaback/accion/consulta/informacion/tmpLoadfile.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.dataTmpFile = response.data;
               }).catch(function onError(response) {
		    var data = response.data;
              });
};

$scope.tmpLoadFile();

$scope.ejercicio = function(){

$scope.planNacional = new Object();
$scope.planNacional.fechaInicio = $('#fechaInicio').val();
$scope.planNacional.fechaFin = $('#fechaFin').val();

	if($('#fechaInicio').val() !='' && $('#fechaFin').val() !=''){

	var momentA = moment($('#fechaFin').val(),"YYYY-MM-DD");
	var momentB = moment($('#fechaInicio').val(),"YYYY-MM-DD");
            if(momentA > momentB){

		$http({
		       url: '/pronapinnaback/accion/consulta/informacion/cntPlanNacionalAnualCreado.do',
		       method: "POST",
		       data: $scope.planNacional,
		       headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	             }).then(function onSuccess(response) {
	                if(response.data > 0){
			  $scope.mensajePlanNacional('Ya existe creado un ejercicio para el periodo :'+ $scope.planNacional.fechaInicio + ' al ' + $scope.planNacional.fechaFin,1);
			}else{
			  $http({
			           url: '/pronapinnaback/accion/guardar/oprPlanNacional.do',
			           method: "POST",
			           data: $scope.planNacional,
			           headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	                  }).then(function onSuccess(response) {
			           var data = response.data;
			           $scope.mensajePlanNacional('Se gener\u00F3 correctamente el ejercicio para el periodo :'+ $scope.planNacional.fechaInicio + ' al ' + $scope.planNacional.fechaFin,0);
			           $scope.reloadData();
			  }).catch(function onError(response) {
			           var data = response.data;
			  });
			}
		}).catch(function onError(response) {
			var data = response.data;
		});
            }else{
               $scope.mensajePlanNacional('La Fecha Fin debe ser mayor a la Fecha Inicio');
            }
	}else{
          $scope.mensajePlanNacional('Debe capturar la (Fecha Inicio) y la (Fecha Fin) del Periodo a crear');
	}

};


$scope.procesar = function(){

	if($('#fechaInicio').val() !='' && $('#fechaFin').val() !=''){

		var file = $('#filePlanNacional').val();
		    if (file.substring(3,11) == 'fakepath') {
		            var filename = file.substring(12);
	                    $scope.planNacional.documento = filename;
		    }

		$scope.planNacional.id=0;
		$scope.planNacional.fechaInicio = $('#fechaInicio').val().substr(0, 4);
		$scope.planNacional.fechaFin = $('#fechaFin').val().substr(0, 4);
		$scope.planNacional.clave = 'PN-'+$scope.planNacional.fechaInicio+'-'+$scope.planNacional.fechaFin;
		$scope.planNacional.idEstatus = 1;

		      	$http({
				url: '/pronapinnaback/accion/guardar/tblPlanNacional.do',
				method: "POST",
				data: $scope.planNacional,
				headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
		        }).then(function onSuccess(response) {
				var data = response.data;
				$scope.planNacionalActivo();
                                $('#fechaInicio').val('');
                                $('#fechaFin').val('');
				$scope.mensajePlanNacional('Programa Nacional creado correctamente con el folio : ' + $scope.planNacional.clave);
				$scope.reloadData();
	                }).catch(function onError(response) {
	                	$scope.mensajePlanNacional("Problemas al consulte al administrador");
	                });
	}else{
          $scope.mensajePlanNacional('Debe capturar la (Fecha Inicio) y la (Fecha Fin) para crear el Programa.');
	}
};

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/informacion/tblPlanNacional.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     console.log($scope.data);
                     defer.resolve(result.data);
                   });
                   return defer.promise;
};


$scope.cierraPlanNacional = function(){
      	$http({
		url: '/pronapinnaback/accion/modifica/informacion/updateEstatusTblPlanNacional.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
                    $scope.mensajePlanNacional("El programa se cerr\u00f3 correctamente.");
		    window.location.href = '#!/catPlan_nacional';
               }).catch(function onError(response) {
		    var data = response.data;
		    $scope.mensajePlanNacional("Problemas al consulte al administrador");
              });
};    

$scope.guardar = function(){
      	$http({
		url: '/pronapinnaback/calatolos/consulta/objetivos.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    BootstrapDialog.show({
                type: types[0],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: 'SUCCESS ' + JSON.stringify(data),
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
	            $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
		    BootstrapDialog.show({
                type: types[0],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message:'ERROR' + JSON.stringify(data),
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
              });
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data) {
	$.each($scope.data, function(key,item){
		if(item.id == data){
		    $scope.registroEdit.objetivos= item.objetivos;
		    $scope.registroEdit.estrategias= item.estrategias;
		}
	});

        $scope.someClickHandler(data);
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "plan_nacionalEditFrm.html",
          controller: "ModalInstanceCtrlPlanNacional",
          controllerAs: "mo",
          width: "400px",
          scope: $scope,
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  

//    var dt = this;
    
    //dt.message = '';
   // dt.someClickHandler = someClickHandler;    

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [7, "desc"]
        ])
        .withOption("responsive", true);    


   $scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("clave").withTitle("Folio"),
        DTColumnBuilder.newColumn("documento").withTitle("Documento"),
        DTColumnBuilder.newColumn("fechaInicio").withTitle("Periodo Inicio"),
        DTColumnBuilder.newColumn("fechaFin").withTitle("Periodo Fin"),
        DTColumnBuilder.newColumn(null).withTitle("Fecha Generaci\u00F3n")
	   .renderWith(function(data, type, row, meta) {
               var strFecha = "";
               var date = new Date(row.fechaCarga);
               strFecha = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
               return strFecha;
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
        DTColumnBuilder.newColumn(null).withTitle("Usuario Gener\u00F3")
           .renderWith(function(data, type, row, meta) {
               var html = row.tblUsuario.nombre+' '+row.tblUsuario.apellidoPaterno+' '+row.tblUsuario.apellidoMaterno;
               return html
	   })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
        DTColumnBuilder.newColumn(null).withTitle("Estatus")
           .renderWith(function(data, type, row, meta) {
               var html = row.idEstatus==1?'<a href="" ng-click="mostrarPeriodos('+row.id+');">Vigente</a>':'Historico';
               return html
	   })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];

$scope.mostrarPeriodos = function(id){
	$('#modal-periodo').modal();
        
      	$http({
		url: '/pronapinnaback/accion/consulta/tblPeriodo/'+id+'.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.tblPeriodos = response.data;
                    if($scope.tblPeriodos.length > 0){
                      $scope.tblPeriodo = $scope.tblPeriodos[0];  
                      $scope.verPeriodo = true;
                    }
               }).catch(function onError(response) {
		    var data = response.data;
		    BootstrapDialog.show({
                type: types[0],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message:'ERROR' + JSON.stringify(data),
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
              });
};

$scope.formValidation = function() {    
      if($scope.catPeriodosFrm.$invalid) {
      } else {
        $scope.guardarPeriodo();
      }
};

$scope.guardarPeriodo = function(){

$scope.tblPeriodo.periodoIni = $('#periodoIni').val();
$scope.tblPeriodo.periodoFin = $('#periodoFin').val();

      	$http({
		url: '/pronapinnaback/accion/guardar/tblPeriodo.do',
		method: "POST",
		data: $scope.tblPeriodo,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    $scope.verPeriodo = false;
                    $scope.mensajePlanNacional("Registro guardado correctamente");
               }).catch(function onError(response) {
		    var data = response.data;
                    $scope.mensajePlanNacional("Problemas al guardar el registro");
              });
};


}]);

/// Controlador del Formulario de tipo Modal
 app.controller("ModalInstanceCtrlPlanNacional", function($scope,$uibModalInstance) {
    var mo = this;    
    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $scope.guardar();
      $uibModalInstance.dismiss("cancel");
    };
    
    
    mo.formValidation = function() {
      
      if ($scope.catObjetivoFrm.$invalid) {
        // Submit as normal
        BootstrapDialog.show({
            type: types[0],
	closable: true,
	closeByBackdrop: false,
	closeByKeyboard: false,
	title: 'Mensaje del Sistema',
	message:'No Enviar',
	buttons: [{
		label: 'Aceptar',
		action: function (dialog) {
			dialog.close();
		}
	}]
});
      } else {
        $scope.catObjetivoFrm.submitted = true;
        BootstrapDialog.show({
            type: types[0],
	closable: true,
	closeByBackdrop: false,
	closeByKeyboard: false,
	title: 'Mensaje del Sistema',
	message:'Enviar',
	buttons: [{
		label: 'Aceptar',
		action: function (dialog) {
			dialog.close();
		}
	}]
});
      }
    }
    
  });

app.directive('fileInput', ['$parse', function ($parse) {
		return {
			restrict: 'A',
			link: function($scope, elm, attrs) {
				elm.bind('change', function(){
					$parse(attrs.fileInput)
					.assign($scope,elm[0].files)
					$scope.$apply()
				});
			}
		}
}]);