var app = angular.module("pronappinaApp");

app.service("UsuariosService", ["$q", "$timeout",
  function UsuariosService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("UsuariosCtrl", ["$scope", "$state", "$stateParams", "$uibModal", "UsuariosService", "$q", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "DTColumnDefBuilder", "$http", "$timeout" ,
  function UsuariosCtrl($scope, $state, $stateParams, mo, UsuariosService,$q, $compile,DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $http, $timeout) {

    $scope.data = [];
    $scope.object = {};
    $scope.registroEdit = new Object();
    $scope.lstCatPerfil = [];
    $scope.lstCatDependencia = [];

var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajeUsuario = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

/*console.log("*************************** 1 " + $state.reload);
 //flip value of reload param
if($stateParams.reload){
  //$state.go($state.current, $stateParams);
  $state.reload();
}*/

console.log("*************************** 2 " + $stateParams.reload);

$scope.recuperaCatalogos = function(){
           $http({
  		url: '/pronapinnaback/accion/consulta/generales.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.lstCatPerfil = response.data.catPerfil;
		    $scope.lstCatDependencia = response.data.catDependencia;
               }).catch(function onError(response) {
		    // Handle error
		    var data = response.data;
        	    $scope.mensajeUsuario('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
              });
};    
    
$scope.recuperaCatalogos();

function getData() {
                   var defer = $q.defer();
                   $http({
  			url: '/pronapinnaback/accion/consulta/seguridad/usuario.do',
			method: "GET",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
        	   }).then(function onSuccess(result) {
                     $scope.data = result.data;
                     defer.resolve(result.data);
                   }).catch(function onError(response) {
  		     $scope.mensajeUsuario('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
		   });
                   return defer.promise;
};

$scope.guardar = function(){
      	$http({
		url: '/pronapinnaback/calatolos/guarda/seguridad/usuario.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    $scope.mensajeUsuario('El registro se guard\u00F3 correctamente',BootstrapDialog.TYPE_SUCCESS);
		    $scope.reloadData();
               }).catch(function onError(response) {
		    var data = response.data;
  		    $scope.mensajeUsuario('Problemas al guardar el registro, notifique al administrador',BootstrapDialog.TYPE_DANGER);
              });
};

$scope.dtInstance = {};
    
$scope.reloadData = function() {
	$scope.dtInstance._renderer.rerender(); 
}

$scope.editarRegistro = function(data,accion) {
     if(accion == 'M'){
	$.each($scope.data, function(key,item){
		if(item.id == data){
		    $scope.registroEdit.id= item.id;
		    $scope.registroEdit.usuario= item.usuario;
		    $scope.registroEdit.nombre= item.nombre;
		    $scope.registroEdit.apellidoPaterno= item.apellidoPaterno;
		    $scope.registroEdit.apellidoMaterno= item.apellidoMaterno;
		    $scope.registroEdit.telefono= item.telefono;
		    $scope.registroEdit.email= item.email;
		    $scope.registroEdit.idEstatus= item.idEstatus;
		    $scope.registroEdit.catPerfil = item.catPerfil;
		    $scope.registroEdit.catDependencia = item.catDependencia;
		}
	});
      }else{
         $scope.registroEdit = new Object();
         $scope.registroEdit.id = 0;
         $scope.registroEdit.idEstatus = 1;
      }

      $scope.someClickHandler(data);
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "UsuarioEditFrm.html",
          controller: "ModalInstanceCtrlUsuario",
          controllerAs: "mo",
          width: "400px",
          scope: $scope,
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  

  $scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(getData)
        .withDOM(`<"row"<"col-sm-6"i><"col-sm-6"f>>
                 <"table-responsive"tr><"row"<"col-sm-6"l><"col-sm-6"p>>`)
        .withBootstrap()
        .withLanguage({
          paginate: {
            previous: "&laquo;",
            next: "&raquo;",
          },
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
          sEmptyTable:     "No existen datos",
          sInfo:           "Mostrar _START_ de _END_ de _TOTAL_ registros",
          sInfoEmpty:      "Mostrar 0 de 0 de 0 registros",
          sLengthMenu:     "Mostrar _MENU_ registros",
          sInfoFiltered:   "(Filtrado de _MAX_ entradas totales)",
          sLoadingRecords: "Cargando...",
          sZeroRecords: "No se encontraron registros coincidentes"
        })
        .withOption("order", [
          [2, "desc"]
        ])
        .withOption("responsive", true);    


   $scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('No')
	  .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {
	     if (rowData.AnswerStatus == 1) $(cell).addClass('read')
	  })  
	  .renderWith(function(data, type, full, meta) {
	    return (meta.row+1);
	  }),
        DTColumnBuilder.newColumn("usuario").withTitle("Clave Usuario"),
        DTColumnBuilder.newColumn("nombre").withTitle("Nombre"),
        DTColumnBuilder.newColumn("apellidoPaterno").withTitle("Apellido Paterno"),
        DTColumnBuilder.newColumn("apellidoMaterno").withTitle("Apellido Materno"),
        /*DTColumnBuilder.newColumn("email").withTitle("Email"),
        DTColumnBuilder.newColumn("telefono").withTitle("Telefono"),*/
        DTColumnBuilder.newColumn("catPerfil.clave").withTitle("Perfil"),
        DTColumnBuilder.newColumn("catDependencia.descripcion").withTitle("Dependencia"),
        DTColumnBuilder.newColumn(null).withTitle("Estatus") 
	   .renderWith(function(data, type, row, meta) {
               return row.idEstatus==1?'Activo':'Inactivo';
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   }),
	DTColumnBuilder.newColumn(null).withTitle("Acci&oacute;n") 
	   .renderWith(function(data, type, row, meta) {
               return "<button type='button' class='btn btn-default' ng-click='editarRegistro("+data.id+",\"M\")'>Editar</button>"
	    })
	   .withOption('createdCell', function(cell, cellData, rowData, rowIndex, colIndex) {    
	     $compile(cell)($scope);  
	   })
      ];
}]);

// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlUsuario", function($scope,$uibModalInstance) {
    var mo = this;    
    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
    
    
    mo.formValidation = function() {
      
      if ($scope.catUsuarioFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.catUsuarioFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});

angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});


