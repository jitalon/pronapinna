var app = angular.module("pronappinaApp", ['chart.js']);

app.controller("EjecutivosCtrl", ["$scope", "$uibModal", "DTOptionsBuilder", "DTColumnBuilder" , "$http" ,
  function EjecutivosCtrl($scope, mo, DTOptionsBuilder, DTColumnBuilder,$http) {

var types = [        BootstrapDialog.TYPE_SUCCESS,  
                     BootstrapDialog.TYPE_DANGER];

$scope.mensajeGrafica = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

$scope.data = {};

$scope.funcionGraficas = function(){
      	$http({
		url: '/pronapinnaback/accion/consulta/graficas.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    $scope.data = response.data;
                    console.log($scope.data);
               }).catch(function onError(response) {
		    var data = response.data;
                    $scope.mensajeGrafica('Problemas recuperar la informacion, notifique al administrador',BootstrapDialog.TYPE_DANGER);
               }).finally(function() {

$scope.fecha = $scope.data.fecha;
$scope.objData = JSON.parse($scope.data.grafica1); 
$scope.objData2 = JSON.parse($scope.data.grafica2);
$scope.objData3 = JSON.parse($scope.data.reporte2);


Highcharts.chart('container1', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        }
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
               enabled: true,
               format: '<b>{point.name}</b>: {point.percentage:.1f} %',
               style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor)||
                  'black'
               }
            }
         }
    },
//$scope.labels = [$scope.objData.conprogramanum + " Con Programacion" , $scope.objData.sinprogramanum +" Sin Programacion" ];
//$scope.data = [$scope.objData.conprogramacion,$scope.objData.sinprogramacion];

    series: [{
        name: 'Actividades',
        data: [
            ['Con Programacion', $scope.objData.conprogramanum],
            ['Sin Programacion', $scope.objData.sinprogramanum]
        ]
    }]
});


/*$scope.labels = [$scope.objData.conprogramanum + " Con Programacion" , $scope.objData.sinprogramanum +" Sin Programacion" ];
$scope.data = [$scope.objData.conprogramacion,$scope.objData.sinprogramacion];
$scope.colorsPie = ["#1d7af3","#f3545d"]; 
$scope.PieDataSetOverride = [{ yAxisID: 'y-axis-1' }];

    $scope.optionsPie = {
        legend: {
                 display: true,
	         position: 'left'
        },
        responsive: true,
        scales: { 
    		xAxes: [{        
                  id: 'y-axis-1',
                  type: 'linear',
                  display: false,
                  position: 'left'}], 
		yAxes: [{      
                  id: 'y-axis-1',
                  type: 'linear',
                  display: false,
                  position: 'left'}] 
        }
    };



$scope.labels2 = [$scope.objData2.sinrespuestanum +" Sin Respuesta", $scope.objData2.enprocesonum + " En Proceso", $scope.objData2.pendientenum + " Pendiente", $scope.objData2.cumplidanum +" Cumplidas"];
$scope.data2 = [$scope.objData2.sinrespuesta,$scope.objData2.enproceso,$scope.objData2.pendiente,$scope.objData2.cumplida];
$scope.colorsPie2 = ['#FF5252', '#FF8A80', '#80b6ff', '#c980ff']; 
$scope.PieDataSetOverride2 = [{ yAxisID: 'y-axis-1' }];
 
    $scope.optionsPie2 = {
        legend: {
                 display: true,
	         position: 'left'
        },
        chartArea: {
	        backgroundColor: 'rgba(251, 85, 85, 0.4)'
	},
        responsive: true,
        scales: { 
    		xAxes: [{        
                  id: 'y-axis-1',
                  type: 'linear',
                  display: false,
                  position: 'left'}], 
		yAxes: [{      
                  id: 'y-axis-1',
                  type: 'linear',
                  display: false,
                  position: 'left'}] 
        }
    };*/


Highcharts.chart('container2', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        }
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
               enabled: true,
               format: '<b>{point.name}</b>: {point.percentage:.1f} %',
               style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor)||
                  'black'
               }
            }
         }
    },
    series: [{
        name: 'Lineas de Acci\u00f3n',
        data: [
            ['Sin Respuesta', $scope.objData2.sinrespuesta],
            ['En Proceso', $scope.objData2.enproceso],
            ['Pendiente', $scope.objData2.pendiente],
            ['Cumplidas', $scope.objData2.cumplida]
        ]
    }]
});



              });

};
$scope.printDiv = function(divId) {
    var printContents = document.getElementById(divId).innerHTML;
    var popupWin = window.open('', '_blank', 'width=300,height=300');
    popupWin.document.open();
    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
    popupWin.document.close();
  } 
$scope.funcionGraficas();

    
}]);
