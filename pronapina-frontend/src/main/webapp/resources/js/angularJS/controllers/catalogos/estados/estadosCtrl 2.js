var app = angular.module("pronappinaApp");

app.service("EstadosService", ["$q", "$timeout",
  function EstadosService($q, $timeout) {
    this.getContacts = function getContacts() {
      
    };
  }
]);

app.controller("EstadosCtrl", ["$scope", "$uibModal", "EstadosService", "DTOptionsBuilder", "DTColumnBuilder" , "$http", "$timeout" ,
  function EstadosCtrl($scope, mo, EstadosService,DTOptionsBuilder, DTColumnBuilder, $http, $timeout) {
      
/* ------------------- #       Entidad Federativa      # --------------------- */
/* Desarrollador : LUIS G. ESCAMILLA FRIAS                                     */
/* Funciones :                                                                 */ 
/* Fecha Creacion : 01/11/2019                                                 */
/* --------------------------------------------------------------------------- */	  	  

$scope.lstEstado = [];
$scope.estadoSeleccionado = new Object();

$scope.recuperaCatalogos = function(){
           $http({
  		url: '/pronapinnaback/accion/consulta/informacion/catEstado.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    // Handle success
                    console.log(response);
		    $scope.lstEstado = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    // Handle error
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
};    
    
$scope.recuperaCatalogos();
	  	  
$scope.viewMunicipio = function(valor){
        $scope.lstMunicipiosEstado = angular.copy([]);
	$scope.estadoSeleccionado = valor;

	$http({
  		url: '/pronapinnaback/accion/consulta/informacion/catMunicipio/'+valor.id+'.do',
		method: "GET",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    // Handle success
                    console.log(response);
		    $scope.lstMunicipiosEstado = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
               }).catch(function onError(response) {
		    // Handle error
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
};	

$scope.guardar = function(){

      	$http({
		url: '/pronapinnaback/calatolos/guarda/catMunicipio.do',
		method: "POST",
		data: $scope.registroEdit,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
                }).then(function onSuccess(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
	            $scope.viewMunicipio($scope.estadoSeleccionado);
               }).catch(function onError(response) {
		    var data = response.data;
		    var status = response.status;
		    var statusText = response.statusText;
		    var headers = response.headers;
		    var config = response.config;
              });
};  

$scope.editarRegistro = function(item,accion) {
     $scope.registroEdit = new Object();
     if(accion == 'M'){

	$scope.registroEdit.id= item.id;
	$scope.registroEdit.clave= item.clave;
	$scope.registroEdit.descripcion= item.descripcion;
	$scope.registroEdit.catEstado = item.catEstado;
      }else{
         $scope.registroEdit.id = 0;
         $scope.registroEdit.catEstado = $scope.estadoSeleccionado;
      }

      $scope.someClickHandler(item);
};
    
$scope.someClickHandler = function(info) {
      var modalInstance = mo.open({
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "municipioEditFrm.html",
          controller: "ModalInstanceCtrlMunicipio",
          controllerAs: "mo",
          backdrop: true,
          keyboard: true,
          width: "400px",
          scope: $scope,
          size: 15
        });
  
        modalInstance.result.then(function(selectedItem) {
          mo.selected = selectedItem;
        }, function() {
          //$log.info("Modal dismissed at: " + new Date());
        });
};  
 
}]);

/* ------------------- #   FIN  Entidad Federativa     # --------------------- */
/* --------------------------------------------------------------------------- */


/// Controlador del Formulario de tipo Modal
app.controller("ModalInstanceCtrlMunicipio", function($scope,$uibModalInstance) {
    var mo = this;    
    mo.ok = function() {	
	$uibModalInstance.close();
    };

    mo.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
    
    
    mo.formValidation = function() {
      
      if ($scope.catMunicipioFrm.$invalid) {
        console.log('Formulario no valido para carga');
      } else {
        $scope.catMunicipioFrm.submitted = true;
        $scope.guardar();
        $uibModalInstance.dismiss("cancel");
        console.log('Formulario valido');
      }
    }
    
});


angular.module('ui.bootstrap').config(function ($provide) {
    $provide.decorator('$uibModal', function ($delegate) {
        var open = $delegate.open;

        $delegate.open = function (options) {
            options = angular.extend(options || {}, {
                backdrop: 'static',
                keyboard: false
            });

            return open(options);
        };
        return $delegate;
    })
});