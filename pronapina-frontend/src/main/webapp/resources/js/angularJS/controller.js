angular.module("pronappinaApp")
  .controller("AppCtrl", ["$scope", "$rootScope", "$http","$timeout", function AppCtrl($scope, $rootScope, $http, $timeout) {

	  $scope.logout = function(){
		  localStorage.clear();
		  window.location.href="/pronapinna/login";
	  }
$scope.fncInicial = function(){

	
	
      $rootScope.user = new Object();
      $rootScope.user.username = window.localStorage.getItem('USER');
      $rootScope.user.perfil = new Object();
      $rootScope.user.dependencia = new Object();

      $rootScope.autorizacion = {};
      $rootScope.autorizacion.usuario = "";
      $rootScope.autorizacion.administracion = {};
      $rootScope.autorizacion.administracion.visible = false;
      $rootScope.autorizacion.ejecucion = {};
      $rootScope.autorizacion.ejecucion.visible = false;
      $rootScope.autorizacion.reportes = {};
      $rootScope.autorizacion.reportes.visible = false;
      $rootScope.autorizacion.historico = {};
      $rootScope.autorizacion.historico.visible = false;
      $rootScope.autorizacion.seguridad = {};
      $rootScope.autorizacion.seguridad.visible = false;
      $rootScope.autorizacion.bitacora = {};
      $rootScope.autorizacion.bitacora.visible = false;
      console.log(window.localStorage.getItem('USER'));
  	if(window.localStorage.getItem('USER')=== null){
  		window.location.href="/pronapinna/login";
  	}else{
  		$timeout(function() {
  			localStorage.clear();
  			BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
		closable: false,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: "Lo sentimos su sesi&oacute;n ha expirado",
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				window.location.href="/pronapinna/login";
				dialog.close();
				
			}
		}]
	});
  		  },14400000);
  	}
      $http({
		url: '/pronapinnaback/accion/consulta/seguridad/usuarioByUserName.do',
		method: "POST",
		data: $rootScope.user,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
      }).then(function onSuccess(response) {
                console.log("Iniciando");
                
		var data = response.data;
		$rootScope.user.id = data.id;
		$rootScope.user.nombre = data.nombre + ' '+ data.apellidoPaterno + ' ' + data.apellidoMaterno;
                $rootScope.user.perfil = data.catPerfil;
                $rootScope.user.dependencia = data.catDependencia;
                $scope.user.dependencia = data.catDependencia;
                console.log("Termiando");
      }).catch(function onError(response) {

      }).finally(function() {
	 	if($rootScope.user.perfil.clave == 'SUPERADMIN'){
		 	$rootScope.autorizacion.administracion.visible=true;
			$rootScope.autorizacion.ejecucion.visible=true;
			$rootScope.autorizacion.reportes.visible=true;
			$rootScope.autorizacion.historico.visible=true;
			$rootScope.autorizacion.seguridad.visible=true;
			$rootScope.autorizacion.bitacora.visible=true;
		}
		if($rootScope.user.perfil.clave == 'ADMINISTRADOR'){
			$rootScope.autorizacion.administracion.visible=true;
			$rootScope.autorizacion.seguridad.visible=true;
			$rootScope.autorizacion.bitacora.visible=true;
			$rootScope.autorizacion.ejecucion.visible=true;

        	        $rootScope.autorizacion.ejecucion.btnGeneraActividad=false;
	                $rootScope.autorizacion.ejecucion.btnRechazaActividad=false;
	                $rootScope.autorizacion.ejecucion.btnApruebaActividad=false;
	                $rootScope.autorizacion.ejecucion.inputVerificador=false;
	                $rootScope.autorizacion.ejecucion.inputActividad=true;
	                $rootScope.autorizacion.reportes.visible = true;

			$rootScope.autorizacion.ejecucion.accionesPuntuales=false;
			$rootScope.autorizacion.ejecucion.monitoreo = true;
			$rootScope.autorizacion.ejecucion.observacion = true;
			$rootScope.autorizacion.ejecucion.autorizacion = false;
		}
		if($rootScope.user.perfil.clave == 'OPERADOR'){

	  	    $http({
			url: '/pronapinnaback/accion/consulta/tblPeriodoActivo.do',
			method: "POST",
			data: null,
			headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
		    }).then(function onSuccess(response) {
	                if(response.data.length > 0){
			   $rootScope.autorizacion.ejecucion.visible=true;
	                   $rootScope.autorizacion.ejecucion.btnGeneraActividad=true;
	                   $rootScope.autorizacion.ejecucion.btnRechazaActividad=false;
	                   $rootScope.autorizacion.ejecucion.btnApruebaActividad=false;
	                   $rootScope.autorizacion.ejecucion.inputVerificador=true;
	                   $rootScope.autorizacion.ejecucion.inputActividad=false;

			   $rootScope.autorizacion.ejecucion.accionesPuntuales=true;
			   $rootScope.autorizacion.ejecucion.monitoreo = false;
			   $rootScope.autorizacion.ejecucion.autorizacion = false;
			   $rootScope.autorizacion.ejecucion.observacion = true;

	                }else{
	                  $rootScope.autorizacion.ejecucion.visible=true;
	                  $rootScope.autorizacion.ejecucion.monitoreovisible=false;
	                  $rootScope.autorizacion.ejecucion.accionesPuntuales=false;
	                  $rootScope.autorizacion.ejecucion.observacion = true;
	                  
//        	          alert("El periodo de captura ya no se encuentra disponible.");
	                  $scope.mensajeDialog('El periodo de captura ya no se encuentra disponible.',BootstrapDialog.TYPE_DANGER);
	                }
		    }).catch(function onError(response) {

		    });        
		}
});        

};

$scope.fncInicial();

var types = [BootstrapDialog.TYPE_SUCCESS,BootstrapDialog.TYPE_DANGER];

$scope.mensajeDialog = function(mensaje, type){
	BootstrapDialog.show({
                type: types[type],
		closable: true,
		closeByBackdrop: false,
		closeByKeyboard: false,
		title: 'Mensaje del Sistema',
		message: mensaje,
		buttons: [{
			label: 'Aceptar',
			action: function (dialog) {
				dialog.close();
			}
		}]
	});
};

}]);
  