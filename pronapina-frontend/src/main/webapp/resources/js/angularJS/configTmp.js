"use strict";

angular
  .module("pronappinaApp")
  .config(function($provide,$stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {

  $httpProvider.interceptors.push('loadingStatusInterceptor');

  $urlRouterProvider.otherwise("/home");

  $ocLazyLoadProvider.config({
    debug: false,
    modules: [{
      name: "angular-peity",
      serie: true,
      files: [
        "resources/js/vendor/peity/jquery.peity.min.js",
        "resources/js/vendor/angular-peity/angular-peity.min.js"
      ]

    }, {
      name: "chart.js",
      serie: true,
      files: [
        "resources/js/vendor/numeral/numeral.min.js",
        "resources/js/vendor/numeral/locales.min.js",
        "resources/js/vendor/chartjs/Chart.bundle.js",
        "resources/js/vendor/angular-chart/angular-chart.js",
        "resources/js/vendor/angular-chart/angular-chart.min.js"
      ]
    }, {
      name: "ui.select",
      serie: true,
      files: [
        "resources/css/vendor/angular-ui/angular-ui-select.min.css",
        "resources/js/vendor/angular-ui/angular-ui-select.min.js"
      ]
    }, {
      name: "toast",
      serie: true,
      files: [
        "resources/css/vendor/toast/toast.min.css",
        "resources/js/vendor/toast/toast.min.js"
      ]
    }, {
      name: "checklist-model",
      serie: true,
      files: [
        "resources/js/vendor/angular1.8.2/checklist-model.js"
      ]
    }, {
      name: "ngCropper",
      serie: true,
      files: [
        "resources/css/vendor/ngCropper/ngCropper.min.css",
        "resources/js/vendor/ngCropper/ngCropper.min.js"
      ]
    }, {
      name: "ngSlider",
      serie: true,
      files: [
        "resources/css/vendor/ngSlider/ngSlider.min.css",
        "resources/js/vendor/ngSlider/ngSlider.min.js"
      ]
    }, {
      name: "blueimp.fileupload",
      serie: true,
      files: [
        "resources/js/vendor/jquery-ui/jquery-ui.min.js",
        "resources/js/vendor/load-image/load-image.all.min.js",
        "resources/js/vendor/fileupload/jquery.iframe-transport.js",
        "resources/js/vendor/fileupload/jquery.fileupload.js",
        "resources/js/vendor/fileupload/jquery.fileupload-process.js",
        "resources/js/vendor/fileupload/jquery.fileupload-image.js",
        "resources/js/vendor/fileupload/jquery.fileupload-validate.js",
        "resources/js/vendor/fileupload/jquery.fileupload-angular.js"
      ]
    }, {
      name: "datatables",
      serie: true,
      files: [
        "resources/css/vendor/datatables/datatables.min.css",
        "resources/css/vendor/datatables/datatables-responsive.min.css",
        "resources/css/vendor/datatables/datatables-colreorder.min.css",
        "resources/css/vendor/datatables/datatables-scroller.min.css",
        "resources/css/vendor/datatables/dataTables.tableTools.css",
        
        "https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css",
        
        "resources/js/vendor/datatables/jquery.dataTables.min.js",
        "resources/js/vendor/datatables/dataTables.bootstrap.min.js",
        "resources/js/vendor/datatables/dataTables.responsive.min.js",
        "resources/js/vendor/datatables/responsive.bootstrap.min.js",
        "resources/js/vendor/datatables/dataTables.colReorder.min.js",
        "resources/js/vendor/datatables/dataTables.scroller.min.js",

        "resources/js/vendor/angular-datatables/angular-datatables.js",
        "resources/js/vendor/angular-datatables/dataTables.lightColumnFilter.js",
        "resources/js/vendor/angular-datatables/angular-datatables.light-columnfilter.js",

        "resources/js/vendor/angular-datatables/angular-datatables.min.js",
        "resources/js/vendor/angular-datatables/dataTables.lightColumnFilter.min.js",
        "resources/js/vendor/angular-datatables/angular-datatables.light-columnfilter.min.js",

        "resources/js/vendor/angular-datatables/angular-datatables.bootstrap.min.js",
        "resources/js/vendor/angular-datatables/angular-datatables.tabletools.min.js",
        "resources/js/vendor/angular-datatables/angular-datatables.colvis.min.js",
        "resources/js/vendor/angular-datatables/dataTables.tableTools.js",        
        "https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js",
        "https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js",
        "https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js" ,
        "https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js",
        "https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"
      ]
    }, {
      name: "uiGmapgoogle-maps",
      serie: true,
      files: [
        "resources/js/vendor/angular-google-maps/angular-google-maps.min.js",
        "resources/js/vendor/angular-simple-logger/angular-simple-logger.js"
      ]
    }, {
      name: "textAngular",
      serie: true,
      files: [
        "resources/css/vendor/textAngular/textAngular.min.css",
        "resources/js/vendor/textAngular/textAngular-sanitize.min.js",
        "resources/js/vendor/textAngular/textAngular-rangy.min.js",
        "resources/js/vendor/textAngular/textAngular.js",
        "resources/js/vendor/textAngular/textAngularSetup.js",
      ]
    }, {
      name: "wu.masonry",
      serie: true,
      files: [
        "resources/js/vendor/masonry/masonry.pkgd.min.js",
        "resources/js/vendor/imagesloaded/imagesloaded.pkgd.min.js",
        "resources/js/vendor/angular-masonry/angular-masonry.min.js"
      ]
    }, {
      name: "angular-flexslider",
      serie: true,
      files: [
        "resources/css/vendor/flexslider/flexslider.min.css",
        "resources/js/vendor/flexslider/flexslider.min.js",
        "resources/js/vendor/angular-flexslider/angular-flexslider.min.js"
      ]
    }]
  });

  $stateProvider
    .state("root", {
      abstract: true,
      templateUrl: "views/app.tpl.html",
      location: 'replace'
    })
    .state("main", {
      abstract: true,
      templateUrl: "views/app.tpl.html",
      location: 'replace'
    })
    
    .state("main.begin", {
      url: "/home",
      controller: "InicioCtrl as dt",
      templateUrl: "views/home.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/inicio/inicioCtrl.js"
          ]);
        }
      }
    })
    
    
    /////////////////////////////////////////////////////////////////////
    //DOSG Codigo del Bloque de Catalogos.
    .state("main.objetivos", {
      url: "/catObjetivos",
      controller: "ObjetivosCtrl as dt",
      templateUrl: "views/catalogos/objetivos/objetivos.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/objetivos/objetivosCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.dependencias", {
      url: "/catDependencias",
      controller: "DependenciasCtrl as dt",
      templateUrl: "views/catalogos/dependencias/dependencias.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/dependencias/dependenciasCtrl.js"
          ]);
        }
      }
    })
        
    .state("main.plan_nacional", {
      url: "/catPlan_nacional",
      controller: "Plan_nacionalCtrl as dt",
      templateUrl: "views/catalogos/plan_nacional/plan_nacional.tpl.html",
      location: 'replace',
      resolve: {
                loadDatatables: function($ocLazyLoad) {
          	   return $ocLazyLoad.load("datatables");
                },
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                   return $ocLazyLoad.load('resources/js/angularJS/controllers/catalogos/plan_nacional/plan_nacionalCtrl.js');
                }]
               }
    })
    
     .state("main.instancia_coordinadora", {
      url: "/catInstancia_coordinadora",
      controller: "Instancia_coordinadoraCtrl as dt",
      templateUrl: "views/catalogos/instancia_coordinadora/instancia_coordinadora.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/instancia_coordinadora/instancia_coordinadoraCtrl.js"
          ]);
        }
      }
    })
   
     .state("main.acciones_puntuales", {
      url: "/catAcciones_puntuales",
      controller: "Acciones_puntualesCtrl as dt",
      templateUrl: "views/catalogos/acciones_puntuales/acciones_puntuales.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/acciones_puntuales/acciones_puntualesCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.responsables", {
      url: "/catResponsables",
      controller: "ResponsablesCtrl as dt",
      templateUrl: "views/catalogos/responsables/responsables.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/responsables/responsablesCtrl.js"
          ]);
        }
      }
    })
    
    
     .state("main.estrategias", {
      url: "/catEstrategias",
      controller: "EstrategiasCtrl as dt",
      templateUrl: "views/catalogos/estrategias/estrategias.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/estrategias/estrategiasCtrl.js"
          ]);
        }
      }
    })

    .state("main.tipo_respuesta", {
      url: "/catTipo_respuesta",
      controller: "Tipo_respuestaCtrl as dt",
      templateUrl: "views/catalogos/tipo_respuesta/tipo_respuesta.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/tipo_respuesta/tipo_respuestaCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.cacciones", {
      url: "/catCacciones",
      controller: "CaccionesCtrl as dt",
      templateUrl: "views/catalogos/cacciones/cacciones.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/cacciones/caccionesCtrl.js"
          ]);
        }
      }
    })
    
     .state("main.derechos", {
      url: "/catDerechos",
      controller: "DerechosCtrl as dt",
      templateUrl: "views/catalogos/derechos/derechos.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/derechos/derechosCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.categorias", {
      url: "/catCategorias",
      controller: "CategoriasCtrl as dt",
      templateUrl: "views/catalogos/categoria/categoria.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/categoria/categoriaCtrl.js"
          ]);
        }
      }
    })

    .state("main.periodos", {
      url: "/catPeriodos",
      controller: "PeriodosCtrl as dt",
      templateUrl: "views/catalogos/periodos/periodos.tpl.html",
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/periodos/periodosCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.estados", {
      url: "/catEstadosyMunicipios",
      controller: "EstadosCtrl as dt",
      templateUrl: "views/catalogos/estados/estados.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/catalogos/estados/estadosCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.ejecucion2", {
      url: "/ejecucion2",
      controller: "EjecucionCtrl as dt",
      templateUrl: "views/seguimiento/ejecucion/main2.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/ejecucion/ejecucionCtrl2.js"
          ]);
        }
      }
    })
    
    .state("main.ejecucion", {
      url: "/ejecucion",
      controller: "EjecucionCtrl as dt",
      templateUrl: "views/seguimiento/ejecucion/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/ejecucion/ejecucionCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.ejecucionDetalle", {
      url: "/ejecucionDetalle",
      controller: "EjecucionDetalleCtrl as dt",
      location: 'replace',
      templateUrl: "views/seguimiento/ejecucion/detalle.tpl.html",
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/ejecucion/detalleCtrl.js"
          ]);
        }
      }
    })
    
    
    .state("main.monitoreo", {
      url: "/monitoreo",
      controller: "MonitoreoCtrl as dt",
      location: 'replace',
      templateUrl: "views/seguimiento/monitoreo/main.tpl.html",
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/monitoreo/monitoreoCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.revision", {
      url: "/revision",
      controller: "RevisionCtrl as dt",
      templateUrl: "views/seguimiento/revision/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/revision/revisionCtrl.js"
          ]);
        }
      }
    })

    .state("main.revisionDetalle", {
      url: "/revisionDetalle",
      controller: "RevisionDetalleCtrl as dt",
      location: 'replace',
      templateUrl: "views/seguimiento/revision/detalle.tpl.html",
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/revision/detalleCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.autorizacion", {
      url: "/autorizacion",
      controller: "AutorizacionCtrl as dt",
      templateUrl: "views/seguimiento/autorizacion/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguimiento/autorizacion/autorizacionCtrl.js"
          ]);
        }
      }
    })
    
    
    .state("main.sabanaDatos", {
      url: "/sabanaDatos",
      controller: "SabanaDatosCtrl as dt",
      templateUrl: "views/reportes/sabanaDatos/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/reportes/sabanaDatos/sabanaDatosCtrl.js"
          ]);
        }
      }
    })
 
    .state("main.ejecutivos", {
      url: "/ejecutivos",
      controller: "EjecutivosCtrl as dt",
      templateUrl: "views/reportes/ejecutivos/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/reportes/ejecutivos/ejecutivosCtrl.js"
          ]);
        }
      }
    })   
    
    .state("main.historico", {
      url: "/historico",
      controller: "HistoricoCtrl as dt",
      templateUrl: "views/historico/anterior/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/historico/anterior/historicoCtrl.js"
          ]);
        }
      }
    })
    
    
    
    .state("main.bitacora", {
      url: "/bitacora",
      controller: "MovimientosCtrl as dt",
      templateUrl: "views/bitacora/consulta/main.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/bitacora/consulta/movimientosCtrl.js"
          ]);
        }
      }
    })
    .state("main.notificaciones", {
      url: "/notificaciones",
      controller: "NotificacionesCtrl as dt",
      templateUrl: "views/bitacora/consulta/notificaciones.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/bitacora/consulta/notificacionesCtrl.js"
          ]);
        }
      }
    })
    
    .state("main.cambioPassword", {
      url: "/cambioPassword",
      controller: "CambioPasswordCtrl as dt",
      cache: false,
      templateUrl: "views/seguridad/configuracion/cambioPassword.tpl.html",
      location: 'replace',
      params: {reload: false},
      resolve: {
                loadDatatables: function($ocLazyLoad) {
          	   return $ocLazyLoad.load("datatables");
                },
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                   return $ocLazyLoad.load('resources/js/angularJS/controllers/seguridad/configuracion/cambioPasswordCtrl.js');
                }]
               }
    })
    
    .state("main.usuarios", {
      url: "/usuarios",
      controller: "UsuariosCtrl as dt",
      cache: false,
      templateUrl: "views/seguridad/configuracion/usuarios.tpl.html",
      location: 'replace',
      params: {reload: false},
      resolve: {
                loadDatatables: function($ocLazyLoad) {
          	   return $ocLazyLoad.load("datatables");
                },
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                   return $ocLazyLoad.load('resources/js/angularJS/controllers/seguridad/configuracion/usuariosCtrl.js');
                }]
               }
    })
    
    .state("main.roles", {
      url: "/roles",
      controller: "RolesCtrl as dt",
      cache: false,
      templateUrl: "views/seguridad/configuracion/roles.tpl.html",
      location: 'replace',
      resolve: {
        loadDatatables: function($ocLazyLoad) {
          return $ocLazyLoad.load("datatables");
        },
        loadFiles: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            "resources/js/angularJS/controllers/seguridad/configuracion/rolesCtrl.js"
          ]);
       },
       onEnter: function () {
        console.log("gato----------------------");
       }
      }
    })
    
    
    .state("main.legisladores", {
      url: "/legisladores",
      templateUrl: "views/legisladores.tpl.html"
    });
});

angular
  .module("pronappinaApp")
.directive('loadingStatusMessage', function() {
  return {
    link: function($scope, $element, attrs) {
      var show = function() {
        $element.css('display', 'block');
      };
      var hide = function() {
        $element.css('display', 'none');
      };
      $scope.$on('loadingStatusActive', show);
      $scope.$on('loadingStatusInactive', hide);
      hide();
    }
  };
});

angular
  .module("pronappinaApp")
.directive('ngConfirmClick', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Estas seguro?";
                    var clickAction = attr.confirmedClick;
                    var types = [        BootstrapDialog.TYPE_SUCCESS,  
                        BootstrapDialog.TYPE_DANGER];
                    element.bind('click',function (event) {
                    	BootstrapDialog.show({
                            type: types[1],
            		closable: true,
            		closeByBackdrop: false,
            		closeByKeyboard: false,
            		title: 'Mensaje del Sistema',
            		message: msg,
            		buttons: [{
            			label: 'Aceptar',
            			action: function (dialog) {
            				scope.$eval(clickAction)
            				dialog.close();
            			}
            		}]
            	});
                    });
                }
            };
    }])

angular
  .module("pronappinaApp")
.directive( 'tooltipPopup', function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: "views/generico/tooltip-popup.html",
  };
})
.directive( 'tooltip', function ( $compile, $timeout ) {
  
  var template = 
    '<tooltip-popup></tooltip-popup>';
  
  return {
    scope: { tooltipTitle: '@tooltip', placement: '@tooltipPlacement', animation: '&tooltipAnimation' },
    link: function ( scope, element, attr ) {
      var tooltip = $compile( template )( scope ), 
          transitionTimeout;
      
      // Calculate the current position and size of the directive element.
      function getPosition() {
        return {
          width: element.prop( 'offsetWidth' ),
          height: element.prop( 'offsetHeight' ),
          top: element.prop( 'offsetTop' ),
          left: element.prop( 'offsetLeft' )
        };
      }
      
      // Show the tooltip popup element.
      function show() {
        var position,
            ttWidth,
            ttHeight,
            ttPosition;
          
        // If no placement was provided, default to 'top'.
        scope.placement = scope.placement || 'top';
        
        // If there is a pending remove transition, we must cancel it, lest the
        // toolip be mysteriously removed.
        if ( transitionTimeout ) $timeout.cancel( transitionTimeout );
        
        // Lazy compile the tooltip element
        // FIXME: For some reason, this does *not* always work correctly on the 
        // *first* run, but does so on all subsequent runs.
        //tooltip = tooltip ||  $compile( template )( scope );
        
        // Set the initial positioning.
        tooltip.css({ top: 0, left: 0, display: 'block' });
        
        // Now we add it to the DOM because need some info about it. But it's not 
        // visible yet anyway.
        element.after( tooltip );
        
        // Get the position of the directive element.
        position = getPosition();
        
        // Get the height and width of the tooltip so we can center it.
        ttWidth = tooltip.prop( 'offsetWidth' );
        ttHeight = tooltip.prop( 'offsetHeight' );
        
        // Calculate the tooltip's top and left coordinates to center it with
        // this directive.
        switch ( scope.placement ) {
          case 'right':
            ttPosition = {
              top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
              left: (position.left + position.width) + 'px'
            };
            break;
          case 'bottom':
            ttPosition = {
              top: (position.top + position.height) + 'px',
              left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
            };
            break;
          case 'left':
            ttPosition = {
              top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
              left: (position.left - ttWidth) + 'px'
            };
            break;
          default:
            ttPosition = {
              top: (position.top - ttHeight) + 'px',
              left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
            };
            break;
        }
        
        // Now set the calculated positioning.
        tooltip.css( ttPosition );
          
        // And show the tooltip.
        scope.isOpen = true;
      }
      
      // Hide the tooltip popup element.
      function hide() {
        // First things first: we don't show it anymore.
        //tooltip.removeClass( 'in' );
        scope.isOpen = false;
        
        // And now we remove it from the DOM. However, if we have animation, we 
        // need to wait for it to expire beforehand.
        // FIXME: this is a placeholder for a port of the transitions library.
        if ( angular.isDefined( scope.animation ) && scope.animation() ) {
          transitionTimeout = $timeout( function () { tooltip.remove(); }, 500 );
        } else {
          tooltip.remove();
        }
      }
      
      // Register the event listeners.
      element.bind( 'mouseenter', function() {
        scope.$apply( show );
      });
      element.bind( 'mouseleave', function() {
        scope.$apply( hide );
      });
    }
  };
});

angular
  .module("pronappinaApp")
         .factory('loadingStatusInterceptor', function($q, $rootScope) {
  var activeRequests = 0;
  var started = function() {
    if(activeRequests==0) {
      $rootScope.$broadcast('loadingStatusActive');
    }    
    activeRequests++;
  };
  var ended = function() {
    activeRequests--;
    if(activeRequests==0) {
      $rootScope.$broadcast('loadingStatusInactive');
    }
  };
  return {
    request: function(config) {
      started();
      return config || $q.when(config);
    },
    response: function(response) {
      ended();
      return response || $q.when(response);
    },
    responseError: function(rejection) {
      ended();
      return $q.reject(rejection);
    }
  };
});

angular
  .module("pronappinaApp")
  .config(config)
  .run(["$rootScope", "$state", "$stateParams", "$http", 
    function($rootScope, $state, $stateParams, $http) {
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;
	//$rootScope.rol = window.localStorage.getItem('PERFIL');
	//$rootScope.userName = window.localStorage.getItem('USER');
      
	$rootScope.user = new Object();
	$rootScope.user.username = window.localStorage.getItem('USER');
	$rootScope.user.perfil = new Object();
	$rootScope.user.dependencia = new Object();


        $rootScope.autorizacion = {};
	$rootScope.autorizacion.usuario = "";
	$rootScope.autorizacion.administracion = {};
	$rootScope.autorizacion.administracion.visible = false;
	$rootScope.autorizacion.ejecucion = {};
	$rootScope.autorizacion.ejecucion.visible = false;
	$rootScope.autorizacion.reportes = {};
	$rootScope.autorizacion.reportes.visible = false;
	$rootScope.autorizacion.historico = {};
	$rootScope.autorizacion.historico.visible = false;
	$rootScope.autorizacion.seguridad = {};
	$rootScope.autorizacion.seguridad.visible = false;
	$rootScope.autorizacion.bitacora = {};
	$rootScope.autorizacion.bitacora.visible = false;


	$http({
		url: '/pronapinnaback/accion/consulta/seguridad/usuarioByUserName.do',
		method: "POST",
		data: $rootScope.user,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	}).then(function onSuccess(response) {
		var data = response.data;
		$rootScope.user.id = data.id;
		$rootScope.user.nombre = data.nombre + ' '+ data.apellidoPaterno + ' ' + data.apellidoMaterno;
		$rootScope.user.perfil = data.catPerfil;
		$rootScope.user.dependencia = data.catDependencia;

	if($rootScope.user.perfil.clave == 'SUPERADMIN'){
		$rootScope.autorizacion.administracion.visible=true;
		$rootScope.autorizacion.ejecucion.visible=true;
		$rootScope.autorizacion.reportes.visible=true;
		$rootScope.autorizacion.historico.visible=true;
		$rootScope.autorizacion.seguridad.visible=true;
		$rootScope.autorizacion.bitacora.visible=true;
	}
	if($rootScope.user.perfil.clave == 'ADMINISTRADOR'){
		$rootScope.autorizacion.administracion.visible=true;
		$rootScope.autorizacion.seguridad.visible=true;
		$rootScope.autorizacion.bitacora.visible=true;

		$rootScope.autorizacion.ejecucion.visible=true;
                $rootScope.autorizacion.ejecucion.btnGeneraActividad=false;
                $rootScope.autorizacion.ejecucion.btnRechazaActividad=false;
                $rootScope.autorizacion.ejecucion.btnApruebaActividad=false;
                $rootScope.autorizacion.ejecucion.inputVerificador=false;
                $rootScope.autorizacion.ejecucion.inputActividad=true;
                $rootScope.autorizacion.reportes.visible = true;

		$rootScope.autorizacion.ejecucion.accionesPuntuales=false;
		$rootScope.autorizacion.ejecucion.monitoreo = true;
		$rootScope.autorizacion.ejecucion.autorizacion = false;

	}
	if($rootScope.user.perfil.clave == 'OPERADOR'){

  	    $http({
		url: '/pronapinnaback/accion/consulta/tblPeriodoActivo.do',
		method: "POST",
		data: null,
		headers: {'Authorization': 'Bearer '+window.localStorage.getItem('TOKEN'),'Content-Type': 'application/json'}
	    }).then(function onSuccess(response) {
	    	var types = [        BootstrapDialog.TYPE_SUCCESS,  
                BootstrapDialog.TYPE_DANGER];
                if(response.data.length > 0){
		   $rootScope.autorizacion.ejecucion.visible=true;
                   $rootScope.autorizacion.ejecucion.btnGeneraActividad=true;
                   $rootScope.autorizacion.ejecucion.btnRechazaActividad=false;
                   $rootScope.autorizacion.ejecucion.btnApruebaActividad=false;
                   $rootScope.autorizacion.ejecucion.inputVerificador=true;
                   $rootScope.autorizacion.ejecucion.inputActividad=false;

		   $rootScope.autorizacion.ejecucion.accionesPuntuales=true;
		   $rootScope.autorizacion.ejecucion.monitoreo = false;
		   $rootScope.autorizacion.ejecucion.autorizacion = false;

                   

                }else{
                  BootstrapDialog.show({
                      type: types[1],
      		closable: true,
      		closeByBackdrop: false,
      		closeByKeyboard: false,
      		title: 'Mensaje del Sistema',
      		message: 'El periodo de captura ya no se encuentra disponible.',
      		buttons: [{
      			label: 'Aceptar',
      			action: function (dialog) {
      				dialog.close();
      			}
      		}]
      	});
                }
	    }).catch(function onError(response) {

	    });        

	}
/*
	if($rootScope.user.perfil.clave == 'VERIFICADOR'){
		$rootScope.autorizacion.ejecucion.visible=true;
                $rootScope.autorizacion.ejecucion.btnGeneraActividad=false;
                $rootScope.autorizacion.ejecucion.btnRechazaActividad=true;
                $rootScope.autorizacion.ejecucion.btnApruebaActividad=true;
                $rootScope.autorizacion.ejecucion.inputVerificador=false;
                $rootScope.autorizacion.ejecucion.inputActividad=true;
                $rootScope.autorizacion.reportes.visible = true;

		$rootScope.autorizacion.ejecucion.accionesPuntuales=false;
		$rootScope.autorizacion.ejecucion.monitoreo = true;
		$rootScope.autorizacion.ejecucion.autorizacion = false;
	}
	if($rootScope.user.perfil.clave == 'AUTORIZADOR'){
		$rootScope.autorizacion.ejecucion.visible=true;
                $rootScope.autorizacion.ejecucion.btnGeneraActividad=false;
                $rootScope.autorizacion.ejecucion.btnRechazaActividad=true;
                $rootScope.autorizacion.ejecucion.btnApruebaActividad=true;
                $rootScope.autorizacion.ejecucion.inputVerificador=false;
                $rootScope.autorizacion.ejecucion.inputActividad=true;
                $rootScope.autorizacion.reportes.visible = true;

		$rootScope.autorizacion.ejecucion.accionesPuntuales=false;
		$rootScope.autorizacion.ejecucion.monitoreo = false;
		$rootScope.autorizacion.ejecucion.autorizacion = true;
	}
*/

	}).catch(function onError(response) {

	});        
	
    }
  ]);
