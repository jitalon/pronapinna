<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html ng-app="pronappinaApp">
  <head>
    <meta charset="UTF-8" property="gobmxhelper" content="no plugins">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Secretar&iacute;a de Gobernaci&oacute;n</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
    <meta http-equiv="Content-Security-Policy" content="default-src 'self' http://*; frame-src 'none'; form-action 'self'; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval';  object-src 'none';" />
    
    <meta name="referrer" content="origin-when-crossorigin">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">  
  
    <!-- Royal Slider -->
    <!--link href="royalslider/royalslider.css" rel="stylesheet">
    <link href="royalslider/skins/default-inverted/rs-default-inverted.css" rel="stylesheet"-->

	  <!-- Layer Slider -->
	  <!--link rel="stylesheet" href="layerslider/css/layerslider.css" type="text/css"-->
    
	  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!--[if lt IE 9]>
	  <script src="/pronapinna/resources/assets/js/html5shiv.js"></script>
	  <script src="/pronapinna/resources/assets/js/respond.min.js"></script>
	  <![endif]-->
    <!-- Bootstrap core CSS -->
    <link href="/pronapinna/resources/assets/css/bootstrap.css" rel="stylesheet">
    
    <link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
    <!--Cambio Style CSS-->
    <link href="/pronapinna/resources/style.css" rel="stylesheet">
    <link href="/pronapinna/resources/css/tex.css" rel="stylesheet">
	  
    <!-- Favicons -->
	  <link rel="shortcut icon" href="/pronapinna/resources/assets/ico/favicon.ico" type="image/x-icon">
  	<!-- Favicons -->
  	<link rel="shortcut icon" href="/pronapinna/resources/assets/ico/favicon.ico" type="image/x-icon">
  	<link rel="apple-touch-icon" href="/pronapinna/resources/assets/ico/apple-touch-icon.png" />
  	<link rel="apple-touch-icon" sizes="57x57" href="/pronapinna/resources/assets/ico/apple-touch-icon-57x57.png">
  	<link rel="apple-touch-icon" sizes="72x72" href="/pronapinna/resources/assets/ico/apple-touch-icon-72x72.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="/pronapinna/resources/assets/ico/apple-touch-icon-114x114.png">
  	<link rel="apple-touch-icon" sizes="144x144" href="/pronapinna/resources/assets/ico/apple-touch-icon-144x144.png">
    
    <!--<script type='text/javascript' src='https://code.jquery.com/jquery-3.1.1.js'></script> -->
  </head>
    <c:if test="${pageContext.request.userPrincipal.name != null}"> 
        <script>
           window.localStorage.setItem('USER',"${pageContext.request.userPrincipal.name}");
   	       window.localStorage.setItem('PERFIL',"${pageContext.request.userPrincipal.authorities}");
        </script>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </c:if>
  <body ng-controller="AppCtrl as app"  data-spy="scroll" data-offset="25">
    <div loading-status-message class="modal-backdrop fade ng-scope in"  ng-style="{'z-index': 1050 + $$topModalIndex*10, display: 'block'}">
     <div class="modal-dialog modal-15"><img src="/pronapinna/resources/images/cargando.gif" style="margin-top: -23px;margin-left: -163px;left: 50%;top: 50%;position: absolute;" alt="Imagen1"/></div>
    </div>

    <div ui-view></div>
    <div class="description"></div>
    <!-- Main Scripts -->
    <script src="/pronapinna/resources/js/libcdn/jquery.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/bootstrap/bootstrap.min.js"></script>
    <!--<script src="https://framework-gb.cdn.gob.mx/assets/scripts/jquery-ui-datepicker.js"></script>-->
    <script src="/pronapinna/resources/js/libcdn/datepicker/jquery-ui-datepicker.js"></script>
    <script src="/pronapinna/resources/js/vendor/bootstrap/bootstrap-dialog.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/metismenu/metismenu.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/slimscroll/slimscroll.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/moment/moment.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/greensock-js/plugins/CSSPlugin.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/greensock-js/easing/EasePack.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/greensock-js/TweenLite.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/lodash-4.17.21/lodash.min.js"></script>

    <!-- Main Anglar Scripts-->
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/angular.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/angular-sanitize.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/angular-animate.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular-ui/angular-ui-router.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular-ui/angular-ui-bootstrap-tpls.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/ocLazyLoad/ocLazyLoad.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular-upload/ng-upload.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular-upload/ng-upload.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/checklist-model.js"></script>          

    <script src="/pronapinna/resources/js/vendor/highcharts/highcharts.js"></script>
    <script src="/pronapinna/resources/js/vendor/highcharts/highcharts-3d.js"></script>

    <!-- App Anglar Scripts --> 
    <script src="/pronapinna/resources/js/angularJS/app.js"></script> 
    <script src="/pronapinna/resources/js/angularJS/config.js"></script> 
    <!--script src="/pronapinna/resources/js/angularJS/directives.js"></script--> 
    <script src="/pronapinna/resources/js/angularJS/controller.js"></script>
    
    <script src="/pronapinna/resources/js/vendor/soap/soapclient.js"></script>
	  <script src="/pronapinna/resources/js/vendor/soap/angular.soap.js"></script>

    <script src="/pronapinna/resources/js/vendor/exportXls/xlsx.full.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/exportXls/FileSaver.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/exportXls/tableexport.min.js"></script>
    <script src="/pronapinna/resources/js/libcdn/gobmx.js"></script>
    <!--<script src="https://framework-gb.cdn.gob.mx/gobmx.js"></script>-->
  </body>
</html>