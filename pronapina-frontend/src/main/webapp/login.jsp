<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html ng-app="pronappinaApp" lang="en">
<head>
	<meta charset="utf-8" property="gobmxhelper" content="no plugins"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta http-equiv="Content-Security-Policy" content="default-src 'self' http://*; frame-src 'none'; form-action 'self'; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval';  object-src 'none';" />
    <meta name="referrer" content="origin-when-crossorigin">
	<title>PRONAPINNA</title>

	<!-- Bootstrap core CSS -->
	<link href="/pronapinna/resources/assets/css/bootstrap.css" rel="stylesheet">

	<link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
	<!--Cambio Style CSS-->
	<link href="/pronapinna/resources/style.css" rel="stylesheet">
	<link href="/pronapinna/resources/css/tex.css" rel="stylesheet">
</head>

<body ng-controller="AppLoginCtrl as app" ata-spy="scroll"	data-offset="25">

	<header class="header-wrapper" align="center" style="margin: 59px 0 0 0;">
		<br> <img src="/pronapinna/resources/images/sipinna_blanco.png"
			width="300" alt="Logo" /> <br>
		<h1 class="form-signin-heading tituloprincipal">Bienvenidos a PRONAPINNA</h1>
		<br />
	</header> 
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<p>&nbsp;</p>
				<form ng-submit="login()" class="form-signin" autocomplete="off">
					<div class="form-group {{error != null ? 'has-error' : ''}}">
						<div class="col-md-12">
							<span class="oculta">{{message}}</span>
						</div>
						<div class="col-md-12">
							<input name="username" type="text" class="form-control" placeholder="Usuario" autofocus="true" ng-model="username" style="margin-bottom: 5px;"/>
						</div>
						<div class="col-md-12">
							<input name="password" type="password" class="form-control" placeholder="Clave de acceso" ng-model="password" style="margin-bottom: 5px;"/>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						</div>
						<div class="col-md-6">
							<a href="/pronapinna/recuperacion"> Recuperar Contrase&ntilde;a</a>
						</div>
						<div class="col-md-6">
							<button class="btn btn-primary pull-right" type="submit">Accesar</button>
						</div>
						<div class="col-md-12">
							<br>
							<br>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	
	<!-- Main Scripts -->
	<script src="/pronapinna/resources/js/libcdn/jquery.min.js"></script>
	<script src="/pronapinna/resources/js/vendor/bootstrap/bootstrap.min.js"></script>
	<!--<script src="https://framework-gb.cdn.gob.mx/assets/scripts/jquery-ui-datepicker.js"></script>-->
    <script src="/pronapinna/resources/js/libcdn/datepicker/jquery-ui-datepicker.js"></script>
	<script	src="/pronapinna/resources/js/vendor/bootstrap/bootstrap-dialog.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/metismenu/metismenu.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/slimscroll/slimscroll.min.js"></script>
	<script src="/pronapinna/resources/js/vendor/moment/moment.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/greensock-js/plugins/CSSPlugin.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/greensock-js/easing/EasePack.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/greensock-js/TweenLite.min.js"></script>
	<script src="/pronapinna/resources/js/vendor/lodash-4.17.21/lodash.min.js"></script>

	<!-- Main Anglar Scripts-->
	<script src="/pronapinna/resources/js/vendor/angular1.8.2/angular.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/angular-sanitize.min.js"></script>
    <script src="/pronapinna/resources/js/vendor/angular1.8.2/angular-animate.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/angular-ui/angular-ui-router.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/angular-ui/angular-ui-bootstrap-tpls.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/ocLazyLoad/ocLazyLoad.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/angular-upload/ng-upload.js"></script>
	<script	src="/pronapinna/resources/js/vendor/angular-upload/ng-upload.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/angular1.8.2/checklist-model.js"></script>

	<script src="/pronapinna/resources/js/vendor/highcharts/highcharts.js"></script>
	<script	src="/pronapinna/resources/js/vendor/highcharts/highcharts-3d.js"></script>

	<!-- App Anglar Scripts -->
	<script src="/pronapinna/resources/js/angularJS/app.js"></script>
	<script src="/pronapinna/resources/js/angularJS/config.js"></script>
	<!--script src="/pronapinna/resources/js/angularJS/directives.js"></script-->
	<script src="/pronapinna/resources/js/angularJS/login.js"></script>

	<script src="/pronapinna/resources/js/vendor/soap/soapclient.js"></script>
	<script src="/pronapinna/resources/js/vendor/soap/angular.soap.js"></script>

	<script	src="/pronapinna/resources/js/vendor/exportXls/xlsx.full.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/exportXls/FileSaver.min.js"></script>
	<script	src="/pronapinna/resources/js/vendor/exportXls/tableexport.min.js"></script>
	<!--<script src="https://framework-gb.cdn.gob.mx/gobmx.js"></script>-->
	<script src="/pronapinna/resources/js/libcdn/gobmx.js"></script>
</body>
</html>