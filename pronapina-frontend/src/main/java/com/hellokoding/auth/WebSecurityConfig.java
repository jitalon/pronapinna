package com.hellokoding.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;


import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
    @Autowired
    private DataSource dataSource;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .requiresChannel()
                .antMatchers("/**","/resources/**", "/recuperacion", "/passRecovery/**").requiresSecure();
//                .anyRequest().permitAll()
//                .and()
//            .formLogin()
//                .loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password")
//                .loginPage("/login")
//                .permitAll()
//                .and()
//            .logout()
//                .permitAll()
//                .and()
//            .csrf().disable();
        http
  	  	.headers()
  	  		.contentSecurityPolicy("script-src 'self'");
        http
        .headers()
            .referrerPolicy(ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);
        http
    	.headers()
    		.httpStrictTransportSecurity()
    			.includeSubDomains(true)
    			.maxAgeInSeconds(2592000);
        http.headers().xssProtection();

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
    	PasswordEncoder encoder = new BCryptPasswordEncoder();
    	return encoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    String password = getEncryptedPass();
	PasswordEncoder passwordEncoder1 = new BCryptPasswordEncoder();
	String hashedPassword = passwordEncoder1.encode(password);

        System.out.println("************************************************************ " + hashedPassword);


        auth.jdbcAuthentication().dataSource(dataSource)
            .authoritiesByUsernameQuery("select nombre,b.clave from pronapinna.Tbl_usuario a, pronapinna.cat_perfil b where a.id_perfil = b.id and a.usuario=? and a.id_estatus=1")
            .usersByUsernameQuery("select usuario,password,true from pronapinna.Tbl_usuario where usuario=? and id_estatus=1");
            
    }

	private String getEncryptedPass() {
		// TODO Auto-generated method stub
		return "password";
	}

	
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		Properties sipinnaSetup = new Properties();
		try {
			sipinnaSetup.load(WebSecurityConfig.class.getResourceAsStream("/application.properties"));

		} catch (IOException e) {
		}

	     MultipartConfigFactory factory = new MultipartConfigFactory();

	     factory.setMaxFileSize(sipinnaSetup.getProperty("spring.http.multipart.max-file-size"));

	     factory.setMaxRequestSize(sipinnaSetup.getProperty("spring.http.multipart.max-request-size"));

	     return factory.createMultipartConfig();

	}
/*
    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication()
                .withUser("user").password("{noop}password").roles("USER")
            .and()
                .withUser("manager").password("{noop}password").roles("MANAGER");
    }
*/
}