package com.hellokoding.auth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException; 
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {


    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        LOG.debug("****************************************** Login debug message " + error);
        if (error != null)
            model.addAttribute("error", "El usuario o clave de acceso son inv&aacute;lidas.");

        if (logout != null)
            model.addAttribute("message", "Has cerrado sesión correctamente.");

        return "login";
    }

}